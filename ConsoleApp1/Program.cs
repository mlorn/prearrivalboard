﻿using iText.Forms;
using iText.Forms.Fields;
using iText.Kernel.Pdf;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using PreArrivalBoard;


namespace ConsoleApp1
{
  class Program
  {
    static void Main(string[] args)
    {
      Payout();
      Console.WriteLine("Press any key to continue...");
      Console.ReadKey();
    }

    private static void Payout()
    {
      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        //var payoutData = magnumDb.spLetterPayout("M107306", "PD984");
        //if (payoutData != null)
        //{
        //  foreach (var r in payoutData)
        //  {
        //    if (r.TransactionKindID == 7)
        //    {
        //      Console.WriteLine($"Resv No={r.ReservationNumber}");
        //      Console.WriteLine($"Transaction={r.TransactionKindID}");
        //      Console.WriteLine($"amount= {r.Amount}");
        //    }
        //  }
        //}
      }

    }

    private static void PreArrival()
    {
      string filePath = "";
      string fileName;
      string requestUrl;
      string physicalFileName;
      byte[] fileContent = null;
      string resvNo = "M103001";

      filePath = Properties.Settings.Default.StaticURL;
      fileName = "Additional-Guests.pdf";

      requestUrl = $"{filePath}/{fileName}";

      fileName = $"{fileName} ({resvNo})";
      physicalFileName = $@"{Properties.Settings.Default.LetterFolder}\{fileName}";
      HttpClient client = new HttpClient();

      HttpResponseMessage msg = client.GetAsync(requestUrl).Result;
      if (msg.IsSuccessStatusCode)
      {
        //using var fs = new FileStream(physicalFileName, FileMode.Create, FileAccess.Write);

        // create a new file to write to
        //var contentStream = msg.Content.ReadAsStreamAsync().Result; // get the actual content stream
        fileContent = msg.Content.ReadAsByteArrayAsync().Result;

        using MemoryStream ms = new MemoryStream();
        ms.Write(fileContent, 0, fileContent.Length);

        ms.Position = 0;

        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(new PdfReader(ms), new PdfWriter(physicalFileName));
        try
        {
          PdfAcroForm form = PdfAcroForm.GetAcroForm(pdf, true);

          IDictionary<String, PdfFormField> fields = form.GetFormFields();

          PdfFormField toSet;


          if (fields.TryGetValue("FirstAndLastName", out toSet))
          {
            toSet.SetValue("Mao Lorn");
          }

          fields.TryGetValue("CellPhoneNumber", out toSet);

          toSet.SetValue("(401) 236-0151");

          fields.TryGetValue("ReservationNumber", out toSet);

          toSet.SetValue("M103001");

          fields.TryGetValue("VillaName", out toSet);

          toSet.SetValue("WV GYP");

          fields.TryGetValue("ArrivalDate", out toSet);

          toSet.SetValue("02/10/2022");

          fields.TryGetValue("DepartureDate", out toSet);

          toSet.SetValue("02/17/2022");
        }
        finally
        {
          pdf.Close();
          ms.Flush();
          ms.Close();
        }
      }

    }
  }
}
