using System;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
//using System.Windows.Forms;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;

namespace PreArrivalBoard
{
  /// <summary>
  /// Print revision letters or Refund Letters or Remittance Letters
  /// Letters can be printed individually by a Resv No or all 
  /// letters that have been changed and approved or a deposit/payment
  /// reference number.
  /// </summary>
  public class ProcessLetterItineraryLetter
  {
    private DataSet dsResv;
    private DataSet dsAirResv = null;
    private string resvNo;
    //private string depositNo;
    private string revisionTag;
    private int printTo;
    private List<PrintedLetter> printedLetter = new List<PrintedLetter>();
    private LetterData letterData;
    private PrintLetter printLetter;

    public ProcessLetterItineraryLetter(PrintLetter printLetter)
    {
      this.printLetter = printLetter;
    }

    #region Print H Reservation
    /// <summary>
    /// Client Confirmation Without TA - H Reservation
    /// </summary>
    private void clientConfirmationWithoutTA_H()
    {
      string filterString;

      filterString = "ResvPrefix = 'H' AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorKindID <> 4) ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Client Confirmation With TA - H Reservation
    /// </summary>
    private void clientConfirmationWithTA_H()
    {
      string filterString;

      filterString = "ResvPrefix = 'H' AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientConfirmationWithTA_HM,
          (int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo);
      }
    }

    #endregion

    #region Print M Reservation
    /// <summary>
    /// Client Confirmation Without TA - M Reservation
    /// </summary>
    private void clientConfirmationWithoutTA_M()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation Without TA - M Reservation
    /// Deposit due is charged to a credit card
    /// </summary>
    private void clientConfirmationWithoutTA_M2()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Insufficient Deposit Without TA - M Reservation
    /// </summary>
    private void clientInsufficientDepositWithoutTA_M()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientInsufficientDepositWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation With TA - M Reservation
    /// </summary>
    private void clientConfirmationWithTA_M()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientConfirmationWithTA_HM,
          (int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation With TA - M Reservation
    /// Deposit Due is charged to a credit card
    /// </summary>
    private void clientConfirmationWithTA_M2()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientConfirmationWithTA_HM,
          (int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    /// <summary>
    /// Client Insufficient Deposit With TA - M Reservation
    /// </summary>
    private void clientInsufficientDepositWithTA_M()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientInsufficientDepositWithTA_HM,
         (int)LetterType.ClientInsufficientDepositWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region Print M Reservation for WSBH (vendor base ID 7684)
    /// <summary>
    /// Client Confirmation Without TA - M Reservation
    /// </summary>
    private void clientConfirmationWithoutTA_M_7684()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation Without TA - M Reservation
    /// Deposit due is charged to a credit card
    /// </summary>
    private void clientConfirmationWithoutTA_M2_7684()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Insufficient Deposit Without TA - M Reservation
    /// </summary>
    private void clientInsufficientDepositWithoutTA_M_7684()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientInsufficientDepositWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation With TA - M Reservation
    /// </summary>
    private void clientConfirmationWithTA_M_7684()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientConfirmationWithTA_HM,
          (int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation With TA - M Reservation
    /// Deposit Due is charged to a credit card
    /// </summary>
    private void clientConfirmationWithTA_M2_7684()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientConfirmationWithTA_HM,
          (int)LetterType.ClientConfirmationWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    /// <summary>
    /// Client Insufficient Deposit With TA - M Reservation
    /// </summary>
    private void clientInsufficientDepositWithTA_M_7684()
    {
      string filterString;

      filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
      filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientInsufficientDepositWithTA_HM,
         (int)LetterType.ClientInsufficientDepositWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion


    #region H Reservation Deposit Request without TA
    /// <summary>
    /// Deposit Request Letter without TA - H Reservation
    /// </summary>
    private void depositRequestLetterWithoutTA_H()
    {
      string filterString;

      filterString = "(ResvPrefix = 'H') AND IsTravelAgent = 'N' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.DepositRequestLetterWithoutTA_HM, this.dsResv, this.printTo);
      }
    }
    #endregion

    #region H Reservation Deposit Request with TA
    /// <summary>
    /// Deposit Request Letter with TA - H Reservation
    /// </summary>
    private void depositRequestLetterWithTA_H()
    {
      string filterString;

      filterString = "(ResvPrefix = 'H') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND IsTravelAgent = 'Y' AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.DepositRequestLetterWithTA_HM,
          (int)LetterType.DepositRequestLetterWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    #endregion

    #region M Reservation Deposit Request without TA
    /// <summary>
    /// Deposit Request Letter without TA - M Reservation
    /// </summary>
    private void depositRequestLetterWithoutTA_M()
    {
      string filterString;

      filterString = "(ResvPrefix = 'M') AND IsTravelAgent = 'N' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND (VendorBaseID <> 7684) ";  //Vendor Base ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.DepositRequestLetterWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region M Reservation Deposit Request with TA
    /// <summary>
    /// Deposit Request Letter with TA - M Reservation
    /// </summary>
    private void depositRequestLetterWithTA_M()
    {
      string filterString;

      filterString = "(ResvPrefix = 'M') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND IsTravelAgent = 'Y' AND (VendorBaseID <> 7684) ";  //Vendor Base ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.DepositRequestLetterWithTA_HM,
         (int)LetterType.DepositRequestLetterWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region M Reservation Deposit Request for WSBH (vendor base id 7684)
    /// <summary>
    /// Deposit Request Letter without TA - M Reservation
    /// </summary>
    private void depositRequestLetter_M_7684()
    {
      string filterString;

      filterString = "(ResvPrefix = 'M') AND IsTravelAgent = 'N' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.DepositRequestLetterWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    #endregion

    private void depositRequestLetterWithTA_M_7684()
    {
      string filterString;
      filterString = "(ResvPrefix = 'M') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND IsTravelAgent = 'Y' AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.DepositRequestLetterWithTA_HM,
         (int)LetterType.DepositRequestLetterWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    #region Print R Reservations

    /// <summary>
    /// Client Confirmation without TA - R Resv
    /// </summary>
    /// <param name="resvNo">The resv no.</param>
    private void clientConfirmationWithoutTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientConfirmationWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation without TA - R Resv
    /// to make sure that insufficient deposit letter not print
    /// </summary>
    private void clientConfirmationWithoutTA_R2()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND IsDepositPaid = 'Y' ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientConfirmationWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Insufficient Deposit without TA - R Resv
    /// </summary>
    private void clientInsufficientDepositWithoutTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND IsDepositPaid = 'N' ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientInsufficientDepositWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation with TA - R Resv
    /// </summary>
    private void clientConfirmationWithTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientConfirmationWithTA_R,
          (int)LetterType.ClientConfirmationWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Confirmation With TA - R Resv
    /// Take care of not to print insufficient deposit letter
    /// </summary>
    private void clientConfirmationWithTA_R2()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND isDepositPaid = 'Y' ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientConfirmationWithTA_R,
          (int)LetterType.ClientConfirmationWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Client Insufficient Deposit With TA - R Resv
    /// </summary>
    private void clientInsufficientDepositWithTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND isDepositPaid = 'N' ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.ClientInsufficientDepositWithTA_R,
          (int)LetterType.ClientInsufficientDepositWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Deposit Request Letter without TA - R Resv
    /// </summary>
    private void depositRequestLetterWithoutTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.DepositRequestLetterWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Deposit Request Letter with TA - R Resv
    /// </summary>
    private void depositRequestLetterWithTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND IsTravelAgent = 'Y' ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.DepositRequestLetterWithTA_R,
         (int)LetterType.DepositRequestLetterWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Deposit Request Letter French - R Resv
    /// </summary>
    private void depositRequestLetterFrench_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.DepositRequestLetterFrench_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    #endregion


    #region Air Confirmation Letter

    private void airConfirmationLetter()
    {
      if (this.dsAirResv == null)
      {
        this.dsAirResv = this.letterData.GetLetterAirData(this.resvNo, this.revisionTag);
      }
      if (this.dsAirResv.Tables[0].Rows.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.AirConfirmationLetter, this.dsAirResv, printTo);
      }
    }

    #endregion


    #region Print Client Refund Letter - H or M Reservation
    /// <summary>
    /// Client Refund Letter without TA - H or M Resv
    /// </summary>
    private void clientRefundLetterWithoutTA_HM()
    {
      string filterString;

      filterString = "ResvPrefix = 'H' OR ResvPrefix = 'M' AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND Status = 'CXL' ";
      filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientRefundLetterWithoutTA_HM, this.dsResv, this.printTo);
      }
    }


    /// <summary>
    /// Client Refund Letter with TA - H or M Resv
    /// </summary>
    private void clientRefundLetterWithTA_HM()
    {
      string filterString;

      filterString = "ResvPrefix = 'H' OR ResvPrefix = 'M' AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND Status = 'CXL' ";
      filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientRefundLetterWithTA_HM, this.dsResv, this.printTo);
      }
    }
    #endregion

    #region Print Client Refund Letter - R Reservations

    /// <summary>
    /// Client Refund Letter without TA - R Resv
    /// </summary>
    private void clientRefundLetterWithoutTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND Status = 'CXL' ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientRefundLetterWithoutTA_R, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Client Refund Letter with TA - R Resv
    /// </summary>
    private void clientRefundLetterWithTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND Status = 'CXL' ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.ClientRefundLetterWithTA_R, this.dsResv, this.printTo);
      }
    }
    #endregion

    #region PM Remittance Letters

    /// <summary>
    /// PM Remittance with Gross Deal
    /// </summary>
    private void pmRemittanceWithGrossDeal()
    {
      string filterString;

      filterString = "TransactionKindID = 7 AND IsNetDeal = 'N' ";

      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.PMRemittanceWithGrossDeal, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// PM Remittance with Net Deal
    /// </summary>
    private void pmRemittanceWithNetDeal()
    {
      string filterString;

      filterString = "TransactionKindID = 7 AND IsNetDeal = 'Y' ";

      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.PMRemittanceWithNetDeal, this.dsResv, this.printTo);
      }
    }
    #endregion

    #region PM Confirmation Letter
    /// <summary>
    /// PM Confirmation Letter Without TA - R Resv
    /// </summary>
    private void pmConfirmationWithoutTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 7) ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.PMConfirmationWithoutTA_R, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// PM Confirmation Letter With TA - R Resv
    /// </summary>
    private void pmConfirmationWithTA_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 7) ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.PMConfirmationWithTA_R, this.dsResv, this.printTo);
      }
    }
    #endregion

    #region PM Confirmation H and M Resv
    /// <summary>
    /// PM Confirm without TA - H & M Resv
    /// </summary>
    private void pmConfirmationWithoutTA_HM()
    {
      string filterString;

      filterString = "(ResvPrefix = 'H' OR ResvPrefix = 'M') AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 7) ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.PMConfirmationWithoutTA_HM, this.dsResv, this.printTo);
      }
    }
    #endregion

    /// <summary>
    /// PM Confirm with TA - H & M Resv
    /// </summary>
    private void pmConfirmationWithTA_HM()
    {
      string filterString;

      filterString = "(ResvPrefix = 'H' OR ResvPrefix = 'M') AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 7) ";
      filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.PMConfirmationWithTA_HM, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Cars the reservation confirmation.
    /// </summary>
    private void CarReservationConfirmation()
    {
      string filterString;

      filterString = "(ResvPrefix = 'H') AND VendorKindID = 4 ";
      filterString += "AND (Status <> 'WTL' OR SpecialCxl <> 'All funds paid are non-refundable.') "; //rollover reservation
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.CarReservationConfirmation, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Itineraries the statement.
    /// </summary>
    private void ItineraryStatement()
    {
      if (dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.ItineraryStatement, this.dsResv, this.printTo);
      }
    }

    #region Client Travel Credit
    /// <summary>
    /// Clients the travel credit.
    /// </summary>
    private void clientTravelCredit()
    {
      string filterString;

      filterString = "(Status = 'WTL' AND SpecialCxl = 'All funds paid are non-refundable.') "; //rollover reservation
      filterString += "AND (WimcoTotalRecdClient + PMTotalRecdClient - TotalPaidClient) > 0 ";

      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile((int)LetterType.CleintTravelCredit, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    /// <summary>
    /// Processes the revision letters
    /// </summary>
    /// <param name="resvNo">The resv no.</param>
    /// <param name="revsionTag">The revsion tag.</param>
    /// <param name="printTo">The print to.</param>
    /// <returns></returns>
    public string Process(int itineraryID, string resvNo, string firstName, string lastName, int printTo)
    {
      //string resvNumberList = string.Empty;
      string result = string.Empty;
      bool processRevision = true;

      this.resvNo = resvNo;
      //this.revisionTag = revisionTag;
      this.printTo = printTo;
      //if languageID has not been set, set it to 1 as English default language
      //if (languageID == 0)
      //{
      //  languageID = Utility.English;
      //}
      this.letterData = new LetterData();
      this.dsResv = this.letterData.GetItineraryLetterData(itineraryID, resvNo, firstName, lastName);

      /*
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        this.dsAirResv = this.letterData.GetLetterAirData(this.resvNo, this.revisionTag);
        if (this.dsAirResv.Tables[0].Rows.Count == 0)
        {
          result = "There is no letter to print.";
          return result;
        }
        processRevision = false;
      }
      */
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        result = "There is no letter.";
        return result;
      }

      try
      {
        // clear reservation from the list.
        //ReservationList.ListOfReservations.Clear();

        #region Print H Reservation
        clientConfirmationWithoutTA_H();

        clientConfirmationWithTA_H();
        #endregion

        #region Print M Reservation

        clientConfirmationWithoutTA_M();

        clientConfirmationWithoutTA_M2();  // deposit due is charged to a credit card.

        clientInsufficientDepositWithoutTA_M();

        clientConfirmationWithTA_M();

        clientConfirmationWithTA_M2();   // Deposit due is charged to a credit card

        clientInsufficientDepositWithTA_M();
        #endregion

        #region Print M Reservation for WSBH (VendorBase ID 7684)
        // not running because WSBH reservations use R letter kinds
        /*
        clientConfirmationWithoutTA_M_7684();

        clientConfirmationWithoutTA_M2_7684();  // deposit due is charged to a credit card.

        clientInsufficientDepositWithoutTA_M_7684();

        clientConfirmationWithTA_M_7684();

        clientConfirmationWithTA_M2_7684();   // Deposit due is charged to a credit card

        clientInsufficientDepositWithTA_M_7684();
        */
        #endregion

        #region H Reservation Deposit Request

        depositRequestLetterWithoutTA_H();
        depositRequestLetterWithTA_H();
        #endregion

        #region M Reservation Deposit Request

        depositRequestLetterWithoutTA_M();
        depositRequestLetterWithTA_M();
        #endregion

        #region M Reservation Deposit Request for WSBH (VendorBase ID 7684)

        depositRequestLetter_M_7684();
        #endregion

        #region Print R Reservations
        clientConfirmationWithoutTA_R();
        clientConfirmationWithoutTA_R2();   // Take care of not to print insufficient deposit letter

        clientInsufficientDepositWithoutTA_R();

        clientConfirmationWithTA_R();
        clientConfirmationWithTA_R2();     // Take care of not to print insufficient deposit letter

        clientInsufficientDepositWithTA_R();

        //depositRequestLetter_R();

        clientTravelCredit();

        depositRequestLetterWithoutTA_R();
        depositRequestLetterWithTA_R();

        #endregion

        #region Itinerary Statement letter
        //ItineraryStatement();
        ProcessLetterItineraryStatement itineraryStatement = new ProcessLetterItineraryStatement(printLetter);
        /*
        try
        {
          resvNumberList = HttpContext.Current.Session["ResvNumberList"].ToString();
        }
        catch
        {
          resvNumberList = string.Empty;
        }
        */

        if (resvNo.Length > 0)
        {
          itineraryStatement.Process(itineraryID, resvNo, printTo);
        }
        #endregion

        /*
        #region Air Confirmation Letter
        airConfirmationLetter();

        #endregion

        #region Print Car Reservavation Confirmation

        CarReservationConfirmation();
        #endregion
        */

      }
      //catch (Exception ex)
      //{
      //  result = false;
      //  MessageBox.Show("Error has been occurred while printing letters. With error message: " + ex.Message);
      //}
      finally
      {
        // set this to true so that it will ask to select a printer
        //printLetter.FirstTime = true;
        result = string.Empty;
        //ReservationList.AddReservationToList(this.dsResv);
      }

      return result;

    }

    public string ProcessFrenchDepositRequest(string resvNo, string revisionTag, int printTo, int languageID)
    {
      string result = string.Empty;
      bool processRevision = true;

      this.resvNo = resvNo;
      this.revisionTag = revisionTag;
      this.printTo = printTo;
      this.letterData = new LetterData();
      this.dsResv = this.letterData.GetRevisionData(this.resvNo, this.revisionTag, languageID);
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        if (this.dsAirResv.Tables[0].Rows.Count == 0)
        {
          result = "There is no letter to print.";
          return result;
        }
        processRevision = false;
      }

      try
      {
        if (processRevision)
        {
          // clear reservation from the list.
          //ReservationList.ListOfReservations.Clear();

          #region Print H Reservation
          /*
          clientConfirmationWithoutTA_H();

          clientConfirmationWithTA_H();
          */
          #endregion

          #region Print M Reservation

          clientConfirmationWithoutTA_M();

          clientConfirmationWithoutTA_M2();  // deposit due is charged to a credit card.

          clientConfirmationWithTA_M();

          clientConfirmationWithTA_M2();   // Deposit due is charged to a credit card

          #endregion

          #region Print M Reservation for WSBH (VendorBase ID 7684)
          /*
          clientConfirmationWithoutTA_M_7684();

          clientConfirmationWithoutTA_M2_7684();  // deposit due is charged to a credit card.

          clientInsufficientDepositWithoutTA_M_7684();

          clientConfirmationWithTA_M_7684();

          clientConfirmationWithTA_M2_7684();   // Deposit due is charged to a credit card

          clientInsufficientDepositWithTA_M_7684();
          */
          #endregion

          #region H Reservation Deposit Request

          //depositRequestLetter_H();
          #endregion

          #region M Reservation Deposit Request

          //depositRequestLetter_M();
          #endregion

          #region M Reservation Deposit Request for WSBH (VendorBase ID 7684)

          depositRequestLetter_M_7684();
          #endregion

          #region Print R Reservations
          /*
          clientConfirmationWithoutTA_R();
          clientConfirmationWithoutTA_R2();   // Take care of not to print insufficient deposit letter

          depositRequestLetter_R();
          */
          #endregion
        }

      }
      //catch (Exception ex)
      //{
      //  result = false;
      //  MessageBox.Show("Error has been occurred while printing letters. With error message: " + ex.Message);
      //}
      finally
      {
        // set this to true so that it will ask to select a printer
        //printLetter.FirstTime = true;
        result = string.Empty;
        //ReservationList.AddReservationToList(this.dsResv);
      }

      return result;

    }

  }
}
