﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PreArrivalBoard
{
  public partial class OpenPDF : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      string fileName = string.Empty;

      if (!IsPostBack)
      {
        Response.ContentType = "Application/pdf";

        try
        {
          //Write the file directly to the HTTP content output stream.
          try
          {
            fileName = Session["FileName"].ToString();
          }
          catch
          {
            fileName = string.Empty;
          }

          if (fileName.Length > 0)
          {
            Response.WriteFile(fileName);
          }
          else
          {
            //Response.Redirect("~/WebEmail.aspx");
          }
        }
        finally
        {
          //end the buffer
          Response.End();
        }
      }
    }
  }
}
