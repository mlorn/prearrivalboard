﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using QuickRes.HelperClass;
using System.Web.Security;

namespace PreArrivalBoard
{
  public partial class Default : System.Web.UI.Page
  {
    ProcessLetterPreArrivalLetter preArrivalLetter;
    PrintLetter printLetter = new PrintLetter();
    private bool refreshState;
    private bool isRefresh;

    /// <summary>
    /// Loads the state of the view.
    /// </summary>
    /// <param name="savedState">State of the saved.</param>
    protected override void LoadViewState(object savedState)
    {
      object[] AllStates = (object[])savedState;
      base.LoadViewState(AllStates[0]);
      refreshState = bool.Parse(AllStates[1].ToString());
      if (Session["ISREFRESH"].ToString() != null && Session["ISREFRESH"].ToString() != string.Empty)
        isRefresh = (refreshState == (bool)Session["ISREFRESH"]);
    }

    protected override object SaveViewState()
    {
      Session["ISREFRESH"] = refreshState;
      object[] AllStates = new object[3];
      AllStates[0] = base.SaveViewState();
      AllStates[1] = !(refreshState);
      return AllStates;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

      if (!this.Page.User.Identity.IsAuthenticated)
      {
        FormsAuthentication.RedirectToLoginPage();
      }
      BindData();

    }

    protected void BindData()
    {
      DataTable dt;
      PreArrivalData preArrivalData;

      preArrivalData = new PreArrivalData();

      dt = preArrivalData.GetPreArrivalCommItinerary();
      grdItinerary.DataSource = dt;
      grdItinerary.DataBind();

      dt = preArrivalData.GetPreArrivalFinalItinerary();
      grdFinalItinerary.DataSource = dt;
      grdFinalItinerary.DataBind();
    }

    protected void grdReservation_BeforePerformDataSelect(object sender, EventArgs e)
    {
      Session["ItineraryID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void grdFinalResv_BeforePerformDataSelect(object sender, EventArgs e)
    {
      Session["ItineraryID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void lnkbtnClientEmail_Click(object sender, EventArgs e)
    {
      string clientEmail = string.Empty;
      int itineraryID = 0;
      string[] cmdArg;
      string result = string.Empty;
      string url = string.Empty;

      //LeadData leadData = new LeadData();
      // CommandArgument cotains both LeadID and ClientEmailAddress and seperated by :
      string temp = ((LinkButton)sender).CommandArgument;
      cmdArg = temp.Split(':');
      if (cmdArg.Length == 2)
      {
        itineraryID = Convert.ToInt32(cmdArg[0]);
        clientEmail = (cmdArg[1].ToString()).ToLower();
        Session["ClientEmail"] = clientEmail;
      }

      if (itineraryID > 0)
      {
        //clientID = leadData.GetClientID(leadID);
        Session["Itinerary"] = itineraryID;
        Session["ClientEmail"] = clientEmail;
        preArrivalLetter = new ProcessLetterPreArrivalLetter(printLetter);
        result = preArrivalLetter.Process(itineraryID.ToString(), Utility.PDF);
      }

      ScriptManager.RegisterStartupScript(this, this.GetType(), "Javascript", "javascript:ShowEmailWindow(); ", true);

      //if (result == string.Empty)
      //{
      //  //Response.Redirect("~/WebEmail.aspx");
      //  if (Properties.Settings.Default.IsTesting)
      //  {
      //    url = $"{Properties.Settings.Default.LetterSiteTestValue}/WebEmail.aspx";
      //  }
      //  else
      //  {
      //    url = $"{Properties.Settings.Default.LetterSite}/WebEmail.aspx";
      //  }
      //  ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);

      //}
      //else
      //{
      //  showMessage(result);
      //}
    }

    private void showMessage(string result)
    {
      Label lblMessage = new Label();
      lblMessage.Text = "<script language='javascript'>" + Environment.NewLine +
         "window.alert(" + "'" + result + "'" + ")</script>";

      // add the label to the page to display the alert
      Page.Controls.Add(lblMessage);
    }

    protected void lnkBtnRemove_Click(object sender, EventArgs e)
    {
      string temp = string.Empty;
      string[] stringValues;
      string resvNo = string.Empty;
      string kind = string.Empty;
      int itineraryID;
      DataTable dt;
      PreArrivalData preArrivalData = new PreArrivalData();
      if (isRefresh == false)
      {
        temp = ((LinkButton)sender).CommandArgument;
        itineraryID = Convert.ToInt32(temp);

        preArrivalData.RemoveItinerary(itineraryID);
        dt = preArrivalData.GetPreArrivalCommItinerary();
        grdItinerary.DataSource = dt;
        grdItinerary.DataBind();
        UpdatePanel1.Update();
      }
    }

    protected void lnkBtnClientName_Click(object sender, EventArgs e)
    {
      string temp = string.Empty;
      string resvNo = string.Empty;
      string kind = string.Empty;
      string clientID;
      DataTable dt;
      PreArrivalData preArrivalData = new PreArrivalData();
      if (isRefresh == false)
      {
        temp = ((LinkButton)sender).CommandArgument;
        clientID = temp.ToString();

        jumpToOtherSite("C", clientID);
      }
    }

    /// <summary>
    /// Jumps to other site.
    /// </summary>
    /// <param name="refType">Type of the ref.</param>
    /// <param name="refNumber">The ref number.</param>
    private void jumpToOtherSite(string refType, string refNumber)
    {
      BaseLogic.UserData ud;
      string userName = string.Empty;
      if (isRefresh == false)
      {
        try
        {
          ud = (BaseLogic.UserData)Session["UserData"];
        }
        catch
        {
          //TODO: Better handle exception
          ud.userID = 0;
          ud.emailAddress = string.Empty;
          ud.userName = string.Empty;
        }

        userName = ud.userName;
        string url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "','_blank');", true);
        //ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "','_blank',left=0,width=860);", true);
      }
      else
      {
        Response.Redirect(Request.RawUrl);
      }
    }

    /// <summary>
    /// Creates the URL for Resv No.
    /// </summary>
    /// <param name="refType">Type of the ref.</param>
    /// <param name="refNumber">The ref number.</param>
    /// <returns></returns>
    private string CreateURL(string refType, string refNumber)
    {
      string userName;
      string url = string.Empty;

      userName = Session["UserName"].ToString();
      url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
      return url;
    }

    protected void btnPrintLetterFinal_Click(object sender, EventArgs e)
    {
      string clientEmail = string.Empty;
      int itineraryID = 0;
      string[] cmdArg;
      string result = string.Empty;
      string url = string.Empty;

      //LeadData leadData = new LeadData();
      // CommandArgument cotains both LeadID and ClientEmailAddress and seperated by :
      string temp = ((LinkButton)sender).CommandArgument;
      //cmdArg = temp.Split(':');
      //if (cmdArg.Length == 2)
      //{
      //  itineraryID = Convert.ToInt32(cmdArg[0]);
      //  clientEmail = (cmdArg[1].ToString()).ToLower();
      //  Session["ClientEmail"] = clientEmail;
      //}

      //itineraryID = Convert.ToInt32(temp);

      //if (itineraryID > 0)
      //{
      //  //clientID = leadData.GetClientID(leadID);
      //  Session["Itinerary"] = itineraryID;
      //  Session["ClientEmail"] = clientEmail;
      //  preArrivalLetter = new ProcessLetterPreArrivalLetter(printLetter);
      //  result = preArrivalLetter.Process(itineraryID.ToString(), Utility.PDF);
      //}

      //ScriptManager.RegisterStartupScript(this, this.GetType(), "Javascript", "javascript:ShowEmailWindow(); ", true);

      //if (result == string.Empty)
      //{
      //  //Response.Redirect("~/WebEmail.aspx");
      //  if (Properties.Settings.Default.IsTesting)
      //  {
      //    url = $"{Properties.Settings.Default.LetterSiteTestValue}/WebEmail.aspx";
      //  }
      //  else
      //  {
      //    url = $"{Properties.Settings.Default.LetterSite}/WebEmail.aspx";
      //  }
      //  ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);

      //}
      //else
      //{
      //  showMessage(result);
      //}
    }

    /// <summary>
    /// Handles the Click event of the Email control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Email_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      LinkButton lnkbtnEmail = (sender as LinkButton);
      string clientID = Convert.ToString(lnkbtnEmail.CommandArgument);
      string email = lnkbtnEmail.Text;

      Session["ClientID"] = clientID;
      Session["EmailSendToAddress"] = email;

      Session["DID"] = null;
      Session["TemplateKindID"] = null;
      Session["FCCContactSourceID"] = null;
      Session["FCCDateInitial"] = null;
      Session["TemplateID"] = null;

      url = "sendemail.aspx?new=true";
      //window.open('sendemail.aspx?new=true, '_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 860, height = 400', '');
      //Response.Write("<script>window.open('" + url + "','_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 746, height = 660', '');</script>");
    }

  }
}