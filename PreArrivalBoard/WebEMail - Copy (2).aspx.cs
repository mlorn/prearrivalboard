﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Net.Mail;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Internal;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;
//using DevExpress.CodeParser;

namespace PreArrivalBoard
{
  public partial class WebEMail : System.Web.UI.Page
  {
    private bool testing = false;
    private XtraReport rpt = null;
    private XtraReport rptTA = null;
    private SmtpEmail mail = null;
    private DataSet dsResv;
    private LetterList letterList;
    private PrintLetter printLetter = new PrintLetter();
    private int printTo;
    Properties.Settings appSettings = new Properties.Settings();
    private string fileName = string.Empty;
    private string itineraryStatementName = "Itinerary Statement";
    Attachment att = null;
    List<MemoryStream> memList;
    GlobalSetting.UserData userData;
    string clientName = string.Empty;
    List<WimcoBaseLogic.BusinessLogic.UploadTicket> listOfTicket = new List<WimcoBaseLogic.BusinessLogic.UploadTicket>();
    List<WimcoBaseLogic.BusinessLogic.Recipient> listOfrecipient = new List<WimcoBaseLogic.BusinessLogic.Recipient>();
    List<string> listOfXheader = new List<string>();

    public string TemplateKindID { get; set; }
    public string EmailOriginal { get; set; }
    public string TemplateID;

    private int GetUserID()
    {
      int userID = 0;

      Properties.Settings appSetting = new Properties.Settings();

      try
      {
        testing = (bool)Session["Testing"];
      }
      catch
      {
        //TODO: Better handle exception
        testing = false;
      }

      if (appSetting.IsTesting || testing)
      {
        userID = 1;
      }
      else
      {
        GlobalSetting.UserData ud;
        try
        {
          ud = (GlobalSetting.UserData)Session["UserData"];
        }
        catch
        {
          //TODO: Better handle exception
          ud.userID = 1;
          ud.emailAddress = string.Empty;
        }

        userID = ud.userID;
      }

      return userID;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      int userID = 0;

      //if (!IsPostBack)
      //{

      if (TemplateKindID == null)
      {
        TemplateKindID = "5"; //Generic Letter Template.  I don't know if they will be templates specific to letters or not with additional template kinds
      }
      PopulateTemplateDDL(TemplateKindID);
      //}

      userID = GetUserID();

      DataSet dsTemplates = WimcoBaseLogic.BaseLogic.AdHoc("Exec spGetUserEmailTemplatesByKind @TemplateKindID = 4, @UserID= " + userID.ToString());

      //DataSet dsTemplates = BaseLogic.AdHoc("Exec spGetUserEmailTemplatesByKind @TemplateKindID = 4, @UserID= " + userID);
      if (dsTemplates.Tables.Count > 0)
      {
        DataTable dtTemplates = dsTemplates.Tables[0];
        ddlSnippets.Items.Clear();
        foreach (DataRow dr in dtTemplates.Rows)
        {
          ListItem li = new ListItem();
          if (dr["Bias"].ToString() == "1")
          {
            if (dr["TemplateTextHTML"] != null)
            {
              String QT = dr["TemplateTextHTML"].ToString();
              if (QT.Length > 30)
              {
                li.Text = QT.Substring(0, 30) + "...";

              }
              else
              {
                li.Text = QT;
              }
              li.Value = QT;
              /*
              if (hfLastText.Value != "" && hfLastText.Value == li.Value && ddlSnippets.SelectedIndex <= 0)
              {
                ddlSnippets.SelectedItem.Selected = false;
                li.Selected = true;
                txtDump.Text = "";
              }
              */
              ddlSnippets.Items.Add(li);

            }
          }
        }
      }
    }

    protected void PopulateTemplateDDL(string TKID)
    {
      int userID = 0;

      userID = GetUserID();

      DataSet dsTemplates = WimcoBaseLogic.BaseLogic.AdHoc(string.Format("Exec spGetUserEmailTemplatesByKind @TemplateKindID = {0}, @UserID ={1}", TKID, userID));
      DataTable dtTemplates = new DataTable();
      dtTemplates = dsTemplates.Tables[0];
      ddlTemplates.Items.Clear(); // Clean out DDL

      for (int i = 0; i < dtTemplates.Rows.Count; i++)
      {
        ListItem li2 = new ListItem();
        li2.Value = dtTemplates.Rows[i][0].ToString(); //Populate the DDL
        if (dtTemplates.Rows[i][4] == null || dtTemplates.Rows[i][4].ToString() == "")
        {
          li2.Text = dtTemplates.Rows[i][6].ToString().Trim();
        }
        else { li2.Text = dtTemplates.Rows[i][6].ToString().Trim(); }

        li2.Selected = true;

        ddlTemplates.Items.Add(li2);

      }
      ListItem li = new ListItem();
      li.Value = "0";
      li.Text = "Select a Template";
      ddlTemplates.Items.Insert(0, li);
      if (Session["TemplateID"] != null) //Set the Selected Index to match the TemplateID or 0 if it's not there
      {
        ddlTemplates.SelectedValue = Session["TemplateID"].ToString();
      }
      else
      {
        ddlTemplates.SelectedIndex = 0;
      }
      ddlTemplates.DataBind();
      //upReply.Update();

    }


    protected void btnQT_Click(object sender, EventArgs e)
    {
      int userID = 0;

      String fullText = txtQT.Text;
      String shorterText = txtQT.Text;

      if (txtQT.Text.Length == 0)
      {
        txtQT.BorderColor = System.Drawing.Color.Red;
        txtQT.BorderWidth = 2;
        return;
      }
      else
      {
        txtQT.BorderColor = System.Drawing.Color.Gray;
        txtQT.BorderWidth = 1;
      }

      if (fullText.Length > 50)
      {
        shorterText = fullText.Substring(0, 49);
      }

      userID = GetUserID();

      String EmailTemplateID = WimcoBaseLogic.BaseLogic.GetNextKey("EmailTemplate").ToString();
      String EmailTemplateRefID = WimcoBaseLogic.BaseLogic.GetNextKey("EmailTemplateUserRef").ToString();
      sdsQT.InsertParameters[0].DefaultValue = EmailTemplateID;
      sdsQT.InsertParameters[1].DefaultValue = "4";
      sdsQT.InsertParameters[2].DefaultValue = fullText;
      sdsQT.InsertParameters[3].DefaultValue = "True";
      sdsQT.InsertParameters[4].DefaultValue = DateTime.Now.ToString();
      sdsQT.InsertParameters[5].DefaultValue = shorterText;
      sdsQT.InsertParameters[6].DefaultValue = EmailTemplateRefID;
      sdsQT.InsertParameters[7].DefaultValue = "14";
      sdsQT.InsertParameters[8].DefaultValue = userID.ToString();
      sdsQT.InsertParameters[9].DefaultValue = shorterText;
      sdsQT.Insert();
      txtQT.Text = "";
      hfLastText.Value = fullText;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
      string clientEmail = string.Empty;
      LetterData letterData;
      string resvNo = string.Empty;
      //string clientName = string.Empty;
      string subject = string.Empty;
      printTo = Utility.Screen;

      /*
      if (RenderUtils.Browser.IsChrome || RenderUtils.Browser.IsFirefox)
      {
        htmlEditorWebEMail.Toolbars[0].Items.FindByCommandName("paste").Visible = false;
        htmlEditorWebEMail.Toolbars[0].Items.FindByCommandName("cut").Visible = false;
        htmlEditorWebEMail.Toolbars[0].Items.FindByCommandName("copy").Visible = false;
      }
      */

      /*
      rpt = (XtraReport)Session["ReportObject"];


      try
      {
        rpt = (XtraReport)Session["ReportObject"];
      }
      catch
      {
        //TODO: better handle exception
      }


      //rptTA = (XtraReport)Session["ReportObjectTA"];

      try
      {
        rptTA = (XtraReport)Session["ReportObjectTA"];
      }
      catch
      {
        //TODO: better handle exception
      }

      if (rpt == null && rptTA == null)
      {
        showMessage("There is a problem creating the report.");
        return;      
      }
      */

      Page.Header.DataBind();

      if (!IsPostBack)
      {

        //mail = new SmtpEmail();
        //Session["Mail"] = mail;
        //List<MemoryStream> memList = new List<MemoryStream>();

        //Saves the Previous page url in ViewState
        //ViewState["PreviousPage"] = Request.UrlReferrer;
        grdLetterList.DataSource = Session["FileList"] as List<LetterList>;
        grdLetterList.DataBind();

        try
        {
          clientEmail = Session["ClientEmail"].ToString();
        }
        catch
        {
          clientEmail = string.Empty;
        }

        try
        {
          resvNo = Session["ReservationNumber"].ToString();
        }
        catch
        {
          resvNo = string.Empty;
        }

        try
        {
          clientName = Session["ClientName"].ToString();
        }
        catch
        {
          clientName = string.Empty;
        }

        subject = string.Format("{0}, Reservation {1}", clientName, resvNo);

        letterData = new LetterData();
        //htmlEditorWebEMail.Html = letterData.GetEmailSignature(1);
        GlobalSetting.UserData ud;
        try
        {
          Properties.Settings appSetting;
          appSetting = new Properties.Settings();

          try
          {
            ud = (GlobalSetting.UserData)Session["UserData"];
          }
          catch
          {
            //TODO: Better handle exception
            ud.userID = 1;
            ud.emailAddress = string.Empty;
          }

          try
          {
            testing = (bool)Session["Testing"];
          }
          catch
          {
            //TODO: Better handle exception
            testing = false;
          }

          if (appSetting.IsTesting || testing)
          {
            txtFromEmail.Text = "mlorn@wimco.com";
            //txtSubject.Text = rpt.Tag.ToString();
            //txtBCEmail.Text = "mlorn@wimco.com";
            txtToEmail.Text = "mlorn@wimco.com";
            //lblUserID.Text = "1";
            ud.userID = 1;
          }
          else
          {
            txtFromEmail.Text = ud.emailAddress;
            //txtSubject.Text = rpt.Tag.ToString();
            //txtBCEmail.Text = ud.emailAddress;
            txtToEmail.Text = clientEmail;
          }


          txtSubject.Text = subject;
          /*
          if (rpt != null)
          {
            txtAttachment.Text = rpt.Tag.ToString();
            chkRpt.Value = true;
          }
          else
          {
            chkRpt.Value = false;
          }

          if (rptTA != null)
          {
            txtAttachmentTA.Text = rptTA.Tag.ToString();
            chkRptTA.Value = true;
          }
          else
          {
            chkRptTA.Value = false;
          }
          */
          //txtCCEmail.Text = "mlorn@wimco.com";
          //letterData = new LetterData();
          htmlEditorWebEMail.Html = letterData.GetEmailSignature(ud.userID);
        }
        catch
        {

        }

      }
      /*
      else
      {
        txtSubject.Text = rpt.Tag.ToString();
      }
      */
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
      string temp = "testing";
    }
    protected void htmlEditorWebEMail_CustomDataCallback(object sender, DevExpress.Web.CustomDataCallbackEventArgs e)
    {
      switch (e.Parameter)
      {
        case "SendEmail":
          SendEmail();
          break;
      }
    }

    private void SaveHtml(string html)
    {
      GlobalSetting.UserData ud;

      try
      {
        ud = (GlobalSetting.UserData)Session["UserData"];
      }
      catch
      {
        ud.userID = 0;
      }

      if (ud.userID > 0)
      {
        bool result = false;
        LetterData letterData;
        letterData = new LetterData();
        result = letterData.SaveSignature(ud.userID, html);
      }

    }


    protected void txtBCEMail_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
      SendEmailGroupWise();
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('The email has been sent sucessfully');", true);
      //ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "closePage", "window.close();", true);
    }

    /// <summary>
    /// Send an email via GroupWise API.
    /// </summary>
    private void SendEmailGroupWise()
    {
      CheckBox attached = null;
      Label lblLetterName = null;
      Label lblResvNumber = null;
      Label balanceDue = null;
      string resvNo = string.Empty;
      bool uploadSuccessed = false;
      string clientEmail = string.Empty;

      // reset the message list because it's defined as static variable
      MessageList.ListOfMessages.Clear();

      string fileType = string.Empty;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string physicalFileName = string.Empty;
      List<string> fileNames = new List<string>();

      string fileName;
      string letterName = string.Empty;
      string ticketID = string.Empty;

      int fileID;
      int fileSize;
      byte[] fileContent;

      string result = string.Empty;
      List<MessageList> listOfMessages = new List<MessageList>();

      int userID;
      string userName;
      string clientLastName = string.Empty;

      string subject = string.Empty;

      Properties.Settings appSetting;
      appSetting = new Properties.Settings();
      //List<WimcoBaseLogic.BusinessLogic.UploadTicket> listOfTicket = new List<WimcoBaseLogic.BusinessLogic.UploadTicket>();

      userID = 11086; // wimco user
      userName = "WIMCO";
      try
      {
        testing = (bool)Session["Testing"];
      }
      catch
      {
        //TODO: better handle exception
      }

      if (appSetting.IsTesting || testing)
      {
        userID = 1; // Mao
        userName = "Mao";
      }
      else
      {
        try
        {
          userData = (GlobalSetting.UserData)Session["UserData"];
          userID = userData.userID;
          userName = userData.username;
        }
        catch
        {
          //TODO: better handle exception
        }
      }

      try
      {
        clientLastName = Session["ClientLastName"].ToString();
      }
      catch
      {
        clientLastName = string.Empty;
      }

      // client last name length is greater than 20, truncates it 
      // because the field Message.ToContact is defined as varchar(20)
      if (clientLastName.Length > 20)
      {
        clientLastName = clientLastName.Remove(20, clientLastName.Length - 20);
      }

      memList = new List<MemoryStream>();

      // Sender Email
      string fromEmailAddres = txtFromEmail.Text.Trim();

      // To Email
      string toEmailAddress = txtToEmail.Text.Trim();
      if (toEmailAddress.Length > 0)
      {
        AddRecepient(toEmailAddress, WimcoBaseLogic.BusinessLogic.DistributionType.TO);
      }

      // CC Email
      string ccEmailAddress = txtCCEmail.Text.Trim();
      if (ccEmailAddress.Length > 0)
      {
        AddRecepient(ccEmailAddress, WimcoBaseLogic.BusinessLogic.DistributionType.CC);
      }

      // B Copy Email
      string bcEmailAddress = txtBCEmail.Text.Trim();
      if (bcEmailAddress.Length > 0)
      {
        AddRecepient(bcEmailAddress, WimcoBaseLogic.BusinessLogic.DistributionType.BC);
      }

      // subject line
      if (txtSubject.Text.Length > 0)
      {
        subject = txtSubject.Text;
      }
      else
      {
        if (rpt != null)
        {
          subject = rpt.Tag.ToString();
        }
        if (rptTA != null)
        {
          subject = rptTA.Tag.ToString();
        }
      }

      // process only if there is a recepient
      if (listOfrecipient.Count > 0)
      {
        try
        {
          //fileType = cmbSendFormat.SelectedItem.Value.ToString();
          fileType = "pdf";

          // check to see if each standard form being checked for sending
          if (chkPhoneByCheck.Checked)
          {
            // get file id from the emmmerator type
            fileID = (int)LetterType.CheckByPhoneForm;

            // get the leter info
            file = fileStorageData.GetFile(fileID);
            fileName = string.Format("{0}.{1}", file.FileName, fileType);

            //this.dsResv = HttpContext.Current.Session["RptDataset"] as DataSet;
            letterList = GetLetterData();
            if (letterList != null)
            {
              this.dsResv = printLetter.GetDataForForm(letterList, chkPhoneUseBalance.Checked);

              if (this.dsResv != null)
              {
                try
                {
                  // create a letter
                  rpt = printLetter.GetRpt(dsResv, file);

                  // export the letter to PDF in memory
                  MemoryStream mem = new MemoryStream();
                  rpt.ExportToPdf(mem);

                  // call to add an attachment and ticket string
                  AddAttachment(mem, fileName);
                }
                catch
                {
                  //TODO: better handle exception
                }
              }
            }
          }

          if (chkCcAuthorization.Checked)
          {
            fileID = (int)LetterType.CreditCardAuthorizationForm2;

            file = fileStorageData.GetFile(fileID);
            fileName = string.Format("{0}.pdf", file.FileName);

            letterList = GetLetterData();
            if (letterList != null)
            {
              this.dsResv = printLetter.GetDataForForm(letterList, chkCredidCardBalance.Checked);
              if (this.dsResv != null)
              {
                try
                {
                  // create a letter
                  rpt = printLetter.GetRpt(dsResv, file);

                  // export the letter to PDF in memory
                  MemoryStream mem = new MemoryStream();
                  rpt.ExportToPdf(mem);

                  // call to add an attachment and ticket string
                  AddAttachment(mem, fileName);
                }
                catch
                {
                  //TODO: better handle exception
                }
              }
            }
          }

          if (chkUSDTransfer.Checked)
          {
            try
            {
              fileID = (int)LetterType.WireTransferInstructionsUSD;

              file = fileStorageData.GetFile(fileID);
              fileName = string.Format("{0}.pdf", file.FileName);
              //call the method to get a report that does not require dataset.
              rpt = printLetter.GetRpt(file);

              // export the letter to PDF in memory
              MemoryStream mem = new MemoryStream();
              rpt.ExportToPdf(mem);

              // call to add an attachment and ticket string
              AddAttachment(mem, fileName);
            }
            catch
            {
              //TODO: Better handle exception
            }

          }

          if (chkEuroTransfer.Checked)
          {
            fileID = (int)LetterType.WireTransferInstructionsEuro;

            file = fileStorageData.GetFile(fileID);
            fileName = string.Format("{0}.pdf", file.FileName);

            rpt = printLetter.GetRpt(file);

            // export the letter to PDF in memory
            MemoryStream mem = new MemoryStream();
            rpt.ExportToPdf(mem);

            // call to add an attachment and ticket string
            AddAttachment(mem, fileName);
          }

          if (chkPoundTransfer.Checked)
          {
            fileID = (int)LetterType.WireTransferInstructionsStering;

            file = fileStorageData.GetFile(fileID);
            fileName = string.Format("{0}.pdf", file.FileName);
            //populate dataset with data
            //dsResv = printLetter.GetDataForForm();

            rpt = printLetter.GetRpt(file);

            // export the letter to PDF in memory
            MemoryStream mem = new MemoryStream();
            rpt.ExportToPdf(mem);

            // call to add an attachment and ticket string
            AddAttachment(mem, fileName);
          }

          // loop through the grid to see if any letter has been selected to be sent.
          for (int i = 0; i < grdLetterList.Rows.Count; i++)
          {
            lblLetterName = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
            fileName = lblLetterName.Text;
            //include file path
            fileNames.Add($@"{appSettings.LetterFolder}\{fileName}");

            // test to see if a letter has been selected.
            attached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");
            if (attached.Checked)
            {
              physicalFileName = $@"{appSettings.LetterFolder}\{fileName}";

              balanceDue = (Label)grdLetterList.Rows[i].Cells[2].FindControl("lblBalanceDue");
              lblResvNumber = (Label)grdLetterList.Rows[i].Cells[3].FindControl("lblResvNumber");
              resvNo = lblResvNumber.Text;

              // physicalFileName is pointed to a physical file on a server
              using (FileStream fs = new FileStream(physicalFileName, FileMode.Open, FileAccess.Read))
              {
                fileSize = Convert.ToInt32(fs.Length);
                fileContent = new byte[fileSize];
                fs.Read(fileContent, 0, fileSize);

                WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(fileContent, fileName);
                if (uploadTicket.Success)
                {
                  WimcoBaseLogic.BusinessLogic.UploadTicket ticket = new WimcoBaseLogic.BusinessLogic.UploadTicket();
                  ticket.Ticket = uploadTicket.Ticket;
                  ticket.Filename = fileName;
                  // the list of tickets will be added to a sending email
                  listOfTicket.Add(ticket);

                  letterName = fileName;
                  // remove (M132456).pdf) from letter name which starts with "(".  For example, Reservation Confirmation (M132456).pdf
                  int index = letterName.IndexOf("(") - 1;
                  letterName = letterName.Remove(index, letterName.Length - index);
                  //if it is not an Itinerary Statement letter, add a message to a list for creating a Message record to indicate
                  //a letter has been sent.
                  if (!letterName.Contains(itineraryStatementName))
                  {
                    // add a message to list which will be added to the Message table for each reservation number.
                    MessageList.AddMessageToList(listOfMessages, resvNo, $"Magnum - {letterName}", userID, userName, clientLastName, Utility.LetterSent);
                  }
                  uploadSuccessed = true;
                  listOfXheader.Add(string.Format("X-Wimco-LetterType={0}", letterName));
                }
              }
            }
          }

          //at less one letter has been attached successfully.
          if (uploadSuccessed)
          {
            if (resvNo.Length > 0)
            {
              listOfXheader.Add(string.Format("X-Wimco-ReservationNumber={0}", resvNo));
            }
          }

          // call to check if additional attachments have been uploaded
          AdditionalAttachment();

          WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn gwsr = new WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn();
          /*
          string html = htmlEditorWebEMail.Html;

          byte[] bytes = System.Text.Encoding.Default.GetBytes(html);
          html = System.Text.Encoding.UTF8.GetString(bytes);
          */

          // recipients(s)
          WimcoBaseLogic.BusinessLogic.Recipient[] recipients = new WimcoBaseLogic.BusinessLogic.Recipient[listOfrecipient.Count];
          int j = 0;
          foreach (var r in listOfrecipient)
          {
            recipients[j] = new WimcoBaseLogic.BusinessLogic.Recipient();
            recipients[j].email = r.email;
            recipients[j].displayName = string.Empty;
            recipients[j].distType = r.distType;
            recipients[j].recipType = r.recipType;
            j++;
          }

          //attachement via ticket(s) 
          WimcoBaseLogic.BusinessLogic.UploadTicket[] uploadTickets = new WimcoBaseLogic.BusinessLogic.UploadTicket[listOfTicket.Count];
          j = 0;
          // add a list of tickets to the Upload Ticket.
          foreach (var t in listOfTicket)
          {
            uploadTickets[j] = new WimcoBaseLogic.BusinessLogic.UploadTicket();
            uploadTickets[j].Ticket = t.Ticket;
            uploadTickets[j].Filename = t.Filename;
            j++;
          }

          listOfXheader.Add(string.Format("X-Wimco-AgentID={0}", userID));

          // xHeader(s)
          string[] xHeaders = listOfXheader.ToArray();

          // send the e-mail message via GroupWise
          gwsr = WimcoBaseLogic.BaseLogic.Groupwise_SendGWEmail(userID, subject, htmlEditorWebEMail.Html, string.Empty, recipients, xHeaders, uploadTickets);

          //if sent successfully, add a message to the Message table to indicate that a letter has been sent.
          if (gwsr.Success)
          {
            try
            {
              clientEmail = Session["ClientEmail"].ToString();
              if (fileNames.Count > 0)
              {
                foreach (var f in fileNames)
                {
                  if (File.Exists(f))
                  {
                    try
                    {
                      File.Delete(f);
                    }
                    catch
                    {
                      //TODO: better handle exception
                    }
                  }
                }
              }
            }
            catch
            {
              clientEmail = string.Empty;
            }

            //clientEmail = "mlorn@wimco.com";
            // update the Message Table an letter has been sent to a client only, reset letter (remove) if sending to a client email.
            if (clientEmail == txtToEmail.Text)
            {
              // add message to the Message table
              LetterDataBridge letterDataBridge = new LetterDataBridge();
              result = letterDataBridge.AddMessage(listOfMessages);
              letterDataBridge.ResetResvAgentLetter(listOfMessages);
            }
            //showMessage("The email has been sent.");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('The email has been sent sucessfully');", true);
            //ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "closePage", "window.close();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePage", "window.close();", true);
          }
          else
          {
            // Failed. Why?
            string ErrMsg = gwsr.Message;
            int StatusCode = gwsr.StatusCode;
            // slack it!
            WimcoBaseLogic.BaseLogic.SendSlackDebugMessage("GW Email Send Fail: " + ErrMsg + "  StatusCode: " + StatusCode.ToString());
            showMessage("GW Email Send Fail: " + ErrMsg + "  StatusCode: " + StatusCode.ToString());
            lblErrorMessage.Text = "GW Email Send Fail: " + ErrMsg + "  StatusCode: " + StatusCode.ToString();
          }
        }
        catch (Exception ex)
        {
          showMessage("There is a problem with sending the email. Error Message: " + ex.Message);
          lblErrorMessage.Text = "There is a problem with sending the email. Error Message: " + ex.Message;
        }
        finally
        {
          // clear memory after the attaching
          foreach (MemoryStream mem in memList)
          {
            // Close the memory stream.
            mem.Close();
            mem.Flush();
          }
        }
      }
      else
      {
        showMessage("Please enter the To email and make sure that is a correct format.");
        lblErrorMessage.Text = "Please enter the To email and make sure that is a correct format.";
      }
    }

    /// <summary>
    /// Add recepient which can be TO email, CC email, BC email
    /// Keep track of emails that being added
    /// </summary>
    /// <param name="emailAddress">Email Address can contain multiple emails</param>
    /// <param name="distType">Distruction Type can be: TO, CC, or BC</param>
    private void AddRecepient(string emailAddress, WimcoBaseLogic.BusinessLogic.DistributionType distType)
    {
      WimcoBaseLogic.BusinessLogic.Recipient recipient;

      string[] emails;
      char[] seperator = { ';', ',' };
      string validedEmail = string.Empty;

      // email addresses in the input box are seperated by either ";" or ","
      emails = emailAddress.Split(seperator);
      foreach (var email in emails)
      {
        try
        {
          // use the MailAddress class contructor to validate an email address
          MailAddress validEmail = new MailAddress(email.Trim());
          validedEmail = validEmail.Address;
        }
        catch
        {
          //skip invalid email
          continue;
        }
        recipient = new WimcoBaseLogic.BusinessLogic.Recipient();
        recipient.email = validedEmail;
        recipient.displayName = string.Empty;
        recipient.distType = distType;
        recipient.recipType = WimcoBaseLogic.BusinessLogic.RecipientType.User;
        //keep track of emails
        listOfrecipient.Add(recipient);
      }
    }

    /// <summary>
    /// Add an attachment by reading a memory stream.
    /// Keep track of file be attached.
    /// Also, keep track of memory that being used.
    /// </summary>
    /// <param name="mem">Memory stream to be read.</param>
    /// <param name="fileName">File Name that will be shown as an attachment which includes file extention</param>
    private void AddAttachment(MemoryStream mem, string fileName)
    {
      int fileSize;
      byte[] fileContent;

      // read the letter from the memory stream for a attachment
      mem.Seek(0, System.IO.SeekOrigin.Begin);
      fileSize = Convert.ToInt32(mem.Length);
      fileContent = new byte[fileSize];
      mem.Read(fileContent, 0, fileSize);
      //add the letter to the attachment
      WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(fileContent, fileName);
      //if an attachment successfully, get a ticket and keep track of tickets
      if (uploadTicket.Success)
      {
        WimcoBaseLogic.BusinessLogic.UploadTicket ticket = new WimcoBaseLogic.BusinessLogic.UploadTicket();
        ticket.Ticket = uploadTicket.Ticket;
        ticket.Filename = fileName;
        // the list of tickets will be added to a sending email
        listOfTicket.Add(ticket);
        // add Xheader to indicate which form has been sent, also remove a file extention
        listOfXheader.Add(string.Format("X-Wimco-FormSent={0}", fileName.Remove(fileName.Length - 4, 4)));
      }
      // keep track of memory being used, so that it can be destroyed later
      memList.Add(mem);
    }

    /// <summary>
    /// Allow reservation Agent to attach other files other then letters
    /// </summary>
    private void AdditionalAttachment()
    {
      int fileSize;
      byte[] fileContent;

      try
      {
        foreach (var f in fuAdditionalAttachment.UploadedFiles)
        {
          // only attach if a file has been uploaded.

          fileName = Path.GetFileName(f.PostedFile.FileName);
          if (fileName.Length > 0)
          {
            Stream uploadFile = f.PostedFile.InputStream;
            fileSize = Convert.ToInt32(uploadFile.Length);

            if (fileSize > 26214400) // bigger that 25mb? CHECK THIS ON UI SIDE.
            {
              // skip the file
              continue;
            }

            fileContent = new byte[fileSize];
            uploadFile.Read(fileContent, 0, fileSize);

            WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(fileContent, fileName);
            if (uploadTicket.Success)
            {
              WimcoBaseLogic.BusinessLogic.UploadTicket ticket = new WimcoBaseLogic.BusinessLogic.UploadTicket();
              ticket.Ticket = uploadTicket.Ticket;
              ticket.Filename = fileName;
              // the list of tickets will be added to a sending email
              listOfTicket.Add(ticket);
            }
          }
        }
      }
      catch
      {
        //TODO: better handle exception
      }
    }

    /// <summary>
    /// Send an email via SMTP
    /// </summary>
    private void SendEmail()
    {
      CheckBox attached = null;
      Label lblLetterName = null;
      Label lblResvNumber = null;
      Label balanceDue = null;

      // reset the message list because it's defined as static variable
      MessageList.ListOfMessages.Clear();

      string fileType = string.Empty;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string fileName;
      string letterName = string.Empty;
      int fileID;
      string result = string.Empty;
      List<MessageList> listOfMessages = new List<MessageList>();
      try
      {
        userData = (GlobalSetting.UserData)Session["UserData"];
      }
      catch
      {
        //TODO: better handle exception
      }

      int userID;
      string userName;
      string clientLastName = string.Empty;

      userID = userData.userID;
      userName = userData.username;

      try
      {
        clientLastName = Session["ClientLastName"].ToString();
      }
      catch
      {
        clientLastName = string.Empty;
      }

      // client last name length is greater than 20, truncate it 
      // because the field Message.ToContact is defined as varchar(20)
      if (clientLastName.Length > 20)
      {
        clientLastName = clientLastName.Remove(20, clientLastName.Length - 20);
      }

      List<MemoryStream> memList = new List<MemoryStream>();
      try
      {
        //fileType = cmbSendFormat.SelectedItem.Value.ToString();
        fileType = "PDF";

        mail = new SmtpEmail();
        //Session["Mail"] = mail;

        //mail = Session["Mail"] as SmtpEmail;
        if (mail != null)
        {
          //stores a list of memory stream
          //memList = new List<MemoryStream>();

          // Create a new memory stream and export the report into it as PDF.
          //MemoryStream mem = new MemoryStream();

          //switch (fileType)
          //{
          //  case "pdf":
          //    rpt.ExportToPdf(mem);
          //    break;
          //  case "xls":
          //    rpt.ExportToXls(mem);
          //    break;
          //  case "xlsx":
          //    rpt.ExportToXlsx(mem);
          //    break;
          //  case "rtf":
          //    rpt.ExportToRtf(mem);
          //    break;
          //  case "mht":
          //    rpt.ExportToMht(mem);
          //    break;
          //  case "txt":
          //    rpt.ExportToText(mem);
          //    break;
          //  case "csv":
          //    rpt.ExportToCsv(mem);
          //    break;
          //  case "png":
          //    rpt.ExportToImage(mem);
          //    break;
          //  default:
          //    rpt.ExportToPdf(mem);
          //    break;
          //}

          /*
          if (rpt != null)
          {
            if (chkRpt.Checked)
            {
              MemoryStream mem = new MemoryStream();
              rpt.ExportToPdf(mem);

              mem.Seek(0, System.IO.SeekOrigin.Begin);

              att = new Attachment(mem, string.Format("{0}.{1}", txtAttachment.Text, fileType), string.Format("application/{0}", fileType));
              mail.AttachedFiles.Add(att);
              // keep track of memory being used
              memList.Add(mem);
            }
          }


          if (rptTA != null)
          {
            if (chkRptTA.Checked)
            {
              MemoryStream mem = new MemoryStream();
              rptTA.ExportToPdf(mem);

              mem.Seek(0, System.IO.SeekOrigin.Begin);

              att = new Attachment(mem, string.Format("{0}.{1}", txtAttachmentTA.Text, fileType), string.Format("application/{0}", fileType));
              mail.AttachedFiles.Add(att);
              // keep track of memory being used
              memList.Add(mem);
            }
          }
          */

          if (chkPhoneByCheck.Checked)
          {
            fileID = (int)LetterType.CheckByPhoneForm;

            file = fileStorageData.GetFile(fileID);
            fileName = file.FileName;

            //this.dsResv = HttpContext.Current.Session["RptDataset"] as DataSet;
            letterList = GetLetterData();
            if (letterList != null)
            {
              this.dsResv = printLetter.GetDataForForm(letterList);

              if (this.dsResv != null)
              {
                try
                {
                  rpt = printLetter.GetRpt(dsResv, file);

                  MemoryStream mem = new MemoryStream();
                  rpt.ExportToPdf(mem);

                  mem.Seek(0, System.IO.SeekOrigin.Begin);

                  att = new Attachment(mem, string.Format("{0}.{1}", lblCheckByPhoneForm.Text, fileType), string.Format("application/{0}", fileType));
                  mail.AttachedFiles.Add(att);
                  // keep track of memory being used
                  memList.Add(mem);
                }
                catch
                {
                  //TODO: better handle exception
                }
              }
            }
          }

          if (chkCcAuthorization.Checked)
          {
            fileID = (int)LetterType.CreditCardAuthorizationForm2;

            file = fileStorageData.GetFile(fileID);
            fileName = file.FileName;

            letterList = GetLetterData();
            if (letterList != null)
            {
              this.dsResv = printLetter.GetDataForForm(letterList);
              if (this.dsResv != null)
              {
                try
                {
                  rpt = printLetter.GetRpt(dsResv, file);

                  MemoryStream mem = new MemoryStream();
                  rpt.ExportToPdf(mem);

                  mem.Seek(0, System.IO.SeekOrigin.Begin);

                  att = new Attachment(mem, string.Format("{0}.{1}", lblCcAuthorizationForm.Text, fileType), string.Format("application/{0}", fileType));
                  mail.AttachedFiles.Add(att);
                  // keep track of memory being used
                  memList.Add(mem);
                }
                catch
                {
                  //TODO: better handle exception
                }
              }
            }
          }

          if (chkUSDTransfer.Checked)
          {
            try
            {
              fileID = (int)LetterType.WireTransferInstructionsUSD;

              file = fileStorageData.GetFile(fileID);
              fileName = file.FileName;
              //call the method to get a report that does not require dataset.
              rpt = printLetter.GetRpt(file);

              MemoryStream mem = new MemoryStream();
              rpt.ExportToPdf(mem);

              mem.Seek(0, System.IO.SeekOrigin.Begin);

              att = new Attachment(mem, string.Format("{0}.{1}", lblUSDTransfer.Text, fileType), string.Format("application/{0}", fileType));
              mail.AttachedFiles.Add(att);
              // keep track of memory being used
              memList.Add(mem);
            }
            catch
            {
              //TODO: Better handle exception
            }

          }

          if (chkEuroTransfer.Checked)
          {
            fileID = (int)LetterType.WireTransferInstructionsEuro;

            file = fileStorageData.GetFile(fileID);
            fileName = file.FileName;

            rpt = printLetter.GetRpt(file);

            MemoryStream mem = new MemoryStream();
            rpt.ExportToPdf(mem);

            mem.Seek(0, System.IO.SeekOrigin.Begin);

            att = new Attachment(mem, string.Format("{0}.{1}", lblEuroTransfer.Text, fileType), string.Format("application/{0}", fileType));
            mail.AttachedFiles.Add(att);
            // keep track of memory being used
            memList.Add(mem);
          }

          if (chkPoundTransfer.Checked)
          {
            fileID = (int)LetterType.WireTransferInstructionsStering;

            file = fileStorageData.GetFile(fileID);
            fileName = file.FileName;
            //populate dataset with data
            //dsResv = printLetter.GetDataForForm();

            rpt = printLetter.GetRpt(file);

            MemoryStream mem = new MemoryStream();
            rpt.ExportToPdf(mem);

            mem.Seek(0, System.IO.SeekOrigin.Begin);

            att = new Attachment(mem, string.Format("{0}.{1}", lblPoundTransfer.Text, fileType), string.Format("application/{0}", fileType));
            mail.AttachedFiles.Add(att);
            // keep track of memory being used
            memList.Add(mem);
          }


          // Specify sender and recipient options for the e-mail message.

          //mail.From = new MailAddress(txtFromEmail.Text);
          //mail.To.Add(new MailAddress(txtToEmail.Text));
          //mail.CC.Add(new MailAddress(txtCCEmail.Text));
          //mail.Bcc.Add(new MailAddress(txtBCEmail.Text));

          mail.FromEmailAddres = txtFromEmail.Text;
          mail.ToEmailAddress = txtToEmail.Text;
          mail.CcEmailAddress = txtCCEmail.Text;
          if (txtBCEmail.Text.Length > 0)
          {
            mail.BccEmailAddress = string.Format("{0}; {1}", txtBCEmail.Text, txtFromEmail.Text);
          }
          else
          {
            mail.BccEmailAddress = txtFromEmail.Text;
          }
          // Specify other e-mail options.
          if (txtSubject.Text.Length > 0)
          {
            mail.Subject = txtSubject.Text;
          }
          else
          {
            if (rpt != null)
            {
              mail.Subject = rpt.Tag.ToString();
            }
            if (rptTA != null)
            {
              mail.Subject = rptTA.Tag.ToString();
            }
          }
          mail.IsBodyHtml = true;
          mail.MessageBody = htmlEditorWebEMail.Html;

          for (int i = 0; i < grdLetterList.Rows.Count; i++)
          {
            attached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");
            if (attached.Checked)
            {
              lblLetterName = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
              fileName = string.Format(@"{0}\{1}", appSettings.LetterFolder, lblLetterName.Text);
              balanceDue = (Label)grdLetterList.Rows[i].Cells[2].FindControl("lblBalanceDue");
              lblResvNumber = (Label)grdLetterList.Rows[i].Cells[3].FindControl("lblResvNumber");
              /*
              if (letterName.Text.Contains(itineraryStatementName))
              {
                printLetter.FileList.RemoveAt(i);
                string resvNumberList = GetResvNumber();
                if (resvNumberList.Length > 2)
                {
                  ProcessLetterItineraryStatement itineraryStatement = new ProcessLetterItineraryStatement();
                  itineraryStatement.Process(resvNumberList, Utility.PDF);
                }
              }
              */

              att = new Attachment(fileName, string.Format("application/{0}", Utility.PDF));
              mail.AttachedFiles.Add(att);
              letterName = lblLetterName.Text;
              // remove (M132456).pdf) from letter name which starts with "("
              int index = letterName.IndexOf("(") - 1;
              letterName = letterName.Remove(index, letterName.Length - index);
              MessageList.AddMessageToList(listOfMessages, lblResvNumber.Text, string.Format("Magnum - {0}", letterName), userID, userName, clientLastName);
            }
          }

          if (fuAdditionalAttachment.UploadedFiles.Length > 0)
          {
            try
            {
              foreach (var f in fuAdditionalAttachment.UploadedFiles)
              {

                fileName = System.IO.Path.GetFileName(f.PostedFile.FileName);

                att = new Attachment(f.PostedFile.InputStream, fileName);

                mail.AttachedFiles.Add(att);
              }
            }

            catch
            {
            }
          }

          // Send the e-mail message via the specified SMTP server.
          result = mail.SendEmail();
          //if sent successfully, add a message to the Message table
          if (result == string.Empty)
          {
            // add message to the Message table
            LetterDataBridge letterDataBridge = new LetterDataBridge();
            result = letterDataBridge.AddMessage(listOfMessages);
          }

          //Session["ReportObject"] = null;
          //Session["ReportObjectTA"] = null;

          //showMessage("The email has been sent.");
          //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('The email has been sent sucessfully');", true);
          //ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "closePage", "window.close();", true);
          ScriptManager.RegisterStartupScript(this, this.GetType(), "closePage", "window.close();", true);
        }
        else
        {
          showMessage("Cannot send the email.");
        }

        //if (ViewState["PreviousPage"] != null)	//Check if the ViewState 
        ////contains Previous page URL
        //{
        //  Response.Redirect(ViewState["PreviousPage"].ToString());//Redirect to 
        //  //Previous page by retrieving the PreviousPage Url from ViewState.
        //}

        //showMessage("==" + cmbSendFormat.SelectedItem.Value.ToString());

      }
      catch (Exception ex)
      {
        showMessage("There is a problem with sending the report." + ex.Message);
      }
      finally
      {
        // clear memory after the attaching
        foreach (MemoryStream mem in memList)
        {
          // Close the memory stream.
          mem.Close();
          mem.Flush();
        }
      }

    }

    /*
    private void addMassage(List<MessageList> listOfMessages, string resvNumber, string messageText, int userID, string userName)
    {

      MessageList msg = new MessageList();
      msg.ResvNumber = resvNumber;
      // 2 - relate to reservation message
      msg.LinkType = Utility.ResvMessage;
      msg.MessageThreadNumber = string.Format("{0}-000", resvNumber);
      // 8 - letter has been printed
      msg.MessageKindID = Utility.LetterPrinted;
      // 5 - admin communication
      msg.MocID = Utility.AdminCommunication;
      //created by who
      msg.ByWho = userID;
      // 1 - WIMCO contact
      msg.FromWho = Utility.WimcoContact;
      // From contact person
      msg.FromContact = userName;
      // 2 - Client contact
      msg.ToWho = Utility.ClientContact;
      //to contact person
      msg.ToContact = userName;
      msg.Body = messageText;
      //msgList.MessageType = 
      // 8 - no transmittion is required
      msg.MessageStatusID = Utility.NoTransmittionRequired;
      // 205 - client Letter
      msg.EventKindID = Utility.ClientLetter;
      listOfMessages.Add(msg);

    }
    */

    private void showMessage(string result)
    {
      Label lblMessage = new Label();
      lblMessage.Text = "<script language='javascript'>" + Environment.NewLine +
         "window.alert(" + "'" + result + "'" + ")</script>";

      // add the label to the page to display the alert
      Page.Controls.Add(lblMessage);
    }

    protected void ASPxTextBox3_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnGoBack_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterMainViewer.aspx");
      Response.Redirect(url);
    }

    protected void btnEditSignature_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "Signature.aspx");
      Response.Redirect(url);
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
      //SendEmail();
      lblErrorMessage.Text = string.Empty;
      SendEmailGroupWise();
    }

    protected void ucAttachment_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
    {
      Attachment att = null;
      mail = Session["Mail"] as SmtpEmail;
      if (mail != null)
      {
        att = new Attachment(e.UploadedFile.FileContent, e.UploadedFile.FileName);
        mail.AttachedFiles.Add(att);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnPreviewCheckByPhone Form control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewCheckByPhone_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      //LetterList letterList;

      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.CheckByPhoneForm;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      try
      {

        letterList = GetLetterData();
        dsResv = printLetter.GetDataForForm(letterList, chkPhoneUseBalance.Checked);
        rptSelectedLetter = printLetter.GetRpt(dsResv, file);

        Session.Add("ReportObjectPreview", rptSelectedLetter);

        if (Properties.Settings.Default.IsTesting)
        {
          url = string.Format($"{Properties.Settings.Default.LetterSiteTestValue}/{"LetterPreview.aspx"}");
        }
        else
        {
          url = $"{Properties.Settings.Default.LetterSite}/{"LetterPreview.aspx"}";
        }

        //Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
      }
      catch
      {
        //TODO: better handle exception
      }
    }


    protected void chkCcAuthorization_CheckedChanged(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Handles the Click event of the btnPreviewUSD Wire transfer instructions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewUSD_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.WireTransferInstructionsUSD;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      rptSelectedLetter = printLetter.GetRpt(file);
      HttpContext.Current.Session.Add("ReportObjectPreview", rptSelectedLetter);

      if (Properties.Settings.Default.IsTesting)
      {
        url = string.Format($"{Properties.Settings.Default.LetterSiteTestValue}/{"LetterPreview.aspx"}");
      }
      else
      {
        url = $"{Properties.Settings.Default.LetterSite}/{"LetterPreview.aspx"}";
      }

      //HttpContext.Current.Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);

    }

    /// <summary>
    /// Handles the Click event of the btnPreviewCreditCard Authorization form control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewCreditCard_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      LetterList letterList;

      string fileName;
      int fileID;
      string url = string.Empty;
      string villaCode = string.Empty;

      fileID = (int)LetterType.CreditCardAuthorizationForm2;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      letterList = GetLetterData();
      dsResv = printLetter.GetDataForForm(letterList, chkCredidCardBalance.Checked);

      rptSelectedLetter = printLetter.GetRpt(dsResv, file);
      Session.Add("ReportObjectPreview", rptSelectedLetter);

      if (Properties.Settings.Default.IsTesting)
      {
        url = string.Format($"{Properties.Settings.Default.LetterSiteTestValue}/{"LetterPreview.aspx"}");
      }
      else
      {
        url = $"{Properties.Settings.Default.LetterSite}/{"LetterPreview.aspx"}";
      }

      //Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
    }

    /// <summary>
    /// Handles the Click event of the btnPreviewEuro Wire transfer instructions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewEuro_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.WireTransferInstructionsEuro;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      rptSelectedLetter = printLetter.GetRpt(file);
      HttpContext.Current.Session.Add("ReportObjectPreview", rptSelectedLetter);

      if (Properties.Settings.Default.IsTesting)
      {
        url = string.Format($"{Properties.Settings.Default.LetterSiteTestValue}/{"LetterPreview.aspx"}");
      }
      else
      {
        url = $"{Properties.Settings.Default.LetterSite}/{"LetterPreview.aspx"}";
      }

      //HttpContext.Current.Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);

    }

    /// <summary>
    /// Handles the Click event of the btnPreviewPound Wire transfer instructions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewPound_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.WireTransferInstructionsStering;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      rptSelectedLetter = printLetter.GetRpt(file);
      HttpContext.Current.Session.Add("ReportObjectPreview", rptSelectedLetter);

      if (Properties.Settings.Default.IsTesting)
      {
        url = string.Format($"{Properties.Settings.Default.LetterSiteTestValue}/{"LetterPreview.aspx"}");
      }
      else
      {
        url = $"{Properties.Settings.Default.LetterSite}/{"LetterPreview.aspx"}";
      }

      //HttpContext.Current.Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
    }

    protected void grdLetterList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      string url = string.Empty;
      int index;
      string letterSite = string.Empty;
      Label lblLetterName;

      //string resvNumberList = string.Empty;
      string letterName = string.Empty;

      if (e.CommandName == "Select")
      {
        index = Convert.ToInt32(e.CommandArgument);
        lblLetterName = (Label)grdLetterList.Rows[index].Cells[1].FindControl("lblLetterName");

        if (lblLetterName != null)
        {
          letterName = lblLetterName.Text;
          /*
          if (letterName.Contains("Itinerary Statement"))
          {
            //remove the previous Itinerary Statement
            printLetter.FileList.RemoveAt(index);
            //get a list of reservation numbers 
            resvNumberList = GetResvNumber();
            if (resvNumberList.Length > 2)
            {
              ProcessLetterItineraryStatement itineraryStatement = new ProcessLetterItineraryStatement();
              itineraryStatement.Process(resvNumberList, Utility.PDF);
              ViewLetter(letterName);
            }
          }
          else
          {
            ViewLetter(letterName);
          }
          */
          //ViewLetter(letterName);

          fileName = GetLetterFile(letterName);

          Session["FileName"] = fileName;
          if (Properties.Settings.Default.IsTesting)
          {
            url = $"{Properties.Settings.Default.LetterSiteTestValue}/{"OpenPDF.aspx"}";
          }
          else
          {
            url = $"{Properties.Settings.Default.LetterSite}/{"OpenPDF.aspx"}";
          }
          //Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
          ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
        }
      }
    }

    /// <summary>
    /// Handles the Click event of the Button1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Button1_Click(object sender, EventArgs e)
    {
      /*
      CheckBox attached = null;
      Label letterName = null;
      string temp;
      for (int i = 0; i < grdLetterList.Rows.Count; i++)
      {
        attached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");
        if (attached.Checked)
        {
          letterName = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
          temp = letterName.Text;
        }
      }
      */

      SendEmail();

    }

    public string GetLetterFile(string fileName)
    {
      Properties.Settings appSettings = new Properties.Settings();

      //fileName = MapPath((string.Format(@"{0}\{1}.pdf", appSettings.LetterFolder, fileName)));
      fileName = (string.Format(@"{0}\{1}", appSettings.LetterFolder, fileName));

      if (!File.Exists(fileName))
      {
        fileName = string.Empty;
      }

      return fileName;
    }

    private void ViewLetter(string letterName)
    {
      string fileName = string.Empty;

      Response.ContentType = "Application/pdf";


      try
      {
        //Write the file directly to the HTTP content output stream.
        try
        {

          fileName = GetLetterFile(letterName);
          if (fileName.Length > 0)
          {
            Response.WriteFile(fileName);
            //Response.Write("<script>window.open('+fileName+','_blank');</script>");
          }
          else
          {
            //Response.Redirect("~/WebEmail.aspx");
          }
        }
        catch
        {
          throw;
        }
      }
      finally
      {
        //end the buffer
        Response.End();
      }

    }

    protected void btnViewLetter_Click(object sender, EventArgs e)
    {
      string fileName = string.Empty;

      Response.ContentType = "Application/pdf";
      try
      {
        //Write the file directly to the HTTP content output stream.
        try
        {

          fileName = GetLetterFile("CheckByPhoneForm");
          //if a contract is found, open the contract form; otherwise, redirect to
          //the generate contract form
          if (fileName.Length > 0)
          {
            Response.WriteFile(fileName);
            //Response.Write("<script>window.open('+fileName+','_blank');</script>");
          }
          else
          {
            //Response.Redirect("~/WebEmail.aspx");
          }
        }
        catch
        {
          throw;
        }
      }
      finally
      {
        //end the buffer
        Response.End();
      }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      string result = string.Empty;
      string resvNo = string.Empty;
      int itinerayID = 0;
      //string resvNumberList = string.Empty;
      int letterKind;

      ProcessLetterRevision revision;
      ProcessLetterFinalPayment finalPayment;
      //ML - 07/06/2018 : commented out
      //ProcessLetterNonDeposit nonDeposit;
      ProcessLetterItineraryStatement itineraryStatement;
      ProcessLetterItineraryLetter itineraryLetter;
      ProcessLetterPreArrivalLetter preArrivalLetter;

      try
      {
        resvNo = Session["ReservationNumber"].ToString();
      }
      catch
      {
        resvNo = string.Empty;
      }

      /*
      try
      {
        resvNumberList = Session["ResvNumberList"].ToString();
      }
      catch
      {
        resvNumberList = string.Empty;
      }
      */

      //resvNumberList = ";M62909;M71222;M72201;";

      //resvNo = "M56084";

      if (resvNo.Length > 0)
      {
        //delete all exiting files in the list.
        //printLetter.FileList.Clear();

        letterKind = Convert.ToInt32(rdbtnSelectLetterKind.Value);
        switch (letterKind)
        {
          case 1:
            revision = new ProcessLetterRevision(printLetter);
            result = revision.Process(resvNo, string.Empty, Utility.PDF, Utility.English);
            break;
          case 2:
            finalPayment = new ProcessLetterFinalPayment();
            result = finalPayment.Process(resvNo, string.Empty, string.Empty, Utility.PDF);
            break;
          // ML - 07/06/2018: comment out
          /*
          case 3:
            nonDeposit = new ProcessLetterNonDeposit();
            result = nonDeposit.Process(resvNo, string.Empty, string.Empty, Utility.PDF);
            break;
          */
          case 4:
            itineraryLetter = new ProcessLetterItineraryLetter(printLetter);
            printLetter.totalBalanceDue = 0;
            result = itineraryLetter.Process(itinerayID, resvNo, string.Empty, string.Empty, Utility.PDF);
            break;
          case 5:
            itineraryStatement = new ProcessLetterItineraryStatement(printLetter);
            result = itineraryStatement.Process(itinerayID, resvNo, Utility.PDF);
            break;
          case 6:
            preArrivalLetter = new ProcessLetterPreArrivalLetter(printLetter);
            result = preArrivalLetter.Process(resvNo, Utility.PDF);
            break;

          default:
            revision = new ProcessLetterRevision(printLetter);
            result = revision.Process(resvNo, string.Empty, Utility.PDF, Utility.English);
            break;
        }

        //if testing, use the WebEmail page in the current URL, instead of the production one (Nomad sub site).
        try
        {
          testing = (bool)Session["Testing"];
        }
        catch
        {
          //TODO: Better handle exception
          testing = false;
        }

        if (appSettings.IsTesting || testing)
        {
          Response.Redirect("~/WebEmail.aspx");
        }

        if (result == string.Empty)
        {
          url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "WebEMail.aspx");
          Response.Redirect(url);
          //grdLetterList.DataSource = printLetter.FileList;
          //grdLetterList.DataBind();
        }
        else
        {
          showMessage(result);
        }

      }
    }

    private string GetResvNumber()
    {
      CheckBox chkAttached = null;
      Label lblResvNumber = null;
      string result = ",";
      for (int i = 0; i < grdLetterList.Rows.Count; i++)
      {
        chkAttached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");
        if (chkAttached != null)
        {
          if (chkAttached.Checked)
          {
            lblResvNumber = (Label)grdLetterList.Rows[i].Cells[3].FindControl("lblResvNumber");
            if (lblResvNumber != null)
            {
              result += string.Format("{0},", lblResvNumber.Text);
            }
          }
        }
      }

      return result;
    }

    private void GetValue(int index)
    {
      Label lblTemp = null;
      string temp;

      lblTemp = (Label)grdLetterList.Rows[index].Cells[4].FindControl("lblResvNumber");
      if (lblTemp != null)
      {
        //if a reservation is already existing (added to the list), skip it.
        // also, skipping the adding total price and balance.
        // this logic to take care of resv with TA.  With TA, there are two letters.
        if (!letterList.ResvNumber.Contains(lblTemp.Text))
        {
          letterList.ResvNumber += string.Format("{0},", lblTemp.Text);
          lblTemp = (Label)grdLetterList.Rows[index].Cells[2].FindControl("lblTotalPrice");
          if (lblTemp != null)
          {
            letterList.TotalPrice += Convert.ToDecimal(lblTemp.Text);
          }

          lblTemp = (Label)grdLetterList.Rows[index].Cells[3].FindControl("lblBalanceAmt");
          if (lblTemp != null)
          {
            temp = lblTemp.Text.Trim(new Char[] { '$', '€', '£', '(', ')' }); //remove those charaters from a value if there is any

            letterList.BalanceAmt += Convert.ToDecimal(temp);
          }

          lblTemp = (Label)grdLetterList.Rows[index].Cells[3].FindControl("lblAmountDue");
          if (lblTemp != null)
          {
            temp = lblTemp.Text.Trim(new Char[] { '$', '€', '£', '(', ')' }); //remove those charaters from a value if there is any

            letterList.AmountDue += Convert.ToDecimal(temp);
          }

          lblTemp = (Label)grdLetterList.Rows[index].Cells[9].FindControl("lblVillaCode");
          if (lblTemp != null)
          {
            letterList.VillaCode += string.Format("{0},", lblTemp.Text);
          }
        }
      }

      lblTemp = (Label)grdLetterList.Rows[index].Cells[1].FindControl("lblResvCurrencySymbol");
      if (lblTemp != null)
      {
        letterList.ResvCurrencySymbol = lblTemp.Text;
      }


      /*
      lblTemp = (Label)grdLetterList.Rows[index].Cells[4].FindControl("lblResvNumber");
      if (lblTemp != null)
      {
        letterList.ResvNumber += string.Format("{0},", lblTemp.Text);
      }
      */

      lblTemp = (Label)grdLetterList.Rows[index].Cells[5].FindControl("lblResvAgent");
      if (lblTemp != null)
      {
        letterList.ResvAgent = lblTemp.Text;
      }

      lblTemp = (Label)grdLetterList.Rows[index].Cells[6].FindControl("lblDateFrom");
      if (lblTemp != null)
      {
        letterList.DateFrom = Convert.ToDateTime(lblTemp.Text);
      }
      lblTemp = (Label)grdLetterList.Rows[index].Cells[7].FindControl("lblDateTo");
      if (lblTemp != null)
      {
        letterList.DateTo = Convert.ToDateTime(lblTemp.Text);
      }

      lblTemp = (Label)grdLetterList.Rows[index].Cells[10].FindControl("lblFirstName");
      if (lblTemp != null)
      {
        letterList.FirstName = lblTemp.Text;
      }
      lblTemp = (Label)grdLetterList.Rows[index].Cells[11].FindControl("lblLastName");
      if (lblTemp != null)
      {
        letterList.LastName = lblTemp.Text;
      }
      lblTemp = (Label)grdLetterList.Rows[index].Cells[12].FindControl("lblClientName");
      if (lblTemp != null)
      {
        letterList.ClientName = lblTemp.Text;
      }

    }

    private LetterList GetLetterData()
    {
      letterList = new LetterList();
      int index;
      CheckBox chkAttached = null;
      Label lblTemp = null;
      bool letterIncluded = false;

      letterList.ResvNumber = string.Empty;
      letterList.VillaCode = string.Empty;

      // loop through grid view and get letter info
      for (int i = 0; i < grdLetterList.Rows.Count; i++)
      {
        chkAttached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");

        // get Itinerary Statement data, regardless checked or not checked
        lblTemp = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
        if (lblTemp != null)
        {
          // if it is the Itinerary Statement letter, get start and end travel dates of an itinerary
          if (lblTemp.Text.Contains(itineraryStatementName))
          {
            lblTemp = (Label)grdLetterList.Rows[i].Cells[6].FindControl("lblDateFrom");
            if (lblTemp != null)
            {
              letterList.DateFrom = Convert.ToDateTime(lblTemp.Text);
            }
            lblTemp = (Label)grdLetterList.Rows[i].Cells[7].FindControl("lblDateTo");
            if (lblTemp != null)
            {
              letterList.DateTo = Convert.ToDateTime(lblTemp.Text);
            }
          }
        }

        // if a letter is attached
        if (chkAttached.Checked)
        {
          lblTemp = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
          if (lblTemp != null)
          {
            if (!lblTemp.Text.Contains(itineraryStatementName))
            {
              letterIncluded = true;
              GetValue(i);
            }
          }
        }
      }

      // if no letter is attached, use info from the Itinerary Statement if there is one.
      if (!letterIncluded)
      {
        if (grdLetterList.Rows.Count > 0)
        {
          // Itinerary statement is always at end of the grid.
          index = grdLetterList.Rows.Count - 1;
          lblTemp = (Label)grdLetterList.Rows[index].Cells[1].FindControl("lblLetterName");
          if (lblTemp != null)
          {
            // if it is the Itinerary Statement letter, get start and end travel dates of an itinerary
            if (lblTemp.Text.Contains(itineraryStatementName))
            {
              GetValue(index);
            }
          }

        }
      }

      if (letterList.ResvNumber.Length > 0)
      {
        // remove the last comma
        letterList.ResvNumber = letterList.ResvNumber.Substring(0, letterList.ResvNumber.Length - 1);
      }
      if (letterList.VillaCode.Length > 0)
      {
        // remove the last comma
        letterList.VillaCode = letterList.VillaCode.Substring(0, letterList.VillaCode.Length - 1);
      }

      return letterList;
    }

    protected void rdbtnSelectLetterKind_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSaveAsTemplate_Click(object sender, EventArgs e)
    {
      pnlSaveTemplate.Visible = true;


      pnlSaveTemplate.Visible = true;
      DataSet ds = WimcoBaseLogic.BaseLogic.AdHoc("Select (Name + ' - ' + Description) as title, EmailTemplateKindID From EmailTemplateKind Order by EmailTemplateKindID");

      ddlTemplateKind.DataSource = ds;
      ddlTemplateKind.DataTextField = "title";
      ddlTemplateKind.DataValueField = "EmailTemplateKindID";
      ddlTemplateKind.DataBind();
      try
      {
        ddlTemplateKind.SelectedValue = Session["TemplateKindID"].ToString();
      }
      catch
      {
        ddlTemplateKind.SelectedValue = "5";
      } //3 is Generic
      Session["TemplateID"] = TemplateID;
      btnSaveAsTemplate.Visible = false;
      ddlTemplates.Visible = false;
      btnTemplate.Visible = false;
      PopulateTemplateDDL(ddlTemplateKind.SelectedValue);

    }

    protected void btnSaveTemplate_Click(object sender, EventArgs e)
    {
      int userID = 0;
      String TemplateKindID = "";

      try
      {
        TemplateKindID = ddlTemplateKind.SelectedValue;
      }
      catch
      {
        return;
      }

      userID = GetUserID();

      String EmailTemplateID = WimcoBaseLogic.BaseLogic.GetNextKey("EmailTemplate").ToString();
      String EmailTemplateRefID = WimcoBaseLogic.BaseLogic.GetNextKey("EmailTemplateUserRef").ToString();
      sdsTemplate.InsertParameters[0].DefaultValue = EmailTemplateID;
      sdsTemplate.InsertParameters[1].DefaultValue = TemplateKindID;
      sdsTemplate.InsertParameters[2].DefaultValue = htmlEditorWebEMail.Html;
      sdsTemplate.InsertParameters[3].DefaultValue = "True";
      sdsTemplate.InsertParameters[4].DefaultValue = DateTime.Now.ToString();
      sdsTemplate.InsertParameters[5].DefaultValue = txtNameTemplate.Text;
      sdsTemplate.InsertParameters[6].DefaultValue = EmailTemplateRefID;
      sdsTemplate.InsertParameters[7].DefaultValue = "14";
      sdsTemplate.InsertParameters[8].DefaultValue = userID.ToString();
      sdsTemplate.InsertParameters[9].DefaultValue = "";
      sdsTemplate.Insert();
      pnlSaveTemplate.Visible = false;
      btnSaveAsTemplate.Visible = true;
      ddlTemplates.Visible = true;
      btnTemplate.Visible = true;
      Session["TemplateID"] = EmailTemplateID;
    }

    protected void btnCancelTemplate_Click(object sender, EventArgs e)
    {
      pnlSaveTemplate.Visible = false;
      btnSaveAsTemplate.Visible = true;
      ddlTemplates.Visible = true;
      btnTemplate.Visible = true;
    }

    protected void btnTemplate_Click(object sender, EventArgs e)
    {
      String TemplateID = ddlTemplates.SelectedValue;
      if (TemplateID != "0")
      {
        DataSet dsTemplate = WimcoBaseLogic.BaseLogic.AdHoc("spGetUserEmailTemplate @EmailTemplateID = " + TemplateID);
        if (dsTemplate.Tables.Count > 0)
        {
          DataTable dtTemplate = dsTemplate.Tables[0];
          if (dtTemplate.Rows.Count > 0)
          {
            DataRow drTemplate = dtTemplate.Rows[0];

            htmlEditorWebEMail.Html = string.Format("{0} {1} {2}", drTemplate[1], Environment.NewLine, htmlEditorWebEMail.Html);
            //    txtEmailSubject.Text = drTemplate[2].ToString();  //I Think the subject is already written in by the letter form
          }
          else
          {

          }
        }
      }
    }

    protected void chkPhoneByCheck0_CheckedChanged(object sender, EventArgs e)
    {

    }
  }
}

