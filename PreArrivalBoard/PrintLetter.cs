using System;
using System.IO;
using System.Web;
using System.Net.Http;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using iText.Forms;
using iText.Forms.Fields;
using iText.Kernel.Pdf;
using DevExpress.XtraReports;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using QuickRes.DataAccessLayer;
using QuickRes.BusinessLayer;
using System.ComponentModel;
using QuickRes.HelperClass;
using QuickRes.LetterLayer;

namespace PreArrivalBoard
{

  public class PrintLetter
  {
    public bool FirstTime = true;
    public XtraReport MergedLetter = null;
    public bool RequireMergeFile = false;
    //public static List<LetterList> FileList = new List<LetterList>();
    public List<LetterList> fileList = new List<LetterList>();
    public string letterFolder = Properties.Settings.Default.LetterFolder;
    public double totalBalanceDue = 0;

    /// <summary>
    /// Prints the letter to a screen or a printer
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="printTo">The print to.</param>
    public void PrintTo(int fileID, DataSet dsResv, int printTo)
    {
      switch (printTo)
      {
        case Utility.Screen:
          ToScreen(fileID, dsResv, false);
          break;
        case Utility.Printer:
          ToPrinter(fileID, dsResv, false);
          break;
        case Utility.PDF:
          ToPDF(fileID, dsResv);
          break;
        case Utility.EMail:
          ToEMail(fileID, dsResv);
          break;
        default:
          //MessageBox.Show(string.Format("Print to option {0} is not valid.", printTo.ToString()), "Invalid Print To",
          //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
          break;
      }
    }

    /// <summary>
    /// Prints the letter to a screen or a printer
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="printTo">The print to.</param>
    public void PrintTo(string letterName, DataSet dsResv, int printTo)
    {
      switch (printTo)
      {
        //case Utility.Screen:
        //  ToScreen(fileID, dsResv, false);
        //  break;
        //case Utility.Printer:
        //  ToPrinter(fileID, dsResv, false);
        //  break;
        case Utility.PDF:
          ToPDF(letterName, dsResv);
          break;
        //case Utility.EMail:
        //  ToEMail(fileID, dsResv);
        //  break;
        default:
          //MessageBox.Show(string.Format("Print to option {0} is not valid.", printTo.ToString()), "Invalid Print To",
          //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
          break;
      }
    }

    /// <summary>
    /// Prints the letter to a screen or a printer
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="printTo">The print to.</param>
    public void PrintTo(int fileID, DataSet dsResv, int printTo, bool printDuplex)
    {
      switch (printTo)
      {
        case Utility.Screen:
          ToScreen(fileID, dsResv, printDuplex);
          break;
        case Utility.Printer:
          ToPrinter(fileID, dsResv, printDuplex);
          break;
        case Utility.PDF:
          ToPDF(fileID, dsResv);
          break;
        case Utility.EMail:
          ToEMail(fileID, dsResv);
          break;
        default:
          //MessageBox.Show(string.Format("Print to option {0} is not valid.", printTo.ToString()), "Invalid Print To",
          //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
          break;
      }
    }

    public void PrintTo(int fileID, DataSet dsResv, int printTo, bool printDuplex, bool TA)
    {
      switch (printTo)
      {
        case Utility.Screen:
          ToScreen(fileID, dsResv, printDuplex, TA);
          break;
        case Utility.Printer:
          ToPrinter(fileID, dsResv, printDuplex);
          break;
        case Utility.PDF:
          ToPDF(fileID, dsResv);
          break;
        case Utility.EMail:
          ToEMail(fileID, dsResv);
          break;
        default:
          //MessageBox.Show(string.Format("Print to option {0} is not valid.", printTo.ToString()), "Invalid Print To",
          //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
          break;
      }
    }

    public void setPDFOption(XtraReport rpt, string fileName)
    {
      if (rpt == null)
      {
        return;
      }

      PdfExportOptions pdfOptions = rpt.ExportOptions.Pdf;
      pdfOptions.Compressed = true;
      pdfOptions.ImageQuality = PdfJpegImageQuality.Low;
      pdfOptions.NeverEmbeddedFonts = "Tahoma;Courier New";
      pdfOptions.DocumentOptions.Application = "Nomad Letter";
      pdfOptions.DocumentOptions.Author = Utility.UserName;
      pdfOptions.DocumentOptions.Keywords = "WIMCO, www.wimco.com, Caribbean, European, Villa, Hotel";
      pdfOptions.DocumentOptions.Subject = "Letter";
      pdfOptions.DocumentOptions.Title = fileName;
      ExportOptions exportOption = rpt.ExportOptions;
      //exportOption.PrintPreview.DefaultFileName = "Testing";
    }

    public List<LetterList> WriteToPDF(XtraReport rpt, string fileName, LetterList letterList)
    {
      string pdfFile = string.Empty;

      //LetterList letterList = new LetterList();

      setPDFOption(rpt, fileName);

      try
      {
        //Environment.SpecialFolder.ApplicationData
        /*
        DirectoryInfo dirInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Letter");
        // Determine whether the directory exists.
        if (!dirInfo.Exists)
        {
          // Try to create the directory.
          dirInfo.Create();
        }
        */
        pdfFile = string.Format(@"{0}\{1}", letterFolder, fileName);

        rpt.ExportToPdf(pdfFile);

        letterList.FilePath = letterFolder;
        letterList.LetterName = fileName;

        //letterList.letterPath = letterFolder;
        //letterList.ResvNumber = resvNo;
        //letterList.TotalPrice = totalPrice;
        //letterList.BalanceDue = amountDue;

        /*
        if (fileName.Contains("Itinerary Statement"))
        {
          letterList.BalanceDue = PrintLetter.totalBalanceDue;
          PrintLetter.totalBalanceDue = 0;
        }
        else
        {
          PrintLetter.totalBalanceDue += balanceDue;
        }
        */
        fileList.Add(letterList);
      }
      catch (Exception e)
      {
        //MessageBox.Show(string.Format("Could not write a letter to a file: {0}", e.ToString()), "Creating Error",
        //  MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
      }

      HttpContext.Current.Session["FileList"] = fileList;

      return fileList;
    }


    public List<LetterList> WriteToPDF(XtraReport rpt, string fileName)
    {
      string pdfFile = string.Empty;
      LetterList letterList = new LetterList();

      setPDFOption(rpt, fileName);

      try
      {
        //Environment.SpecialFolder.ApplicationData
        /*
        DirectoryInfo dirInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Letter");
        // Determine whether the directory exists.
        if (!dirInfo.Exists)
        {
          // Try to create the directory.
          dirInfo.Create();
        }
        */
        pdfFile = string.Format(@"{0}\{1}", letterFolder, fileName);

        rpt.ExportToPdf(pdfFile);
        letterList.LetterName = fileName;
        letterList.ResvNumber = string.Empty;
        letterList.AmountDue = 0;
        letterList.BalanceAmt = 0;
        fileList.Add(letterList);
      }
      catch (Exception e)
      {
        //MessageBox.Show(string.Format("Could not write a letter to a file: {0}", e.ToString()), "Creating Error",
        //  MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
      }

      HttpContext.Current.Session["FileList"] = fileList;

      return fileList;
    }



    public List<LetterList> TestAttach()
    {
      string letterFolder = string.Empty;
      string fileExtension = "*.pdf";
      string[] fileEntries = null;
      string temp = string.Empty;

      List<LetterList> letterList = new List<LetterList>();
      LetterList letter;

      Properties.Settings appSettings = new Properties.Settings();

      letterFolder = appSettings.LetterFolder;
      if (Directory.Exists(letterFolder))
      {

        fileEntries = Directory.GetFiles(letterFolder);
        foreach (var f in fileEntries)
        {
          letter = new LetterList();
          letter.Attached = true;

          temp = f.Remove(f.Length - 4, 4);  // remove extension.  For example, .pdf
          temp = temp.Remove(0, letterFolder.Length + 1); // remove path.  for example, d:\\LetterFolder
          letter.LetterName = temp;
          letterList.Add(letter);
        }
      }

      return letterList;
    }

    /// <summary>
    /// print to a PDF file.
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    public void ToPDF(int fileID, DataSet dsResv)
    {
      XtraReport rpt;
      string formTag;
      string resvNo = string.Empty;
      string villaCode = string.Empty;
      //int clientID;
      string primaryClientEmail = string.Empty;
      int index;
      decimal totalPrice = 0;
      //decimal amountDue = 0;
      decimal totalAmountDue = 0;
      decimal totalCharge = 0;
      decimal totalBalanceAmt = 0;
      DateTime beginDate = DateTime.MinValue;
      DateTime endDate = DateTime.MinValue;
      string isTravelAgent = "N";

      LetterList letterList = new LetterList();
      //letterList.ResvNumber = string.Empty;

      DataView dv;

      string fileName;
      string pdfFileName;

      //get a letter that storing in the database.
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      file = fileStorageData.GetFile(fileID);
      if (file == null)
      //dsRpt = FileStorage.GetFile(fileID);
      //if (dsRpt.Tables[0].Rows.Count == 0)
      {
        //MessageBox.Show(string.Format("Letter number {0} cannot be found.", fileID.ToString()), "Not Found",
        //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        return;
      }

      // retrieve resv # of first row for displaying on the tab caption
      dv = dsResv.Tables[0].DefaultView;
      if (fileID == (int)LetterType.ItineraryStatement)
      {
        //letterList.ResvNumber = dv[0].Row["ItineraryID"].ToString();
        beginDate = Convert.ToDateTime(dv[0].Row["DateFrom"]);
        //letterList.ResvAgent = dv[0].Row["ResvAgent"].ToString();
        totalAmountDue = 0;
        var itineraryData = from i in dsResv.Tables[0].AsEnumerable()
                            orderby i.Field<DateTime>("DateFrom"), i.Field<DateTime>("DateTo")
                            select new
                            {
                              ItineraryID = i.Field<Int32>("ItineraryID"),
                              ReservationNumber = i.Field<string>("ReservationNumber"),
                              IsTravelAgent = i.Field<string>("IsTravelAgent"),
                              VillaCode = i.Field<string>("VillaCode"),
                              DateFrom = i.Field<DateTime>("DateFrom"),
                              DateTo = i.Field<DateTime>("DateTo"),
                              GrossPrice = i.Field<decimal>("GrossPrice"),
                              ServiceCharge = i.Field<decimal>("ServiceChargeAmt"),
                              TaxAmount = i.Field<decimal>("TaxAmt"),
                              OtherCharge = i.Field<decimal>("OtherChargesAmt"),
                              AmountDue = i.Field<decimal>("AmountDue"),
                              TotalReceived = i.Field<decimal>("TotalReceived"),
                              TACommAmt = i.Field<decimal>("TACommAmt"),
                              BalanceAmt = i.Field<decimal>("BalanceAmt")
                            };
        if (itineraryData != null)
        {
          foreach (var s in itineraryData)
          {
            // make sure that a reservation is not already in the list,
            // This will take care of resv with TA (2 letters).
            if (!resvNo.Contains(s.ReservationNumber))
            {
              // if it has to do with TA, TA commission is included in the amt due.

              //ML - 08/07/2017 : Commmented out and replaced with the below statement.
              /*
              if (s.IsTravelAgent == "Y")
              {
                totalAmountDue += (s.DepositDue + s.TACommAmt) - s.TotalReceived;
              }
              else
              {
                totalAmountDue += s.DepositDue - s.TotalReceived;
              }
              */
              // ML - 08/07/2017 : replaced the statement above
              totalAmountDue += s.AmountDue;
              totalCharge += s.GrossPrice + s.ServiceCharge + s.TaxAmount + s.OtherCharge;
              endDate = s.DateTo;
              resvNo += string.Format("{0},", s.ReservationNumber);
              villaCode += string.Format("{0},", s.VillaCode);
              totalBalanceAmt += s.BalanceAmt;
            }
          }
        }

        if (resvNo.Length > 0)
        {
          // remove the last comma
          resvNo = resvNo.Substring(0, resvNo.Length - 1);
          // remove the last comma
          villaCode = villaCode.Substring(0, villaCode.Length - 1);
        }

        /*
        amountDue = Convert.ToDouble(dv[0].Row["DepositDue"]) - Convert.ToDouble(dv[0].Row["TotalReceived"]);
        letterList.ResvAgent = dv[0].Row["ResvAgent"].ToString();
        letterList.ResvCurrencySymbol = dv[0].Row["ResvCurrencySymbol"].ToString();
        letterList.ClientName = dv[0].Row["ClientName"].ToString();
        letterList.DateFrom = Convert.ToDateTime(dv[0].Row["DateFrom"]);
        letterList.DateTo = Convert.ToDateTime(dv[0].Row["DateTo"]);

        letterList.BalanceDue = amountDue;
        letterList.TotalPrice = totalPrice;
        */
      }
      else
      {
        resvNo = dv[0].Row["ReservationNumber"].ToString();  //get a reservation # from the first row of view
        villaCode = dv[0].Row["VillaCode"].ToString();
        totalPrice = Convert.ToDecimal(dv[0].Row["GrossPrice"]) + Convert.ToDecimal(dv[0].Row["ServiceChargeAmt"]) + Convert.ToDecimal(dv[0].Row["TaxAmt"]) + Convert.ToDecimal(dv[0].Row["OtherChargesAmt"]);
        isTravelAgent = dv[0].Row["IsTravelAgent"].ToString();
        // if it has to with TA, amount due includes TA commission.
        //ML - 08/07/2017 : Commmented out and replaced with the below statement.
        /*
        if (isTravelAgent == "Y")
        {
          amountDue = (Convert.ToDouble(dv[0].Row["DepositDue"]) + Convert.ToDouble(dv[0].Row["TACommAmt"])) - Convert.ToDouble(dv[0].Row["TotalReceived"]);
        }
        else
        {
          amountDue = Convert.ToDouble(dv[0].Row["DepositDue"]) - Convert.ToDouble(dv[0].Row["TotalReceived"]);
        }
        */
        // ML - 08/07/2017 : replaced the statement above
        totalAmountDue += Convert.ToDecimal(dv[0].Row["AmountDue"]);
        beginDate = Convert.ToDateTime(dv[0].Row["DateFrom"]);
        endDate = Convert.ToDateTime(dv[0].Row["DateTo"]);
        totalBalanceAmt += Convert.ToDecimal(dv[0].Row["BalanceAmt"]);
        /*
        totalReceive = Convert.ToDouble(dv[0].Row["WimcoTotalRecdClient"]) - Convert.ToDouble(dv[0].Row["PMTotalRecdClient"]) + Convert.ToDouble(dv[0].Row["TotalPaidClient"]);
        totalBalance = totalPrice - totalReceive;
        */
      }

      letterList.ItineraryID = Convert.ToInt32(dv[0].Row["ItineraryID"]);
      letterList.ResvNumber = resvNo;
      letterList.ResvAgent = dv[0].Row["ResvAgent"].ToString();
      letterList.ResvCurrencySymbol = dv[0].Row["ResvCurrencySymbol"].ToString();
      letterList.VillaCode = villaCode;
      letterList.ClientName = string.Format("{0} {1}", dv[0].Row["FirstName"], dv[0].Row["LastName"]);
      letterList.FirstName = dv[0].Row["FirstName"].ToString();
      letterList.LastName = dv[0].Row["LastName"].ToString();
      letterList.DateFrom = beginDate;
      letterList.DateTo = endDate;
      letterList.AmountDue = totalAmountDue;
      letterList.TotalPrice = totalPrice;
      letterList.BalanceAmt = totalBalanceAmt;
      //clientID = (int)(dv[0].Row["ClientID"]);
      //primaryClientEmail = WimcoBaseLogic.BaseLogic.clientemail_GetPrimaryEmail(clientID);
      //HttpContext.Current.Session["ClientEmail"] = primaryClientEmail;
      HttpContext.Current.Session["ClientEmail"] = dv[0].Row["EMailAddress"].ToString();
      HttpContext.Current.Session["ClientName"] = letterList.ClientName;
      HttpContext.Current.Session["ClientLastName"] = letterList.LastName;

      //format a tab string on a form
      //dr = dsRpt.Tables[0].Rows[0];
      //fileName = dr["FileName"].ToString();
      fileName = file.FileName;
      //remove the extension
      //len = fileName.Length;
      //fileName = fileName.Remove(len - 5, 5);

      // remove everything after the - (dash).
      // most file contains the following file format:
      // Client Confirmation With TA - R or Client Confirmation With TA - HM
      index = fileName.IndexOf("-");
      if (index > 0)
      {
        fileName = fileName.Substring(0, index - 1);
      }
      if (fileID == (int)LetterType.ItineraryStatement)
      {
        fileName = string.Format("{0} ({1})", fileName, letterList.ItineraryID);
        formTag = string.Format("{0}{1})", fileID.ToString(), letterList.ItineraryID);
      }
      else
      {
        fileName = string.Format("{0} ({1})", fileName, letterList.ResvNumber);
        formTag = string.Format("{0}{1})", fileID.ToString(), letterList.ResvNumber);
      }

      try
      {
        if (RequireMergeFile)
        {
          if (FirstTime)
          {
            MergedLetter = GetRpt(dsResv, file);
            setPDFOption(MergedLetter, fileName);
            MergedLetter.CreateDocument();
            MessageList.AddMessageToList(dsResv, "Magnum - " + MergedLetter.Tag.ToString());
            FirstTime = false;
          }
          else
          {
            rpt = GetRpt(dsResv, file);
            setPDFOption(rpt, fileName);
            rpt.CreateDocument();                    //if this is not a first time, add a letter to a previous one
            MergedLetter.Pages.AddRange(rpt.Pages);  //add letters to the previous letter
            MessageList.AddMessageToList(dsResv, "Magnum - " + rpt.Tag.ToString());
          }
        }
        else
        {
          rpt = GetRpt(dsResv, file);
          setPDFOption(rpt, fileName);
          rpt.CreateDocument();
          pdfFileName = string.Format(@"{0}.pdf", fileName);
          WriteToPDF(rpt, pdfFileName, letterList);
          MessageList.AddMessageToList(dsResv, "Magnum - " + rpt.Tag.ToString());
          //rpt.ExportToPdf(string.Format(@"{0}\{1}.pdf", dirInfo.FullName, fileName));
        }
      }
      catch (Exception e)
      {
        // MessageBox.Show(string.Format("Could not write a letter to a file: {0}", e.ToString()), "Creating Error",
        //   MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
      }
    }

    public void ToPDF(string letterName, DataSet dsResv)
    {
      string requestUrl;
      byte[] fileContent = null;
      MemoryStream memoryStream;

      string formTag;
      string resvNo = string.Empty;
      string villaCode = string.Empty;
      //int clientID;
      string primaryClientEmail = string.Empty;
      int index;
      decimal totalPrice = 0;
      //decimal amountDue = 0;
      decimal totalAmountDue = 0;
      decimal totalCharge = 0;
      decimal totalBalanceAmt = 0;
      DateTime beginDate = DateTime.MinValue;
      DateTime endDate = DateTime.MinValue;
      string isTravelAgent = "N";
      string phoneNumber;
      string villaName;
      int numberOfAdults;
      int numberOfChildren;
      string firstName;
      string lastName;
      int numberOfGuests;

      LetterList letterList = new LetterList();
      //letterList.ResvNumber = string.Empty;

      DataView dv;

      string fileName;
      string pdfFileName;

      string physicalFileName;


      // retrieve resv # of first row for displaying on the tab caption
      dv = dsResv.Tables[0].DefaultView;
      resvNo = dv[0].Row.Field<string>("ReservationNumber");  //get a reservation # from the first row of view
      villaCode = dv[0].Row.Field<string>("VillaCode");
      totalPrice = Convert.ToDecimal(dv[0].Row["GrossPrice"]) + Convert.ToDecimal(dv[0].Row["ServiceChargeAmt"]) + Convert.ToDecimal(dv[0].Row["TaxAmt"]) + Convert.ToDecimal(dv[0].Row["OtherChargesAmt"]);
      isTravelAgent = dv[0].Row["IsTravelAgent"].ToString();
      totalAmountDue += Convert.ToDecimal(dv[0].Row["AmountDue"]);
      beginDate = dv[0].Row.Field<DateTime>("DateFrom");
      endDate = dv[0].Row.Field<DateTime>("DateTo");
      totalBalanceAmt += Convert.ToDecimal(dv[0].Row["BalanceAmt"]);
      phoneNumber = dv[0].Row.Field<string>("PhoneNumber");
      villaName = dv[0].Row.Field<string>("VillaName");
      numberOfAdults = dv[0].Row.Field<int>("NumAdults");
      numberOfChildren = dv[0].Row.Field<int>("NumChildren");
      firstName = dv[0].Row.Field<string>("FirstName");
      lastName = dv[0].Row.Field<string>("LastName");
      numberOfGuests = numberOfAdults + numberOfChildren;


      string baseUrl = Properties.Settings.Default.StaticURL;
      string filePath = Properties.Settings.Default.LetterFolder;

      requestUrl = $"{baseUrl}/{letterName}.pdf";
      letterName = $"{letterName} ({resvNo}).pdf";
      physicalFileName = $@"{filePath}\{letterName}";

      letterList.ItineraryID = Convert.ToInt32(dv[0].Row["ItineraryID"]);
      letterList.ResvNumber = resvNo;
      letterList.ResvAgent = dv[0].Row["ResvAgent"].ToString();
      letterList.ResvCurrencySymbol = dv[0].Row["ResvCurrencySymbol"].ToString();
      letterList.VillaCode = villaCode;
      letterList.ClientName = $"{dv[0].Row["FirstName"]} {dv[0].Row["LastName"]}";
      letterList.FirstName = dv[0].Row["FirstName"].ToString();
      letterList.LastName = dv[0].Row["LastName"].ToString();
      letterList.DateFrom = beginDate;
      letterList.DateTo = endDate;
      letterList.AmountDue = totalAmountDue;
      letterList.TotalPrice = totalPrice;
      letterList.BalanceAmt = totalBalanceAmt;
      letterList.LetterName = letterName;
      letterList.FilePath = filePath;

      fileList.Add(letterList);

      HttpContext.Current.Session["FileList"] = fileList;
      HttpContext.Current.Session["ClientEmail"] = dv[0].Row["EMailAddress"].ToString();
      HttpContext.Current.Session["ClientName"] = $"{firstName} {lastName}";
      HttpContext.Current.Session["ClientLastName"] = lastName;

      using HttpClient client = new HttpClient();

      HttpResponseMessage msg = client.GetAsync(requestUrl).Result;

      if (msg.IsSuccessStatusCode)
      {
        // get the actual content stream
        fileContent = msg.Content.ReadAsByteArrayAsync().Result;

        //write file contents to a memory stream.
        using MemoryStream ms = new MemoryStream();

        ms.Write(fileContent, 0, fileContent.Length);
        //set position at a begin of a file.
        ms.Position = 0;

        if (ms == null)
        {
          return;
        }

        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(new PdfReader(ms), new PdfWriter(physicalFileName));
        try
        {
          PdfAcroForm form = PdfAcroForm.GetAcroForm(pdf, true);

          IDictionary<String, PdfFormField> fields = form.GetFormFields();

          PdfFormField toSet;

          if (fields.TryGetValue("FirstAndLastName", out toSet))
          {
            toSet.SetValue($"{firstName} {lastName}");
          }

          if (fields.TryGetValue("CellPhoneNumber", out toSet))
          {
            toSet.SetValue(phoneNumber ?? "");
          }

          if (fields.TryGetValue("ReservationNumber", out toSet))
          {
            toSet.SetValue(resvNo);
          }

          if (fields.TryGetValue("VillaName", out toSet))
          {
            toSet.SetValue($"{villaName} ({villaCode})");
          }

          if (fields.TryGetValue("ArrivalDate", out toSet))
          {
            toSet.SetValue(beginDate.ToString("dd MMM yyyy"));
          }

          if (fields.TryGetValue("DepartureDate", out toSet))
          {
            toSet.SetValue(endDate.ToString("dd MMM yyyy"));
          }

          if (fields.TryGetValue("NumberOfAdults", out toSet))
          {
            toSet.SetValue(numberOfAdults.ToString());
          }

          if (fields.TryGetValue("NumberOfGuests", out toSet))
          {
            toSet.SetValue(numberOfGuests.ToString());
          }

        }
        finally
        {
          pdf.Close();
          ms.Flush();
          ms.Close();
        }
      }
    }

    public void ToEMail(int fileID, DataSet dsResv)
    {
      XtraReport rpt;
      string formTag;
      string resvNo;

      DataView dv;

      string fileName;
      string pdfFileName;

      //get a letter that storing in the database.
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      file = fileStorageData.GetFile(fileID);
      if (file == null)
      //dsRpt = FileStorage.GetFile(fileID);
      //if (dsRpt.Tables[0].Rows.Count == 0)
      {
        // MessageBox.Show(string.Format("Letter number {0} cannot be found.", fileID.ToString()), "Not Found",
        //   MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        return;
      }

      // retrieve resv # of first row for displaying on the tab caption
      dv = dsResv.Tables[0].DefaultView;
      resvNo = dv[0].Row["ReservationNumber"].ToString();  //get a reservation # from the first row of view

      //format a tab string on a form
      //dr = dsRpt.Tables[0].Rows[0];
      //fileName = dr["FileName"].ToString();
      fileName = file.FileName;
      //remove the extension
      //len = fileName.Length;
      //fileName = fileName.Remove(len - 5, 5);
      fileName = string.Format("{0} ({1})", fileName, resvNo);
      formTag = string.Format("{0}{1})", fileID.ToString(), resvNo);

      try
      {
        if (RequireMergeFile)
        {
          if (FirstTime)
          {
            MergedLetter = GetRpt(dsResv, file);
            setPDFOption(MergedLetter, fileName);
            MergedLetter.CreateDocument();
            MessageList.AddMessageToList(dsResv, "Magnum - " + MergedLetter.Tag.ToString());
            FirstTime = false;
          }
          else
          {
            rpt = GetRpt(dsResv, file);
            setPDFOption(rpt, fileName);
            rpt.CreateDocument();                    //if this is not a first time, add a letter to a previous one
            MergedLetter.Pages.AddRange(rpt.Pages);  //add letters to the previous letter
            MessageList.AddMessageToList(dsResv, "Magnum - " + rpt.Tag.ToString());
          }
        }
        else
        {
          rpt = GetRpt(dsResv, file);
          setPDFOption(rpt, fileName);
          rpt.CreateDocument();
          pdfFileName = string.Format(@"{0}.pdf", fileName);
          WriteToPDF(rpt, pdfFileName);
          MessageList.AddMessageToList(dsResv, "Magnum - " + rpt.Tag.ToString());
          //rpt.ExportToPdf(string.Format(@"{0}\{1}.pdf", dirInfo.FullName, fileName));
        }
      }
      catch (Exception e)
      {
        //MessageBox.Show(string.Format("Could not write a letter to a file: {0}", e.ToString()), "Creating Error",
        //  MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
      }
    }


    public void WriteToPDFAndEmail(XtraReport rpt, string fileName)
    {
      string pdfFile = string.Empty;

      setPDFOption(rpt, fileName);
      /*
      try
      {
        pdfFile = string.Format(@"{0}", fileName);
        string path = Application.StartupPath + @"\Document.pdf";
        MAPISendMail.MAPI api = new  MAPISendMail.MAPI(IntPtr.Zero,path, "Test", "");
        MAPISendMail.MapiRecipDesc desc = new MAPISendMail.MapiRecipDesc();
        desc.address = "mlorn@wimco.com";
        desc.name = "Mao Lorn";


      }
      catch (Exception e)
      {
        MessageBox.Show(string.Format("Could not write a letter to a file: {0}", e.ToString()), "Creating Error",
          MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
      }
      */
    }

    /// <summary>
    /// Gets the RPT.
    /// </summary>
    /// <param name="file">The file.</param>
    /// <returns></returns>
    public XtraReport GetRpt(FileStorage file)
    {
      int fileSize;
      byte[] fileData;
      XtraReport rpt;
      ReportHelper reportHelper;

      //get actaul report
      fileSize = (int)file.FileSize;
      fileData = file.FileData;
      reportHelper = new ReportHelper();
      rpt = reportHelper.GetReport(fileSize, fileData);
      return rpt;
    }

    public XtraReport GetRpt(DataSet dsResv, FileStorage file)
    {
      int fileSize;
      byte[] fileData;
      XtraReport rpt;
      ReportHelper reportHelper;

      //get actaul report
      fileSize = (int)file.FileSize;
      fileData = file.FileData;
      reportHelper = new ReportHelper();
      rpt = reportHelper.GetReport(fileSize, fileData);
      rpt.DataSource = dsResv;
      rpt.DataMember = "Table";
      return rpt;
    }


    public DataSet GetDataForForm(LetterList letterList, bool requestBalanceAmt = false)
    {
      //DataSet dsResv = null;
      //DataTable dt = null;
      DataRow dr = null;
      //dsResv = new DataSet("dsResv");
      dsLetterForm dsResv = new dsLetterForm();
      DataTable dt = dsResv.Tables[0];
      dt.TableName = "Table";
      /*
      dt = new DataTable("Table");
      dt.Columns.Add("ReservationNumber", typeof(string));
      dt.Columns.Add("VillaCode", typeof(string));
      dt.Columns.Add("VillaName", typeof(string));
      dt.Columns.Add("ResvAgent", typeof(string));
      dt.Columns.Add("ClientName", typeof(string));
      dt.Columns.Add("FirstName", typeof(string));
      dt.Columns.Add("LastName", typeof(string));
      dt.Columns.Add("DateFrom", typeof(DateTime));
      dt.Columns.Add("DateTo",typeof(DateTime));
      dt.Columns.Add("ResvCurrencySymbol", typeof(string));
      dt.Columns.Add("BalanceDue", typeof(decimal));
      */

      dr = dt.NewRow();
      dr["ReservationNumber"] = letterList.ResvNumber;
      dr["VillaCode"] = letterList.VillaCode;
      dr["VillaName"] = letterList.VillaName;
      dr["ClientName"] = letterList.ClientName;
      dr["FirstName"] = letterList.FirstName;
      dr["LastName"] = letterList.LastName;
      dr["DateFrom"] = letterList.DateFrom;
      dr["DateTo"] = letterList.DateTo;
      dr["ResvCurrencySymbol"] = letterList.ResvCurrencySymbol;
      dr["ResvAgent"] = letterList.ResvAgent;
      //if request balance amount
      if (requestBalanceAmt)
      {
        dr["AmountDue"] = letterList.BalanceAmt;
      }
      else
      {
        dr["AmountDue"] = letterList.AmountDue;
      }

      dt.Rows.Add(dr);

      dsResv.Tables.Clear();
      dsResv.Tables.Add(dt);

      return dsResv;
    }


    /// <summary>
    /// Prints to printer.
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    public void ToPrinter(int fileID, DataSet dsResv, bool printDuplex)
    {
      XtraReport rpt = null;

      //DataRow dr;
      //int fileSize;
      //byte[] fileData;

      //get a letter that storing in the database.
      FileStorageData fileData = new FileStorageData();
      var file = fileData.GetFile(fileID);
      if (file == null)
      {
        //MessageBox.Show(string.Format("Letter number {0} cannot be found.", fileID.ToString()), "Not Found",
        //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        return;
      }

      if (FirstTime)
      {
        MergedLetter = GetRpt(dsResv, file);
        if (printDuplex)
        {
          MergedLetter.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);
        }
        //MergedLetter.PrintDialog();        //let a user select a printer for the first letter 
        MessageList.AddMessageToList(dsResv, "Magnum - " + MergedLetter.Tag.ToString());
        FirstTime = false;
      }
      else
      {
        try
        {
          rpt = GetRpt(dsResv, file);
          if (printDuplex)
          {
            rpt.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);
          }
          //rpt.PrintDialog();
          MessageList.AddMessageToList(dsResv, "Magnum - " + rpt.Tag.ToString());
          //rpt.Print();                 //If there more job after selecting a print, the next letter print directly to a printer.
        }
        finally
        {
          if (RequireMergeFile)
          {
            MergedLetter.Pages.AddRange(rpt.Pages);  //add letters to the previous letter
          }
        }
      }
    }

    private void PrintingSystem_StartPrint(object sender, PrintDocumentEventArgs e)
    {
      e.PrintDocument.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
      //System.Drawing.Printing.PaperSource paperSource;
      //for (int i = 0; i < e.PrintDocument.PrinterSettings.PaperSources.Count; i++)
      //{
      //  paperSource = e.PrintDocument.PrinterSettings.PaperSources[i];
      //  if (paperSource.SourceName == "Tray 3")
      //  {
      //    e.PrintDocument.DefaultPageSettings.PaperSource = paperSource;
      //    break;
      //  }
      //}
      //e.PrintDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocument_BeginPrint);
    }

    private void PrintingSystem_StartPrint2(object sender, PrintDocumentEventArgs e)
    {
      //System.Drawing.Printing.PrintDocument doc = sender as System.Drawing.Printing.PrintDocument;
      //printerName = e.PrintDocument.PrinterSettings.PrinterName;
    }

    // TODO: will have to delete following method ToScreen(int fileID, DataSet dsResv, string resvNo)
    /// <summary>
    /// Prints letter to a screen.
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="resvNo">The resv no.</param>
    public void ToScreen(int fileID, DataSet dsResv, string resvNo)
    {
      XtraReport rptSelectedLetter;
      string formTag;
      string url = string.Empty;
      string clientEmail = string.Empty;

      int fileSize;
      byte[] fileData;
      string fileName;
      DataView dv;

      //get a letter that storing in the database.
      FileStorageData fileStorageData = new FileStorageData();
      var file = fileStorageData.GetFile(fileID);
      if (file == null)
      {
        //MessageBox.Show(string.Format("Letter number {0} cannot be found.", fileID.ToString()), "Not Found",
        //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        return;
      }

      // retrieve resv # for displaying on the tab caption
      dv = dsResv.Tables[0].DefaultView;
      //resvNo = dv[0].Row["ReservationNumber"].ToString();  //get a reservation # from the first row of view

      try
      {
        clientEmail = dv[0].Row["EMailAddress"].ToString();
        HttpContext.Current.Session["ClientEmail"] = clientEmail;
        HttpContext.Current.Session["ClientName"] = string.Format("{0} {1}", dv[0].Row["FirstName"].ToString(), dv[0].Row["LastName"].ToString());
      }
      catch
      {

      }

      //format a tab string on a form
      fileName = file.FileName;
      //remove the extension
      //len = fileName.Length;
      //fileName = fileName.Remove(len - 5, 5);
      fileName = string.Format("{0} ({1})", fileName, resvNo);
      formTag = string.Format("{0}{1})", fileID, resvNo);

      //get actaul report
      fileSize = (int)file.FileSize;
      fileData = file.FileData;
      ReportHelper reportHelper = new ReportHelper();
      //rptSelectedLetter = reportHelper.GetReport(fileSize, fileData);
      rptSelectedLetter = reportHelper.GetReport(fileID);

      if (dsResv.Tables[0].Rows.Count > 0)
      {
        MessageList.AddMessageToList(dsResv, "Magnum - " + rptSelectedLetter.Tag.ToString());
        rptSelectedLetter.DataSource = dsResv;
        rptSelectedLetter.DataMember = "Table";
        recordMessage();
        HttpContext.Current.Session.Add("ReportObject", rptSelectedLetter);
        //HttpContext.Current.Response.Redirect("~/LetterMainViewer.aspx");
        url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterMainViewer.aspx");

        //HttpContext.Current.Response.Write("<script>window.open('" + url + "','_blank');</script>");
        HttpContext.Current.Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      }
      else
      {
        //showMessage(string.Format("There is no data for the reservation # {0}", resvNo));
      }

    }

    /// <summary>
    /// Prints letter to a screen.
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="resvNo">The resv no.</param>
    public void ToScreen(int fileID, DataSet dsResv, bool printDuplex)
    {
      XtraReport rptSelectedLetter;
      string letterTag;
      string resvNo;
      string clientEmail = string.Empty;
      string url = string.Empty;

      DataView dv;

      //int fileSize;
      //byte[] fileData;
      string fileName;

      //get a letter that storing in the database.
      FileStorageData fileStorageData = new FileStorageData();
      var file = fileStorageData.GetFile(fileID);
      if (file == null)
      {
        //MessageBox.Show(string.Format("Letter number {0} cannot be found.", fileID.ToString()), "Not Found",
        //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        return;
      }

      // retrieve resv # for displaying on the tab caption
      dv = dsResv.Tables[0].DefaultView;
      resvNo = dv[0].Row["ReservationNumber"].ToString();  //get a reservation # from the first row of view

      try
      {
        clientEmail = dv[0].Row["EMailAddress"].ToString();
        HttpContext.Current.Session["ClientEmail"] = clientEmail;
        HttpContext.Current.Session["ClientName"] = string.Format("{0} {1}", dv[0].Row["FirstName"].ToString(), dv[0].Row["LastName"].ToString());
      }
      catch
      {

      }

      //format a tab string on a form
      fileName = file.FileName;
      //remove the extension
      //len = fileName.Length;
      //fileName = fileName.Remove(len - 5, 5);
      fileName = string.Format("{0} ({1})", fileName, resvNo);
      letterTag = string.Format("{0}{1})", fileID, resvNo);

      //get actault report
      //fileSize = (int)file.FileSize;
      //fileData = file.FileData;
      ReportHelper reportHelper = new ReportHelper();
      //rptSelectedLetter = reportHelper.GetReport(fileSize, fileData);
      rptSelectedLetter = reportHelper.GetReport(fileID);

      if (printDuplex)
      {
        //rpt.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);
      }

      if (dsResv.Tables[0].Rows.Count > 0)
      {
        MessageList.AddMessageToList(dsResv, "Magnum - " + rptSelectedLetter.Tag.ToString());
        rptSelectedLetter.DataSource = dsResv;
        rptSelectedLetter.DataMember = "Table";
        recordMessage();

        HttpContext.Current.Session.Add("ReportObject", rptSelectedLetter);
        //url = string.Format("{0}/{1}", Properties.Settings.Default.nomadLetterSite, "LetterMainViewer.aspx");
        url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "WebEmail.aspx");
        HttpContext.Current.Response.Redirect(url);
        //HttpContext.Current.Response.Write("<script>window.open('" + url + "','_blank');</script>");
      }
      else
      {
        //showMessage(string.Format("There is no data for the reservation # {0}", resvNo));
      }

    }


    /// <summary>
    /// Prints letter to a screen.
    /// </summary>
    /// <param name="fileID">The file ID.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="resvNo">The resv no.</param>
    public void ToScreen(int fileID, DataSet dsResv, bool printDuplex, bool TA)
    {
      XtraReport rptSelectedLetterTA;
      string letterTag;
      string resvNo;
      string clientEmail = string.Empty;
      string url = string.Empty;

      DataView dv;

      //int fileSize;
      //byte[] fileData;
      string fileName;

      //get a letter that storing in the database.
      FileStorageData fileStorageData = new FileStorageData();
      var file = fileStorageData.GetFile(fileID);
      if (file == null)
      {
        //MessageBox.Show(string.Format("Letter number {0} cannot be found.", fileID.ToString()), "Not Found",
        //  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        return;
      }

      // retrieve resv # for displaying on the tab caption
      dv = dsResv.Tables[0].DefaultView;
      resvNo = dv[0].Row["ReservationNumber"].ToString();  //get a reservation # from the first row of view

      try
      {
        clientEmail = dv[0].Row["EMailAddress"].ToString();
        HttpContext.Current.Session["ClientEmail"] = clientEmail;
        HttpContext.Current.Session["ClientName"] = string.Format("{0} {1}", dv[0].Row["FirstName"].ToString(), dv[0].Row["LastName"].ToString());
      }
      catch
      {

      }

      //format a tab string on a form
      fileName = file.FileName;
      //remove the extension
      //len = fileName.Length;
      //fileName = fileName.Remove(len - 5, 5);
      fileName = string.Format("{0} ({1})", fileName, resvNo);
      letterTag = string.Format("{0}{1})", fileID, resvNo);

      //get actault report
      //fileSize = (int)file.FileSize;
      //fileData = file.FileData;
      ReportHelper reportHelper = new ReportHelper();
      //rptSelectedLetter = reportHelper.GetReport(fileSize, fileData);
      rptSelectedLetterTA = reportHelper.GetReport(fileID);

      if (printDuplex)
      {
        //rpt.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);
      }

      if (dsResv.Tables[0].Rows.Count > 0)
      {
        MessageList.AddMessageToList(dsResv, "Magnum - " + rptSelectedLetterTA.Tag.ToString());
        rptSelectedLetterTA.DataSource = dsResv;
        rptSelectedLetterTA.DataMember = "Table";
        recordMessage();
        HttpContext.Current.Session["IsTA"] = true;
        HttpContext.Current.Session.Add("ReportObjectTA", rptSelectedLetterTA);
        //HttpContext.Current.Response.Redirect("~/LetterMainViewer.aspx");
      }
      else
      {
        //showMessage(string.Format("There is no data for the reservation # {0}", resvNo));
      }

    }


    /// <summary>
    /// Processes the letter with TA
    /// Where a reservation was booked by a TA, two types of letters are printed
    /// One for a TA letter and other is for a client letter.
    /// This method will print letters one after another for the same Resv #.
    /// </summary>
    /// <param name="TALetter">The TA letter.</param>
    /// <param name="clientLetter">The client letter.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="printTo">The print to.</param>
    public void ProcessLetterWithTA(int TALetter, int clientLetter, DataSet dsResv, int printTo)
    {
      DataTable dt;
      string resvNo = string.Empty;
      string filterString;
      PrintLetter printLetter = new PrintLetter();
      //HttpContext.Current.Session["IsTA"] = true;
      dt = dsResv.Tables[0].DefaultView.ToTable();
      foreach (DataRow dr in dt.Rows)
      {
        if (resvNo != dr["ReservationNumber"].ToString())
        {
          resvNo = dr["ReservationNumber"].ToString();
          filterString = string.Format("ReservationNumber = '{0}'", resvNo);
          dsResv.Tables[0].DefaultView.RowFilter = filterString;
          //PrintLetter.PrintTo(TALetter, dsResv, printTo);
          printLetter.PrintTo(TALetter, dsResv, printTo, false, true);
          printLetter.PrintTo(clientLetter, dsResv, printTo);
        }
      }
    }

    /// <summary>
    /// Processes the letter with TA.
    /// </summary>
    /// <param name="TALetter">The TA letter.</param>
    /// <param name="clientLetter">The client letter.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="printTo">The print to.</param>
    /// <param name="printDuplex">if set to <c>true</c> [print duplex].</param>
    public void ProcessLetterWithTA(int TALetter, int clientLetter, DataSet dsResv, int printTo, bool printDuplex)
    {
      DataTable dt;
      string resvNo = string.Empty;
      string filterString;
      //HttpContext.Current.Session["IsTA"] = true;
      dt = dsResv.Tables[0].DefaultView.ToTable();
      foreach (DataRow dr in dt.Rows)
      {
        if (resvNo != dr["ReservationNumber"].ToString())
        {
          resvNo = dr["ReservationNumber"].ToString();
          filterString = string.Format("ReservationNumber = '{0}'", resvNo);
          dsResv.Tables[0].DefaultView.RowFilter = filterString;
          //PrintLetter.PrintTo(TALetter, dsResv, printTo, printDuplex);
          PrintTo(TALetter, dsResv, printTo, printDuplex, true);
          PrintTo(clientLetter, dsResv, printTo, printDuplex);
        }
      }
    }

    /// <summary>
    /// Processes the letter that have terms and conditions
    /// </summary>
    /// <param name="letter">The letter.</param>
    /// <param name="dsResv">The ds resv.</param>
    /// <param name="printTo">The print to.</param>
    /// <param name="printDuplex">if set to <c>true</c> [print duplex].</param>
    public void WithTermAndCondition(int letter, DataSet dsResv, int printTo, bool printDuplex)
    {
      DataTable dt;
      string resvNo = string.Empty;
      string filterString;
      dt = dsResv.Tables[0].DefaultView.ToTable();
      foreach (DataRow dr in dt.Rows)
      {
        if (resvNo != dr["ReservationNumber"].ToString())
        {
          resvNo = dr["ReservationNumber"].ToString();
          filterString = string.Format("ReservationNumber = '{0}' AND TermAndCondition = ''", resvNo);
          dsResv.Tables[0].DefaultView.RowFilter = filterString;
          if (dsResv.Tables[0].DefaultView.Count > 0)
          {
            PrintTo(letter, dsResv, printTo);
          }
          else
          {
            filterString = string.Format("ReservationNumber = '{0}' AND TermAndCondition > ''", resvNo);
            dsResv.Tables[0].DefaultView.RowFilter = filterString;
            if (dsResv.Tables[0].DefaultView.Count > 0)
            {
              PrintTo(letter, dsResv, printTo, printDuplex);
            }

          }
        }
      }
    }

    /// <summary>
    /// Opens the PDF file in the default programm (PDF Reader)
    /// </summary>
    /// <param name="fileName">Name of the file.</param>
    public void OpenPDF(string fileName)
    {
      Process process = new Process();
      try
      {
        process.StartInfo.FileName = fileName;
        process.Start();
        process.WaitForInputIdle();
      }
      catch
      {
        //TODO:
        throw;
      }
    }

    private void recordMessage()
    {
      LetterDataBridge letterDataBridge;
      letterDataBridge = new LetterDataBridge();
      letterDataBridge.AddMessage(MessageList.ListOfMessages);
    }

    public void PrintToSeperateFile(int clientLetter, DataSet dsResv, int printTo, bool printDuplex)
    {
      DataTable dt;
      string resvNo = string.Empty;
      string filterString;
      dt = dsResv.Tables[0].DefaultView.ToTable();
      foreach (DataRow dr in dt.Rows)
      {
        if (resvNo != dr["ReservationNumber"].ToString())
        {
          resvNo = dr["ReservationNumber"].ToString();
          filterString = string.Format("ReservationNumber = '{0}'", resvNo);
          dsResv.Tables[0].DefaultView.RowFilter = filterString;
          PrintTo(clientLetter, dsResv, printTo, printDuplex);
        }
      }
    }

    public void PrintToSeperateFile(string clientLetter, DataSet dsResv, int printTo)
    {
      DataTable dt;
      string resvNo = string.Empty;
      string filterString;
      dt = dsResv.Tables[0].DefaultView.ToTable();
      foreach (DataRow dr in dt.Rows)
      {
        if (resvNo != dr["ReservationNumber"].ToString())
        {
          resvNo = dr["ReservationNumber"].ToString();
          filterString = string.Format("ReservationNumber = '{0}'", resvNo);
          dsResv.Tables[0].DefaultView.RowFilter = filterString;
          PrintTo(clientLetter, dsResv, printTo);
        }
      }
    }

  }
}
