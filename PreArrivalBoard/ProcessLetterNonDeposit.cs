using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
//using System.Windows.Forms;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;
//using QuickReport;

namespace PreArrivalBoard
{
  [Obsolete("No longer use", false)]
  public class ProcessLetterNonDeposit
  {
    private DataSet dsResv;
    private string resvNo;
    string startDepositDueDate;
    string endDepositDueDate;
    private int printTo;
    private PrintLetter printLetter = new PrintLetter();

    #region H Non-Deposit Letter
    private void NonDepositLetter_H()
    {
      string filterString;
      //Non-Deposit Letter - H Reservation
      filterString = "(ResvPrefix = 'H') ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.NonDepositLetter_HM, this.dsResv, this.printTo);
      }
    }
    #endregion

    #region M Non-Deposit Letter
    private void NonDepositLetter_M()
    {
      string filterString;
      //Non-Deposit Letter - M Reservation
      filterString = "(ResvPrefix = 'M') ";
      filterString += "AND (VendorBaseID <> 7684) ";  //Vendor Base ID 7684 is WSBH
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.NonDepositLetter_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region M Non-Deposit Letter for WSBH (Vendor Base ID 7684)
    private void NonDepositLetter_M_7684()
    {
      string filterString;
      //Non-Deposit Letter - M Reservation
      filterString = "(ResvPrefix = 'M') ";
      filterString += "AND (VendorBaseID = 7684) ";  //Vendor Base ID 7684 is WSBH
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.NonDepositLetter_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region R Non-Deposit Letter
    private void NonDepositLetter_R()
    {
      string filterString;

      filterString = "ResvPrefix = 'R' ";
      dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.NonDepositLetter_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    /// <summary>
    /// Processes the specified resv no. or specific date range
    /// either print to a printer or preview on screen.    /// </summary>
    /// <param name="resvNo">The resv no.</param>
    /// <param name="startDepositDueDate">The start deposit due date.</param>
    /// <param name="endDepositDueDate">The end deposit due date.</param>
    /// <param name="printTo">The print to.</param>
    public string Process(string resvNo, string startDepositDueDate, string endDepositDueDate, int printTo)
    {
      string result = string.Empty;

      // if it a non-deposit letter, use a deposit balance due, instead of resv balance due.
      //HttpContext.Current.Session.Remove("IsNonDeposit");

      //HttpContext.Current.Session.Remove("IsTA");
      //HttpContext.Current.Session.Remove("ReportObjectTA");

      this.resvNo = resvNo;
      this.startDepositDueDate = startDepositDueDate;
      this.endDepositDueDate = endDepositDueDate;
      this.printTo = printTo;

      LetterData letterData = new LetterData();
      this.dsResv = letterData.GetNonDepositData(this.resvNo, this.startDepositDueDate, this.endDepositDueDate);
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        result = "There is no letter to print.";
        return result;
      }

      try
      {
        HttpContext.Current.Session["RptDataset"] = this.dsResv;
        HttpContext.Current.Session["IsNonDeposit"] = "Y";

        #region Print H or M Reservation
        NonDepositLetter_H();
        NonDepositLetter_M();
        #endregion

        #region Print M Reservation for WSBH
        // not use because the WSBH use R letter type
        //NonDepositLetter_M_7684();
        #endregion

        #region Print R Reservation
        //Non-Deposit Letter - R Resv
        NonDepositLetter_R();
        #endregion
      }
      catch (Exception ex)
      {
        result = string.Format("Error has been occurred while printing letters. With error message: {0}", ex.Message);
      }

      return result;
    }
  }
}
