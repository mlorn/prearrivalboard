﻿
using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PreArrivalBoard
{

  public partial class Login : System.Web.UI.Page
  {
    PreArrivalData preArrivalData = null;
    string logInAs = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
      preArrivalData = new PreArrivalData();

      //set focus on user name when the page is loaded
      SetFocus(LoginControl.FindControl("UserName"));

    }
    private bool AuthenticationUser(string userName, string password)
    {

      bool result = false;
      LoginControl.FailureText = string.Empty;


      // Insert code that implements a site-specific custom 
      // authentication method here.
      //
      // This example implementation always returns false.
      bool rememberMe;
      rememberMe = LoginControl.RememberMeSet;

      LoginControl.RememberMeSet = rememberMe;

      BaseLogic.UserData ud = new BaseLogic.UserData();

      ud = preArrivalData.Login(userName, password, logInAs);
      //if a user found, store userID and GroupID where a user belongs to
      //if (result == true)
      //{
      //  Session["UserID"] = dashboard.UserID;
      //  Session["GroupID"] = dashboard.GroupID;
      //  Session["UserName"] = dashboard.UserName;
      //  Session["FullName"] = dashboard.FullName;
      //  Session["AdminUserName"] = dashboard.AdminUserName;
      //}

      if (ud.isLoggedIn)
      {
        // create ud cookie
        string uds = BaseLogic.SerializeAnObject(ud);
        string b64 = BaseLogic.EncodeTo64(uds);
        HttpCookie c = new HttpCookie("prearrival");

        c.Expires = DateTime.Now.AddHours(12);
        c.Value = b64;
        c.Path = "/";
        Response.Cookies.Add(c);
        HttpContext.Current.Session["UserData"] = ud;
      }

      return ud.isLoggedIn;

    }

    protected void LoginControl_Authenticate(object sender, AuthenticateEventArgs e)
    {
      //ASPxLoadingPanel1.Visible = true;
      bool Authenticated = false;


      Authenticated = AuthenticationUser(LoginControl.UserName, LoginControl.Password);

      e.Authenticated = Authenticated;
      LoginControl.FailureText = "Your login attempt was not successful. Please try again.";
      //ASPxLoadingPanel1.Visible = false;
    }
  }
}