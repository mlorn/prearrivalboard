using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Data.Linq;
using System.Linq;
//using System.Windows.Forms;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;
//using QuickReport;

namespace PreArrivalBoard
{
  public class ProcessLetterFinalPayment
  {
    private DataSet dsResv;
    private string resvNo;
    private string resvStartDate;
    private string resvEndDate;
    private int printTo;
    private PrintLetter printLetter = new PrintLetter();

    #region properties
    /// <summary>
    /// Gets or sets the ds resv.
    /// </summary>
    /// <value>The ds resv.</value>
    public DataSet DsResv
    {
      get
      {
        return dsResv;
      }
      set
      {
        dsResv = value;
      }
    }

    /// <summary>
    /// Gets or sets the resv no.
    /// </summary>
    /// <value>The resv no.</value>
    public string ResvNo
    {
      get
      {
        return resvNo;
      }
      set
      {
        resvNo = value;
      }
    }
    /// <summary>
    /// Gets or sets the resv start date.
    /// </summary>
    /// <value>The resv start date.</value>
    public string ResvStartDate
    {
      get
      {
        return resvStartDate;
      }
      set
      {
        resvStartDate = value;
      }
    }

    /// <summary>
    /// Gets or sets the resv end date.
    /// </summary>
    /// <value>The resv end date.</value>
    public string ResvEndDate
    {
      get
      {
        return resvEndDate;
      }
      set
      {
        resvEndDate = value;
      }
    }

    /// <summary>
    /// Gets or sets the print to.
    /// </summary>
    /// <value>The print to.</value>
    public int PrintTo
    {
      get
      {
        return printTo;
      }
      set
      {
        printTo = value;
      }
    }
    #endregion

    #region Print H reservation with TA or without TA
    /// <summary>
    /// Finals the payment without TA - H reservation
    /// </summary>
    private void FinalPaymentWithoutTA_H()
    {
      string filterString;
      filterString = "(ResvPrefix = 'H') AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.FinalPaymentWithoutTA_HM, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Final Payment Letter With TA - H reservation
    /// </summary>
    private void FinalPaymentWithTA_H()
    {
      string filterString;
      filterString = "(ResvPrefix = 'H') AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.FinalPaymentWithTA_HM,
          (int)LetterType.FinalPaymentWithoutTA_HM, this.dsResv, this.printTo);
      }
    }
    #endregion

    #region Print M reservation with TA or Without TA
    /// <summary>
    /// Finals the payment without TA - M reservation
    /// </summary>
    private void FinalPaymentWithoutTA_M()
    {
      string filterString;
      filterString = "(ResvPrefix = 'M') AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID <> 7684) ";  //Vendor Base ID 7684 is WSBH
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.FinalPaymentWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Final Payment Letter With TA - M Reservation
    /// </summary>
    private void FinalPaymentWithTA_M()
    {
      string filterString;
      filterString = "(ResvPrefix = 'M') AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID <> 7684) ";  //Vendor Base ID 7684 is WSBH
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.FinalPaymentWithTA_HM,
          (int)LetterType.FinalPaymentWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region Print M reservation with TA or Without TA for WSBH
    /// <summary>
    /// Finals the payment without TA - M reservation
    /// </summary>
    private void FinalPaymentWithoutTA_M_7684()
    {
      string filterString;
      filterString = "(ResvPrefix = 'M') AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID = 7684) ";  //Vendor Base ID 7684 is WSBH
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.FinalPaymentWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Final Payment Letter With TA - M Reservation
    /// </summary>
    private void FinalPaymentWithTA_M_7684()
    {
      string filterString;
      filterString = "(ResvPrefix = 'M') AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      filterString += "AND (VendorBaseID = 7684) ";  //Vendor Base ID 7684 is WSBH
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.ProcessLetterWithTA((int)LetterType.FinalPaymentWithTA_HM,
          (int)LetterType.FinalPaymentWithoutTA_HM, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region Printer R reservation
    /// <summary>
    /// Final Payment Letter without TA - R Resv
    /// </summary>
    private void FinalPaymentWithoutTA_R()
    {
      string filterString;
      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.FinalPaymentWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }

    /// <summary>
    /// Final Payment Letter with TA - R Resv
    /// First print the Final Payment Letter with TA and then 
    /// print Final Payment Letter without TA
    /// </summary>
    private void FinalPaymentWithTA_R()
    {
      string filterString;
      string resvNo;
      DataView dv;

      filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' ";
      filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        dv = this.dsResv.Tables[0].DefaultView;
        resvNo = dv[0].Row["ReservationNumber"].ToString();  //get a reservation # from the first row of view
        printLetter.ProcessLetterWithTA((int)LetterType.FinalPaymentWithTA_R,
          (int)LetterType.FinalPaymentWithoutTA_R, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    #region Check By Phone Form

    private void CheckByPhoneForm()
    {
      DataRow dr = this.dsResv.Tables[0].Rows[0];
      this.dsResv.Tables[0].Clear();
      this.dsResv.Tables[0].Rows.Add(dr);
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.CheckByPhoneForm, this.dsResv, this.printTo, Utility.PrintDuplex);
      }
    }
    #endregion

    /// <summary>
    /// Processes the specified resv no. or specific date range
    /// either print to a printer or preview on screen.
    /// </summary>
    /// <param name="resvNo">The resv no.</param>
    /// <param name="resvStartDate">The resv start date.</param>
    /// <param name="resvEndDate">The resv end date.</param>
    /// <param name="printTo">Print to a printer or screen.</param>
    public string Process(string resvNo, string resvStartDate, string resvEndDate, int printTo)
    {
      string result = string.Empty;
      this.resvNo = resvNo;
      this.resvStartDate = resvStartDate;
      this.resvEndDate = resvEndDate;
      this.printTo = printTo;

      // if it a non-deposit letter, use a deposit balance due, instead of resv balance due.
      //HttpContext.Current.Session.Remove("IsNonDeposit");

      //HttpContext.Current.Session.Remove("IsTA");
      //HttpContext.Current.Session.Remove("ReportObjectTA");

      LetterData letterData = new LetterData();
      this.dsResv = letterData.GetFinalPaymentData(this.resvNo, this.resvStartDate, this.resvEndDate);
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        result = "There is no letter.";
        return result;
      }

      try
      {
        //HttpContext.Current.Session["RptDataset"] = this.dsResv;
        #region Print H Reservation with TA or without TA
        //Final Payment Letter Without TA - H Reservation
        FinalPaymentWithoutTA_H();

        //Final Payment Letter With TA - H Reservation
        FinalPaymentWithTA_H();
        #endregion

        #region Print M Reservation with TA or without TA
        //Final Payment Letter Without TA - M Reservation
        FinalPaymentWithoutTA_M();

        //Final Payment Letter With TA - M Reservation
        FinalPaymentWithTA_M();
        #endregion

        #region Print M Reservation with TA or without TA for WSBH
        // Not use because WSBH use R reservation Typess
        //Final Payment Letter Without TA - M Reservation
        //FinalPaymentWithoutTA_M_7684();

        //Final Payment Letter With TA - M Reservation
        //FinalPaymentWithTA_M_7684();
        #endregion

        #region Print R Reservation
        //Final Payment Letter without TA - R Resv
        FinalPaymentWithoutTA_R();

        //Final Payment Letter with TA - R Resv
        FinalPaymentWithTA_R();
        #endregion

        //CheckByPhoneForm();

      }
      catch (Exception ex)
      {
        result = string.Format("Error has been occurred while printing letters. With error message: {0}", ex.Message);
      }

      return result;
    }
  }
}
