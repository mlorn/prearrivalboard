﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MoreLinq;


namespace PreArrivalBoard
{
  public class PreArrivalData
  {
    private const int dashboardAdmin = 195;
    private const int resvAgent = 186;
    private const int salesAssist = 203; // sales Assistant
    private const string adminUser = "ADMIN";
    private const string LetterTable = "Letter";
    private const int preArrivalPackage = 1;

    public DataTable GetPreArrivalCommItinerary()
    {
      using MagnumDataContext magnumDb = new MagnumDataContext();
      return magnumDb.spPreArrivalCommItinerary().ToDataTable();
    }

    public DataTable GetPreArrivalFinalItinerary()
    {
      using MagnumDataContext magnumDb = new MagnumDataContext();
      return magnumDb.spPreArrivalFinalItinerary().ToDataTable();
    }

    public DataTable GetPreArrivalReservation(string identificationNumber)
    {

      using MagnumDataContext magnumDb = new MagnumDataContext();
      return magnumDb.spLetterPreArrival(identificationNumber).ToDataTable();
    }

    public BaseLogic.UserData Login(string userName, string password, string logInAs)
    {
      bool result = false;
      string user = userName.ToUpper();

      BaseLogic.UserData ud = new BaseLogic.UserData();

      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        var loginData = magnumDb.spLogin(userName, password).ToList();

        var adminGroup = (from a in loginData
                          where a.GroupID == dashboardAdmin //Dashboard Admin
                          select a).FirstOrDefault();

        //User not in the Admin group
        if (adminGroup == null)
        {
          HttpContext.Current.Session["Admin"] = string.Empty;

          var salesAssistGroup = (from s in loginData
                                  where s.GroupID == salesAssist //Sales Agent Group
                                  select s).FirstOrDefault();
          if (salesAssistGroup != null)  // log in as Sales Assistant group
          {
            ud.userID = salesAssistGroup.UserID;
            ud.userName = salesAssistGroup.UserName;
            ud.groupID = (int)salesAssistGroup.GroupID;
            ud.adminUserName = string.Empty;
            ud.fullName = salesAssistGroup.FullName;
            ud.salesAssistID = salesAssistGroup.UserID;
            ud.emailAddress = salesAssistGroup.EmailAddress;
            result = true;

          }
          else
          {
            var salesGroup = (from s in loginData
                              where s.GroupID == resvAgent //Sales Agent Group
                              select s).FirstOrDefault();
            if (salesGroup != null)  // log in as Sales Agent
            {
              ud.userID = salesGroup.UserID;
              ud.userName = salesGroup.UserName;
              ud.groupID = (int)salesGroup.GroupID;
              ud.adminUserName = string.Empty;
              ud.fullName = salesGroup.FullName;
              ud.emailAddress = salesGroup.EmailAddress;
              result = true;
            }
          }
        }
        else  //admin group which can log in as Admin or impersonat a RA
        {
          HttpContext.Current.Session["Admin"] = adminUser;
          if (logInAs == "All")  //logging as Admin user
          {
            ud.userID = adminGroup.UserID;
            ud.userName = adminGroup.UserName;
            ud.groupID = (int)adminGroup.GroupID;
            ud.adminUserName = adminUser;
            ud.fullName = adminGroup.FullName;
            result = true;
          }
          else   //logging as individual (impersonat) 
          {
            //var individualData = magnumDb.spLogInGetUser(userName).FirstOrDefault();
            var individualData = (from s in loginData
                                  where s.UserName.ToUpper() == userName.ToUpper() //Sales Agent Group
                                  select s).FirstOrDefault();
            if (individualData != null)
            {
              ud.userID = individualData.UserID;
              ud.userName = individualData.UserName;
              ud.groupID = (int)individualData.GroupID;
              ud.adminUserName = string.Empty;
              ud.fullName = individualData.FullName;
              ud.emailAddress = individualData.EmailAddress;
              result = true;
            }
          }
        }
      }
      ud.isLoggedIn = result;

      return ud;
    }

    /// <summary>
    /// Inserts a record into the Letter table to indicate a pre-arrival package has been sent.
    /// </summary>
    /// <param name="itineraryID">The itinerary identifier.</param>
    public void InsertLetter(int itineraryID)
    {

      int? nextKey = 0;

      using MagnumDataContext magnumDb = new MagnumDataContext();

      magnumDb.GetNextKey(666, "Letter", ref nextKey);
      Letter letter = new Letter();
      letter.LetterID = (int)nextKey;
      letter.ItineraryID = itineraryID;
      letter.LetterKindID = preArrivalPackage;
      letter.DateSent = DateTime.Now;
      letter.IsEnabled = true;

      magnumDb.Letters.InsertOnSubmit(letter);
      magnumDb.SubmitChanges();

    }

    /// <summary>
    /// Removes the itinerary/reservation from displaying on the board.
    /// </summary>
    /// <param name="itineraryID">The itinerary identifier.</param>
    public void RemoveItinerary(int itineraryID)
    {
      int? nextKey = 0;

      using MagnumDataContext magnumDb = new MagnumDataContext();

      var itineraryData = (from l in magnumDb.Letters
                           where l.ItineraryID == itineraryID && l.IsEnabled == true
                           select l).FirstOrDefault();
      if (itineraryData != null)
      {
        itineraryData.IsEnabled = false;
        magnumDb.SubmitChanges();
      }
      else
      {
        magnumDb.GetNextKey(666, "Letter", ref nextKey);
        Letter letter = new Letter();
        letter.LetterID = (int)nextKey;
        letter.ItineraryID = itineraryID;
        letter.LetterKindID = preArrivalPackage;
        letter.DateRemoved = DateTime.Now;
        letter.IsEnabled = false;

        magnumDb.Letters.InsertOnSubmit(letter);
        magnumDb.SubmitChanges();
      }
    }

  }
}