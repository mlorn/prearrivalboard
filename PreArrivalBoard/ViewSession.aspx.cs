﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PreArrivalBoard
{
  public partial class ViewSession : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ShowSessions();
      ShowUser();
      ShowControlDetailData();
    }

    protected void lb1_Click(object sender, EventArgs e)
    {
      //Report.CreateClientItinerary(268102, string.Empty, string.Empty, string.Empty);
      //Response.Redirect("/letter.UILayer/Letter/LetterMainViewer.aspx", true);

    }


    protected void ShowControlDetailData()
    {
      //DataView myCDD = (DataView)Session["ControlDetailData"];
      //gvControlDetailData.DataSource = myCDD;
      //gvControlDetailData.DataBind();
    }
    protected void ShowUser()
    {
      /*
        BaseLogic.UserData ud = BaseLogic.GetUserData();
        string sUserData = "";

        foreach (var field in typeof(BaseLogic.UserData).GetFields())
        {
            sUserData = string.Format("{0}<br /> {1}: {2}", sUserData, field.Name, field.GetValue(ud));
        }

      */
      try
      {

        lblUserData.Text = Session["UserName"].ToString();
      }
      catch
      {

      }

    }
    protected void ShowSessions()
    {
      string arS = "";
      foreach (string sKey in Session.Keys)
      {
        arS = arS + sKey + ": " + Session[sKey] + "<br />";
      }
      lblSession.Text = arS;
    }
    protected void btnKillSwitch_Click(object sender, EventArgs e)
    {
      //BaseLogic.KillSession();
      Session.RemoveAll();
      /*
      for (short i = (short)HttpContext.Current.Session.Count; i > 0; i-- )
      {
          string sMe = Session.Keys[i-1].ToString();
          if (sMe != "UserData" && sMe != "UserID")
          { Session.Remove(sMe); }
      }

      Response.Redirect(Request.RawUrl); 
      //Session.RemoveAll();
    */
    }
    protected void btnAddSession_Click(object sender, EventArgs e)
    {
      Button btnMe = (Button)sender;
      TextBox tbSessionName = (TextBox)btnMe.NamingContainer.FindControl("tbSessionName");
      TextBox tbSessionValue = (TextBox)btnMe.NamingContainer.FindControl("tbSessionValue");
      string sName = tbSessionName.Text;
      string sValue = tbSessionValue.Text;
      Session[sName] = sValue;
      Response.Redirect("session.aspx");
    }
    protected void btnMakeMessage_Click(object sender, EventArgs e)
    {
      //TestMessage();
    }


  }
}
