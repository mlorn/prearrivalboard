﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewSession.aspx.cs" Inherits="PreArrivalBoard.ViewSession" Trace="false" %>


<html>
<head runat="server">
  <title></title>
  <link type="text/css" href="/css/common.css" rel="Stylesheet" />
  <link type="text/css" href="/css/screen.css" rel="Stylesheet" />
  <link type="text/css" href="/css/styles.css" rel="Stylesheet" />
  <link type="text/css" href="/css/Portal.css" rel="Stylesheet" />
  <style type="text/css">
    .Search {
      font-size: small;
    }

    .alternate {
      background-color: white;
    }

    .ActiveRow {
    }

    #ClientTable {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      border-left: 1px solid black;
      border-right: 1px solid black;
    }

    #MessageGrid {
      visibility: hidden;
    }

    #ClientLabel {
    }

    #ClientField {
      font-weight: bold;
    }

    .width-18 {
      width: 18% !important
    }

    .width-25 {
      width: 25% !important
    }

    .width-33 {
      width: 33% !important
    }

    .width-50 {
      width: 50% !important
    }

    .width-65 {
      width: 65% !important
    }

    .width-100 {
      width: 100% !important
    }

    /* FORM CLASSES */
    ul.form {
      margin: 10px 0 0 0;
      padding: 0;
      list-style-type: none !important;
    }

      ul.form li {
        width: 33%;
        float: left;
        padding: 10px 0;
        border-bottom: 0px solid #D4D4D4;
      }

        ul.form li.margin-right {
          margin-right: 3% !important
        }

    body {
      margin-left: 10px
    }
  </style>

  <style type="text/css">
    round {
      -moz-border-radius: 35px;
      border-radius: 35px;
    }
  </style>
</head>
<body>
  <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
      Session:<br />
      <asp:Label ID="lblSession" runat="server" /><br />
      User:<br />
      <asp:Label ID="lblUserData" runat="server" />
      <br />
      <asp:GridView ID="gvControlDetailData" runat="server" AutoGenerateColumns="true" Caption="ControlDetailData Session" EmptyDataText="There is no data" />
      <hr />
      Session:
      <asp:TextBox ID="tbSessionName" runat="server" CssClass="round" /><br />
      Value:
      <asp:TextBox ID="tbSessionValue" runat="server" CssClass="round" /><br />
      <asp:Button ID="btnAddSession" runat="server" Text="Add To Session" OnClick="btnAddSession_Click" />
      <asp:Button ID="btnKillSwitch" runat="server" Text="Kill Session" OnClick="btnKillSwitch_Click" />
      <asp:Button ID="btnMakeMessage" runat="server" Text="Generate Message" OnClick="btnMakeMessage_Click" />
    </div>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
      OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
    <asp:FormView ID="FormView1" runat="server">
    </asp:FormView>

    <hr />
  </form>
</body>
</html>
