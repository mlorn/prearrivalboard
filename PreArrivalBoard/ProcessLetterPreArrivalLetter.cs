using System;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
//using System.Windows.Forms;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;

namespace PreArrivalBoard
{
  /// <summary>
  /// Print revision letters or Refund Letters or Remittance Letters
  /// Letters can be printed individually by a Resv No or all 
  /// letters that have been changed and approved or a deposit/payment
  /// reference number.
  /// </summary>
  public class ProcessLetterPreArrivalLetter
  {
    private DataSet dsResv;
    private DataSet dsAirResv = null;
    private string resvNo;
    //private string depositNo;
    private string revisionTag;
    private int printTo;
    private List<PrintedLetter> printedLetter = new List<PrintedLetter>();
    private LetterData letterData;
    private PrintLetter printLetter;

    public ProcessLetterPreArrivalLetter(PrintLetter printLetter)
    {
      this.printLetter = printLetter;
    }

    #region Print Pre-Arrival Letters
    /// <summary>
    /// Additional Guest
    /// </summary>
    private void AdditionalGuest()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Additional_Guests_Form.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Air travel information.
    /// </summary>
    private void AirTravelInfo()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Air_Travel_Form.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Cars rental.
    /// </summary>
    private void CarRental()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Car_Rental_Form_Aug_2021.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Cars rental non-festive
    /// </summary>
    private void CarRentalNonFestive()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Car_Rental_Non_Festive_Form_2021_2022.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Equipments rental reservation.
    /// </summary>
    private void EquipmentRentalReservation()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Equipment_Rental_Order_Form_2021_2022.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Fitnesses equipment rental.
    /// </summary>
    private void FitnessEquipmentRental()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Fitness_Equipment_Rental_Form_2021_2022.ToString(), this.dsResv, this.printTo);
      }
    }
    /// <summary>
    /// Foods package.
    /// </summary>
    private void FoodPackage()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.First_Night_Provisioning_Form_2021.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Forms fitness.
    /// </summary>
    private void FormFitness()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Form_Fitness_Form_2020.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Groceries order.
    /// </summary>
    private void GroceryOrder()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Grocery_Order_Form_2021_2022.ToString(), this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// St. barth beach information.
    /// </summary>
    private void StBarthBeachInfo()
    {
      LetterList letterList = new LetterList();
      letterList.FilePath = Properties.Settings.Default.StaticURL;
      letterList.LetterName = $"{LetterType.St_Barth_Beaches_info.ToString()}.pdf";
      printLetter.fileList.Add(letterList);
    }

    /// <summary>
    /// St. barth boat brochure.
    /// </summary>
    private void StBarthBoatBrochure()
    {
      LetterList letterList = new LetterList();
      letterList.FilePath = Properties.Settings.Default.StaticURL;
      letterList.LetterName = $"{LetterType.St_Barth_Boat_Brochure_Info_2021_2022.ToString()}.pdf";
      printLetter.fileList.Add(letterList);
    }

    /// <summary>
    /// St. barth restaurant list.
    /// </summary>
    private void StBarthRestaurantList()
    {
      LetterList letterList = new LetterList();
      letterList.FilePath = Properties.Settings.Default.StaticURL;
      letterList.LetterName = $"{LetterType.St_Barth_Restaurant_List_Info_2021.ToString()}.pdf";
      printLetter.fileList.Add(letterList);
    }

    /// <summary>
    /// Welcomes to st. barthelemy.
    /// </summary>
    private void WelcomeToStBarthelemy()
    {
      LetterList letterList = new LetterList();
      letterList.FilePath = Properties.Settings.Default.StaticURL;
      letterList.LetterName = $"{LetterType.Welcome_General_Info_2022.ToString()}.pdf";
      printLetter.fileList.Add(letterList);
    }

    /// <summary>
    /// Wines list.
    /// </summary>
    private void WineList()
    {
      string filterString;

      filterString = "FirstLogResv = 'Y'";
      this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
      if (this.dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintToSeperateFile(LetterType.Wine_List_Form_2021_2022.ToString(), this.dsResv, this.printTo);
      }


    }

    #endregion


    /// <summary>
    /// Processes Pre-Arrival Letters
    /// </summary>
    /// <param name="identificationNumber">Either an ItineraryID or a Reservation Number.</param>
    /// <param name="printTo">The print to.</param>
    /// <returns></returns>
    public string Process(string identificationNumber, int printTo)
    {
      string result = string.Empty;
      string ListOfResvNo = string.Empty;

      //this.revisionTag = revisionTag;
      this.printTo = printTo;
      //if languageID has not been set, set it to 1 as English default language
      /*
      if (languageID == 0)
      {
        languageID = 1;
      }
      */
      this.letterData = new LetterData();
      this.dsResv = this.letterData.GetPreArrivalData(identificationNumber);
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        result = "There is no letter.";
        return result;
      }

      foreach (var r in this.dsResv.Tables[0].AsEnumerable())
      {
        ListOfResvNo += $"{r.Field<string>("ReservationNumber")}, ";
      }

      //remove the last comma and a space.
      if (ListOfResvNo.Length > 0)
      {
        ListOfResvNo = ListOfResvNo.Remove(ListOfResvNo.Length - 2, 2);
      }

      //store 
      HttpContext.Current.Session["ListOfResvNo"] = ListOfResvNo;

      try
      {

        // clear reservation from the list.
        //ReservationList.ListOfReservations.Clear();
        #region Print Pre-Arrival Letters
        WelcomeToStBarthelemy();
        AdditionalGuest();
        AirTravelInfo();
        StBarthRestaurantList();
        GroceryOrder();
        FoodPackage();
        WineList();
        //CarRental();
        CarRentalNonFestive();
        EquipmentRentalReservation();
        FitnessEquipmentRental();
        StBarthBeachInfo();
        FormFitness();
        StBarthBoatBrochure();
        #endregion
      }
      //catch (Exception ex)
      //{
      //  result = false;
      //  MessageBox.Show("Error has been occurred while printing letters. With error message: " + ex.Message);
      //}
      finally
      {
        // set this to true so that it will ask to select a printer
        //PrintLetter.FirstTime = true;
        result = string.Empty;
        //ReservationList.AddReservationToList(this.dsResv);
      }

      return result;

    }

  }
}
