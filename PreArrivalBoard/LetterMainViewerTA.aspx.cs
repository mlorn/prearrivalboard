﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Net.Mail;
using DevExpress.XtraReports.UI;

namespace PreArrivalBoard
{
  public partial class LetterMainViewerTA : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      XtraReport rpt;
      rpt = GetReportTA();
      Page.Header.Title = rpt.Tag.ToString();
      ReportViewer1.Report = GetReportTA();
      ReportViewer1.DataBind();
    }

    protected XtraReport GetReportTA()
    {
      if ((XtraReport)Session["ReportObjectTA"] == null)
      {
        Session["ReportObjectTA"] = new XtraReport();
      }

      //Session["IsTA"] = false;
      Session.Remove("IsTA");
      return (XtraReport)Session["ReportObjectTA"];
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
      Session["IsTA"] = false;
    }

    protected void ReportViewer1_Unload(object sender, EventArgs e)
    {
      ReportViewer1.Report = null;
      //Session["IsTA"] = false;
      //Session.Remove("ReportObjectTA");
    }

    protected void SaveFormat_OnChanged(object sender, EventArgs e)
    {


    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
      string url = string.Empty;

      //ReportToolbarComboBox  toolbarComboBox = (TopReportToolbar.Items[16] as ReportToolbarComboBox);


      // String str = sformat.Elements[0].Value.ToString();


      /*
      try
      {
        // Create a new memory stream and export the report into it as PDF.
        MemoryStream mem = new MemoryStream();
        ReportViewer1.Report.ExportToPdf(mem);
        // Create a new attachment and put the PDF report into it.
        mem.Seek(0, System.IO.SeekOrigin.Begin);
        Attachment att = new Attachment(mem, ReportViewer1.Report.Tag.ToString(), "application/pdf");

        Session["ReportViewer"] = att;
        
        // Create a new message and attach the PDF report to it.
        MailMessage mail = new MailMessage();
        mail.Attachments.Add(att);

        // Specify sender and recipient options for the e-mail message.
        mail.From = new MailAddress("mlorn@wimco.com", "Mao Lorn");
        mail.To.Add(new MailAddress("mlorn@wimco.com", "Mao Lorn"));

        // Specify other e-mail options.
        mail.Subject = ReportViewer1.Report.Tag.ToString();
        mail.Body = "This is a test e-mail message sent by an application.";

        // Send the e-mail message via the specified SMTP server.
        SmtpClient smtp = new SmtpClient("mail.wimco.com");
        smtp.Send(mail);
        
        // Close the memory stream.
        mem.Close();
        mem.Flush();
        Response.Redirect("~/Letter.UILayer/Letter/WebEMail.aspx");
      }
      catch (Exception ex)
      {
        showMessage("There is a problem with sending the report.");
      }
      */

      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "WebEMail.aspx");
      Response.Redirect(url);

    }

    private void showMessage(string result)
    {
      Label lblMessage = new Label();
      lblMessage.Text = "<script language='javascript'>" + Environment.NewLine +
         "window.alert(" + "'" + result + "'" + ")</script>";

      // add the label to the page to display the alert
      Page.Controls.Add(lblMessage);
    }

    protected void btnEmail2_Click(object sender, EventArgs e)
    {

    }

    protected void btnSelectDifferentLetter_Click(object sender, EventArgs e)
    {
      string url = string.Empty;

      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "SelectionCriteria/AskLetterKind.aspx");

      Response.Redirect(url);
    }
  }
}
