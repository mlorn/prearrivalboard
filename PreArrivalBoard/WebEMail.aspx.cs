﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Net.Mail;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;
//using DevExpress.CodeParser;

namespace PreArrivalBoard
{
  public partial class WebEMail : System.Web.UI.Page
  {
    private bool testing = false;
    private const int maxFileSize = 100000000;
    private const int templateKindID = 11;  //pre-arrival package info
    private const int messageKindID = 9; //Letter has been sent
    private XtraReport rpt = null;
    private XtraReport rptTA = null;
    private SmtpEmail mail = null;
    private DataSet dsResv;
    private LetterList letterList;
    private PrintLetter printLetter = new PrintLetter();
    private int printTo;
    Properties.Settings appSettings = new Properties.Settings();
    private string fileName = string.Empty;
    private string itineraryStatementName = "Itinerary Statement";
    Attachment att = null;
    List<MemoryStream> memList;
    BaseLogic.UserData userData;
    string clientName = string.Empty;
    List<WimcoBaseLogic.BusinessLogic.UploadTicket> listOfTicket = new List<WimcoBaseLogic.BusinessLogic.UploadTicket>();
    List<WimcoBaseLogic.BusinessLogic.Recipient> listOfrecipient = new List<WimcoBaseLogic.BusinessLogic.Recipient>();
    List<string> listOfXheader = new List<string>();

    public string TemplateKindID { get; set; }
    public string EmailOriginal { get; set; }
    public string TemplateID;


    private int GetUserID()
    {
      int userID = 0;

      Properties.Settings appSetting = new Properties.Settings();

      try
      {
        testing = (bool)Session["Testing"];
      }
      catch
      {
        //TODO: Better handle exception
        testing = false;
      }

      if (appSetting.IsTesting || testing)
      {
        userID = 1;
      }
      else
      {
        BaseLogic.UserData ud;
        try
        {
          ud = (BaseLogic.UserData)Session["UserData"];
        }
        catch
        {
          //TODO: Better handle exception
          ud.userID = 1;
          ud.emailAddress = string.Empty;
        }

        userID = ud.userID;
      }

      return userID;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      string clientEmail = string.Empty;
      LetterData letterData;
      string resvNo = string.Empty;
      //string clientName = string.Empty;
      string subject = string.Empty;
      string templateText = string.Empty;
      printTo = Utility.Screen;
      List<LetterList> letterList;
      string listOfResvNo = string.Empty;

      //this.Form.Target = "_blank";

      //Page.Header.DataBind();

      if (!IsPostBack)
      {

        (subject, templateText) = GetEmailTemplate(templateKindID);

        try
        {
          letterList = Session["FileList"] as List<LetterList>;
        }
        catch
        {
          letterList = null;
        }

        grdLetterList.DataSource = letterList;
        grdLetterList.DataBind();

        try
        {
          clientEmail = Session["ClientEmail"].ToString();
        }
        catch
        {
          clientEmail = string.Empty;
        }

        try
        {
          resvNo = Session["ReservationNumber"].ToString();
        }
        catch
        {
          resvNo = string.Empty;
        }

        try
        {
          clientName = Session["ClientName"].ToString();
        }
        catch
        {
          clientName = string.Empty;
        }

        letterData = new LetterData();
        //htmlEditorWebEMail.Html = letterData.GetEmailSignature(1);
        BaseLogic.UserData ud;
        try
        {
          Properties.Settings appSetting;
          appSetting = new Properties.Settings();

          try
          {
            ud = (BaseLogic.UserData)Session["UserData"];
          }
          catch
          {
            //TODO: Better handle exception
            ud.userID = 1;
            ud.emailAddress = string.Empty;
          }

          try
          {
            testing = (bool)Session["Testing"];
          }
          catch
          {
            //TODO: Better handle exception
            testing = false;
          }

          if (appSetting.IsTesting || testing)
          {
            txtFromEmail.Text = "mlorn@wimco.com";
            //txtSubject.Text = rpt.Tag.ToString();
            //txtBCEmail.Text = "mlorn@wimco.com";
            txtToEmail.Text = "mlorn@wimco.com";
            //lblUserID.Text = "1";
            ud.userID = 1;
          }
          else
          {
            //txtFromEmail.Text = ud.emailAddress;
            txtFromEmail.Text = Properties.Settings.Default.SenderEmail;
            txtToEmail.Text = clientEmail;
          }

          try
          {
            listOfResvNo = Session["ListOfResvNo"].ToString();
          }
          catch
          {
            listOfResvNo = string.Empty;
          }

          subject = $"{subject.Trim()}, Reservation(s): {listOfResvNo}";

          txtSubject.Text = subject;

          htmlEditorWebEMail.Html = templateText;

          /*
          if (rpt != null)
          {
            txtAttachment.Text = rpt.Tag.ToString();
            chkRpt.Value = true;
          }
          else
          {
            chkRpt.Value = false;
          }

          if (rptTA != null)
          {
            txtAttachmentTA.Text = rptTA.Tag.ToString();
            chkRptTA.Value = true;
          }
          else
          {
            chkRptTA.Value = false;
          }
          */
          //txtCCEmail.Text = "mlorn@wimco.com";
          //letterData = new LetterData();
          //htmlEditorWebEMail.Html = letterData.GetEmailSignature(ud.userID);
        }
        catch
        {

        }

      }
      /*
      else
      {
        txtSubject.Text = rpt.Tag.ToString();
      }
      */
    }

    protected (string subject, string templateText) GetEmailTemplate(int templateKindID)
    {
      string subject = string.Empty;
      string templateText = string.Empty;

      using MagnumDataContext magnumDb = new MagnumDataContext();

      var emailTemplateData = (from t in magnumDb.EmailTemplates
                               where t.TemplateKindID == templateKindID && t.IsEnabled == true
                               select t).FirstOrDefault();
      if (emailTemplateData != null)
      {
        subject = emailTemplateData.Subject;
        templateText = emailTemplateData.TemplateTextHTML;
      }

      return (subject, templateText);
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
      string temp = "testing";
    }
    protected void htmlEditorWebEMail_CustomDataCallback(object sender, DevExpress.Web.CustomDataCallbackEventArgs e)
    {
      //switch (e.Parameter)
      //{
      //  case "SendEmail":
      //    SendEmail();
      //    break;
      //}
    }

    private void SaveHtml(string html)
    {
      BaseLogic.UserData ud;

      try
      {
        ud = (BaseLogic.UserData)Session["UserData"];
      }
      catch
      {
        ud.userID = 0;
      }

      if (ud.userID > 0)
      {
        bool result = false;
        LetterData letterData;
        letterData = new LetterData();
        result = letterData.SaveSignature(ud.userID, html);
      }

    }


    protected void txtBCEMail_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
      SendEmailGroupWise();
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('The email has been sent sucessfully');", true);
      ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "closePage", "window.close();", true);
    }

    /// <summary>
    /// Send an email via GroupWise API.
    /// </summary>
    private void SendEmailGroupWise()
    {
      CheckBox attached = null;
      Label lblFilePath = null;
      Label lblLetterName = null;
      Label lblResvNumber = null;
      Label balanceDue = null;
      string resvNo = string.Empty;
      bool uploadSuccessed = false;
      string clientEmail = string.Empty;
      string filePath = string.Empty;
      string requestUri = string.Empty;

      // reset the message list because it's defined as static variable
      MessageList.ListOfMessages.Clear();

      string fileType = string.Empty;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string physicalFileName = string.Empty;
      List<string> fileNames = new List<string>();

      string fileName;
      string letterName = string.Empty;
      string ticketID = string.Empty;

      int fileID;
      int fileSize;
      byte[] fileContent = null;

      string result = string.Empty;
      List<MessageList> listOfMessages = new List<MessageList>();

      int userID = 0;
      string userName = string.Empty;
      string clientLastName = string.Empty;

      string subject = string.Empty;
      string listOfResvNo = string.Empty;
      string[] resvNumbers;
      int itineraryID;

      Properties.Settings appSetting;
      appSetting = new Properties.Settings();
      //List<WimcoBaseLogic.BusinessLogic.UploadTicket> listOfTicket = new List<WimcoBaseLogic.BusinessLogic.UploadTicket>();

      try
      {
        testing = (bool)Session["Testing"];
      }
      catch
      {
        //TODO: better handle exception
      }

      if (appSetting.IsTesting || testing)
      {
        userID = 1; // Mao
        userName = "Mao";
      }
      else
      {
        try
        {
          userData = (BaseLogic.UserData)Session["UserData"];
          userID = userData.userID;
          userName = userData.userName;
        }
        catch
        {
          //TODO: better handle exception
        }
      }

      try
      {
        clientLastName = Session["ClientLastName"].ToString();
      }
      catch
      {
        clientLastName = string.Empty;
      }

      // client last name length is greater than 20, truncates it 
      // because the field Message.ToContact is defined as varchar(20)
      if (clientLastName.Length > 20)
      {
        clientLastName = clientLastName.Remove(20, clientLastName.Length - 20);
      }

      memList = new List<MemoryStream>();

      // Sender Email
      string fromEmailAddres = txtFromEmail.Text.Trim();
      if (fromEmailAddres.ToLower() == "conciergesbh@wimco.com")
      {
        userID = 90001;
      }

      // To Email
      string toEmailAddress = txtToEmail.Text.Trim();
      if (toEmailAddress.Length > 0)
      {
        AddRecepient(toEmailAddress, WimcoBaseLogic.BusinessLogic.DistributionType.TO);
      }

      // CC Email
      string ccEmailAddress = txtCCEmail.Text.Trim();
      if (ccEmailAddress.Length > 0)
      {
        AddRecepient(ccEmailAddress, WimcoBaseLogic.BusinessLogic.DistributionType.CC);
      }

      // B Copy Email
      string bcEmailAddress = txtBCEmail.Text.Trim();
      if (bcEmailAddress.Length > 0)
      {
        AddRecepient(bcEmailAddress, WimcoBaseLogic.BusinessLogic.DistributionType.BC);
      }

      // subject line
      if (txtSubject.Text.Length > 0)
      {
        subject = txtSubject.Text;
      }
      else
      {
        if (rpt != null)
        {
          subject = rpt.Tag.ToString();
        }
        if (rptTA != null)
        {
          subject = rptTA.Tag.ToString();
        }
      }

      // process only if there is a recepient
      if (listOfrecipient.Count > 0)
      {
        try
        {
          //fileType = cmbSendFormat.SelectedItem.Value.ToString();
          fileType = "pdf";

          // loop through the grid to see if any letter has been selected to be sent.
          for (int i = 0; i < grdLetterList.Rows.Count; i++)
          {
            lblFilePath = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblFilePath");
            filePath = lblFilePath.Text;

            lblLetterName = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
            fileName = lblLetterName.Text;
            //include file path
            fileNames.Add($@"{filePath}\{fileName}");

            // test to see if a letter has been selected.
            attached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");
            if (attached.Checked)
            {
              balanceDue = (Label)grdLetterList.Rows[i].Cells[2].FindControl("lblBalanceDue");
              lblResvNumber = (Label)grdLetterList.Rows[i].Cells[3].FindControl("lblResvNumber");
              resvNo = lblResvNumber.Text;

              if (filePath.Contains("http"))
              {
                requestUri = $"{filePath}/{fileName}";
                physicalFileName = $@"{Properties.Settings.Default.LetterFolder}\{fileName}";
                using HttpClient client = new HttpClient();

                HttpResponseMessage msg = client.GetAsync(requestUri).Result;
                if (msg.IsSuccessStatusCode)
                {
                  using var fs = new FileStream(physicalFileName, FileMode.Create, FileAccess.Write);

                  // create a new file to write to
                  var contentStream = msg.Content.ReadAsStreamAsync().Result; // get the actual content stream
                  fileContent = msg.Content.ReadAsByteArrayAsync().Result;
                  contentStream.CopyTo(fs); // copy that stream to the file stream
                  fs.Flush(); // flush back to disk before disposing
                }
              }
              else
              {
                // physicalFileName is pointed to a physical file on a server
                physicalFileName = $@"{filePath}\{fileName}";
                using FileStream fs = new FileStream(physicalFileName, FileMode.Open, FileAccess.Read);

                fileSize = Convert.ToInt32(fs.Length);
                fileContent = new byte[fileSize];
                fs.Read(fileContent, 0, fileSize);
              }


              WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(fileContent, fileName);
              if (uploadTicket.Success)
              {
                WimcoBaseLogic.BusinessLogic.UploadTicket ticket = new WimcoBaseLogic.BusinessLogic.UploadTicket();
                ticket.Ticket = uploadTicket.Ticket;
                ticket.Filename = fileName;
                // the list of tickets will be added to a sending email
                listOfTicket.Add(ticket);

                letterName = fileName;
                if (!filePath.Contains("http"))  //static URL file does not include a reservation number with a file name.
                {
                  // remove (M132456).pdf) from letter name which starts with "(".  For example, Reservation Confirmation (M132456).pdf
                  int index = letterName.IndexOf("(") - 1;
                  letterName = letterName.Remove(index, letterName.Length - index);
                }

                uploadSuccessed = true;
                listOfXheader.Add(string.Format("X-Wimco-LetterType={0}", letterName));
              }

            }
          }

          //at less one letter has been attached successfully.
          if (uploadSuccessed)
          {
            if (resvNo.Length > 0)
            {
              listOfXheader.Add(string.Format("X-Wimco-ReservationNumber={0}", resvNo));
            }
          }

          // call to check if additional attachments have been uploaded
          AdditionalAttachment();

          WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn gwsr = new WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn();
          /*
          string html = htmlEditorWebEMail.Html;

          byte[] bytes = System.Text.Encoding.Default.GetBytes(html);
          html = System.Text.Encoding.UTF8.GetString(bytes);
          */

          // recipients(s)
          WimcoBaseLogic.BusinessLogic.Recipient[] recipients = new WimcoBaseLogic.BusinessLogic.Recipient[listOfrecipient.Count];
          int j = 0;
          foreach (var r in listOfrecipient)
          {
            recipients[j] = new WimcoBaseLogic.BusinessLogic.Recipient();
            recipients[j].email = r.email;
            recipients[j].displayName = string.Empty;
            recipients[j].distType = r.distType;
            recipients[j].recipType = r.recipType;
            j++;
          }

          //attachement via ticket(s) 
          WimcoBaseLogic.BusinessLogic.UploadTicket[] uploadTickets = new WimcoBaseLogic.BusinessLogic.UploadTicket[listOfTicket.Count];
          j = 0;
          // add a list of tickets to the Upload Ticket.
          foreach (var t in listOfTicket)
          {
            uploadTickets[j] = new WimcoBaseLogic.BusinessLogic.UploadTicket();
            uploadTickets[j].Ticket = t.Ticket;
            uploadTickets[j].Filename = t.Filename;
            j++;
          }

          listOfXheader.Add(string.Format("X-Wimco-AgentID={0}", userID));

          // xHeader(s)
          string[] xHeaders = listOfXheader.ToArray();

          // send the e-mail message via GroupWise
          gwsr = WimcoBaseLogic.BaseLogic.Groupwise_SendGWEmail(userID, subject, htmlEditorWebEMail.Html, string.Empty, recipients, xHeaders, uploadTickets);

          //if sent successfully, add a message to the Message table to indicate that a letter has been sent.
          if (gwsr.Success)
          {
            try
            {
              clientEmail = Session["ClientEmail"].ToString();
              if (fileNames.Count > 0)
              {
                foreach (var f in fileNames)
                {
                  if (File.Exists(f))
                  {
                    try
                    {
                      File.Delete(f);
                    }
                    catch
                    {
                      //TODO: better handle exception
                    }
                  }
                }
              }
            }
            catch
            {
              clientEmail = string.Empty;
            }

            //clientEmail = "mlorn@wimco.com";
            // update the Message Table an letter has been sent to a client only, reset letter (remove) if sending to a client email.
            //if (clientEmail == txtToEmail.Text)
            //{
            try
            {
              listOfResvNo = Session["ListOfResvNo"].ToString();
            }
            catch
            {
              listOfResvNo = string.Empty;
            }

            if (listOfResvNo.Length > 0)
            {
              resvNumbers = listOfResvNo.Split(',');
              for (int i = 0; i < resvNumbers.Length; i++)
              {
                MessageList.AddMessageToList(listOfMessages, resvNumbers[i], $"Magnum - Pre-Arrival Package", userID, userName, clientLastName, messageKindID);
              }
            }
            // add message to the Message table
            LetterDataBridge letterDataBridge = new LetterDataBridge();
            result = letterDataBridge.AddMessage(listOfMessages);
            PreArrivalData preArrivalData = new PreArrivalData();
            try
            {
              itineraryID = Convert.ToInt32(Session["Itinerary"]);
            }
            catch
            {
              //TODO: better handle exception
              itineraryID = 0;
            }
            preArrivalData.InsertLetter(itineraryID);
            //}
            //showMessage("The email has been sent.");
            lblErrorMessage.Text = "The email has been sent.";
            string startUpScript = string.Format("window.parent.HidePopupAndShowInfo('Server','{0}');", "Email has been sent.");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ANY_KEY", startUpScript, true);

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('The email has been sent sucessfully');", true);
            //ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "closePage", "window.close();", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePage", "window.close();", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePage", "setTimeout('window.close()',40000);", true);
          }
          else
          {
            // Failed. Why?
            string ErrMsg = gwsr.Message;
            int StatusCode = gwsr.StatusCode;
            // slack it!
            WimcoBaseLogic.BaseLogic.SendSlackDebugMessage("GW Email Send Fail: " + ErrMsg + "  StatusCode: " + StatusCode.ToString());
            showMessage("GW Email Send Fail: " + ErrMsg + "  StatusCode: " + StatusCode.ToString());
            lblErrorMessage.Text = "GW Email Send Fail: " + ErrMsg + "  StatusCode: " + StatusCode.ToString();
          }
        }
        catch (Exception ex)
        {
          showMessage("There is a problem with sending the email. Error Message: " + ex.Message);
          lblErrorMessage.Text = "There is a problem with sending the email. Error Message: " + ex.Message;
        }
        finally
        {
          // clear memory after the attaching
          foreach (MemoryStream mem in memList)
          {
            // Close the memory stream.
            mem.Close();
            mem.Flush();
          }
        }
      }
      else
      {
        showMessage("Please enter the To email and make sure that is a correct format.");
        lblErrorMessage.Text = "Please enter the To email and make sure that is a correct format.";
      }
    }

    /// <summary>
    /// Add recepient which can be TO email, CC email, BC email
    /// Keep track of emails that being added
    /// </summary>
    /// <param name="emailAddress">Email Address can contain multiple emails</param>
    /// <param name="distType">Distruction Type can be: TO, CC, or BC</param>
    private void AddRecepient(string emailAddress, WimcoBaseLogic.BusinessLogic.DistributionType distType)
    {
      WimcoBaseLogic.BusinessLogic.Recipient recipient;

      string[] emails;
      char[] seperator = { ';', ',' };
      string validedEmail = string.Empty;

      // email addresses in the input box are seperated by either ";" or ","
      emails = emailAddress.Split(seperator);
      foreach (var email in emails)
      {
        try
        {
          // use the MailAddress class contructor to validate an email address
          MailAddress validEmail = new MailAddress(email.Trim());
          validedEmail = validEmail.Address;
        }
        catch
        {
          //skip invalid email
          continue;
        }
        recipient = new WimcoBaseLogic.BusinessLogic.Recipient();
        recipient.email = validedEmail;
        recipient.displayName = string.Empty;
        recipient.distType = distType;
        recipient.recipType = WimcoBaseLogic.BusinessLogic.RecipientType.User;
        //keep track of emails
        listOfrecipient.Add(recipient);
      }
    }

    /// <summary>
    /// Add an attachment by reading a memory stream.
    /// Keep track of file be attached.
    /// Also, keep track of memory that being used.
    /// </summary>
    /// <param name="mem">Memory stream to be read.</param>
    /// <param name="fileName">File Name that will be shown as an attachment which includes file extention</param>
    private void AddAttachment(MemoryStream mem, string fileName)
    {
      int fileSize;
      byte[] fileContent;

      // read the letter from the memory stream for a attachment
      mem.Seek(0, System.IO.SeekOrigin.Begin);
      fileSize = Convert.ToInt32(mem.Length);
      fileContent = new byte[fileSize];
      mem.Read(fileContent, 0, fileSize);
      //add the letter to the attachment
      WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(fileContent, fileName);
      //if an attachment successfully, get a ticket and keep track of tickets
      if (uploadTicket.Success)
      {
        WimcoBaseLogic.BusinessLogic.UploadTicket ticket = new WimcoBaseLogic.BusinessLogic.UploadTicket();
        ticket.Ticket = uploadTicket.Ticket;
        ticket.Filename = fileName;
        // the list of tickets will be added to a sending email
        listOfTicket.Add(ticket);
        // add Xheader to indicate which form has been sent, also remove a file extention
        listOfXheader.Add(string.Format("X-Wimco-FormSent={0}", fileName.Remove(fileName.Length - 4, 4)));
      }
      // keep track of memory being used, so that it can be destroyed later
      memList.Add(mem);
    }

    /// <summary>
    /// Allow reservation Agent to attach other files other then letters
    /// </summary>
    private void AdditionalAttachment()
    {
      int fileSize;
      byte[] fileContent;

      try
      {
        foreach (var f in fuAdditionalAttachment.UploadedFiles)
        {
          // only attach if a file has been uploaded.

          fileName = Path.GetFileName(f.PostedFile.FileName);
          if (fileName.Length > 0)
          {
            Stream uploadFile = f.PostedFile.InputStream;
            fileSize = Convert.ToInt32(uploadFile.Length);

            if (fileSize > 26214400) // bigger that 25mb? CHECK THIS ON UI SIDE.
            {
              // skip the file
              continue;
            }

            fileContent = new byte[fileSize];
            uploadFile.Read(fileContent, 0, fileSize);

            WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(fileContent, fileName);
            if (uploadTicket.Success)
            {
              WimcoBaseLogic.BusinessLogic.UploadTicket ticket = new WimcoBaseLogic.BusinessLogic.UploadTicket();
              ticket.Ticket = uploadTicket.Ticket;
              ticket.Filename = fileName;
              // the list of tickets will be added to a sending email
              listOfTicket.Add(ticket);
            }
          }
        }
      }
      catch
      {
        //TODO: better handle exception
      }
    }

    /*
    private void addMassage(List<MessageList> listOfMessages, string resvNumber, string messageText, int userID, string userName)
    {

      MessageList msg = new MessageList();
      msg.ResvNumber = resvNumber;
      // 2 - relate to reservation message
      msg.LinkType = Utility.ResvMessage;
      msg.MessageThreadNumber = string.Format("{0}-000", resvNumber);
      // 8 - letter has been printed
      msg.MessageKindID = Utility.LetterPrinted;
      // 5 - admin communication
      msg.MocID = Utility.AdminCommunication;
      //created by who
      msg.ByWho = userID;
      // 1 - WIMCO contact
      msg.FromWho = Utility.WimcoContact;
      // From contact person
      msg.FromContact = userName;
      // 2 - Client contact
      msg.ToWho = Utility.ClientContact;
      //to contact person
      msg.ToContact = userName;
      msg.Body = messageText;
      //msgList.MessageType = 
      // 8 - no transmittion is required
      msg.MessageStatusID = Utility.NoTransmittionRequired;
      // 205 - client Letter
      msg.EventKindID = Utility.ClientLetter;
      listOfMessages.Add(msg);

    }
    */

    private void showMessage(string result)
    {
      Label lblMessage = new Label();
      lblMessage.Text = "<script language='javascript'>" + Environment.NewLine +
         "window.alert(" + "'" + result + "'" + ")</script>";

      // add the label to the page to display the alert
      Page.Controls.Add(lblMessage);
    }

    protected void ASPxTextBox3_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnGoBack_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterMainViewer.aspx");
      Response.Redirect(url);
    }

    protected void btnEditSignature_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "Signature.aspx");
      Response.Redirect(url);
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
      //SendEmail();
      lblErrorMessage.Text = string.Empty;
      SendEmailGroupWise();
    }

    protected void ucAttachment_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
    {
      Attachment att = null;
      mail = Session["Mail"] as SmtpEmail;
      if (mail != null)
      {
        att = new Attachment(e.UploadedFile.FileContent, e.UploadedFile.FileName);
        mail.AttachedFiles.Add(att);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnPreviewCheckByPhone Form control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewCheckByPhone_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      //LetterList letterList;

      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.CheckByPhoneForm;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      try
      {

        letterList = GetLetterData();
        dsResv = printLetter.GetDataForForm(letterList, chkPhoneUseBalance.Checked);
        rptSelectedLetter = printLetter.GetRpt(dsResv, file);

        Session.Add("ReportObjectPreview", rptSelectedLetter);

        url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterPreview.aspx");
        //Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
      }
      catch
      {
        //TODO: better handle exception
      }
    }


    protected void chkCcAuthorization_CheckedChanged(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Handles the Click event of the btnPreviewUSD Wire transfer instructions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewUSD_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.WireTransferInstructionsUSD;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      rptSelectedLetter = printLetter.GetRpt(file);
      HttpContext.Current.Session.Add("ReportObjectPreview", rptSelectedLetter);

      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterPreview.aspx");
      //HttpContext.Current.Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);

    }

    /// <summary>
    /// Handles the Click event of the btnPreviewCreditCard Authorization form control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewCreditCard_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      LetterList letterList;

      string fileName;
      int fileID;
      string url = string.Empty;
      string villaCode = string.Empty;

      fileID = (int)LetterType.CreditCardAuthorizationForm2;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      letterList = GetLetterData();
      dsResv = printLetter.GetDataForForm(letterList, chkCredidCardBalance.Checked);

      rptSelectedLetter = printLetter.GetRpt(dsResv, file);
      Session.Add("ReportObjectPreview", rptSelectedLetter);

      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterPreview.aspx");
      //Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
    }

    /// <summary>
    /// Handles the Click event of the btnPreviewEuro Wire transfer instructions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewEuro_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.WireTransferInstructionsEuro;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      rptSelectedLetter = printLetter.GetRpt(file);
      HttpContext.Current.Session.Add("ReportObjectPreview", rptSelectedLetter);

      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterPreview.aspx");
      //HttpContext.Current.Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);

    }

    /// <summary>
    /// Handles the Click event of the btnPreviewPound Wire transfer instructions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPreviewPound_Click(object sender, EventArgs e)
    {
      XtraReport rptSelectedLetter;
      FileStorageData fileStorageData = new FileStorageData();
      FileStorage file;
      string fileName;
      int fileID;
      string url = string.Empty;

      fileID = (int)LetterType.WireTransferInstructionsStering;

      file = fileStorageData.GetFile(fileID);
      fileName = file.FileName;

      rptSelectedLetter = printLetter.GetRpt(file);
      HttpContext.Current.Session.Add("ReportObjectPreview", rptSelectedLetter);

      url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "LetterPreview.aspx");
      //HttpContext.Current.Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
    }

    protected void grdLetterList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      string url = string.Empty;
      int index;
      string letterSite = string.Empty;
      Label lblLetterName;
      Label lblFilePath;

      //string resvNumberList = string.Empty;
      string letterName = string.Empty;

      if (e.CommandName == "Select")
      {
        index = Convert.ToInt32(e.CommandArgument);
        lblFilePath = (Label)grdLetterList.Rows[index].Cells[1].FindControl("lblFilePath");
        lblLetterName = (Label)grdLetterList.Rows[index].Cells[1].FindControl("lblLetterName");

        if (lblLetterName != null)
        {
          letterName = lblLetterName.Text;
          /*
          if (letterName.Contains("Itinerary Statement"))
          {
            //remove the previous Itinerary Statement
            printLetter.FileList.RemoveAt(index);
            //get a list of reservation numbers 
            resvNumberList = GetResvNumber();
            if (resvNumberList.Length > 2)
            {
              ProcessLetterItineraryStatement itineraryStatement = new ProcessLetterItineraryStatement();
              itineraryStatement.Process(resvNumberList, Utility.PDF);
              ViewLetter(letterName);
            }
          }
          else
          {
            ViewLetter(letterName);
          }
          */
          //ViewLetter(letterName);


          if (lblFilePath.Text.Contains("http"))
          {
            url = $"{lblFilePath.Text}/{lblLetterName.Text}";
          }
          else
          {
            fileName = GetLetterFile(letterName);
            Session["FileName"] = fileName;
            if (Properties.Settings.Default.IsTesting)
            {
              url = $"{Properties.Settings.Default.LetterSiteTestValue}/{"OpenPDF.aspx"}";
            }
            else
            {
              url = $"{Properties.Settings.Default.LetterSite}/{"OpenPDF.aspx"}";
            }
          }
          //Response.Write("<script> var w = window.open('" + url + "','_blank');</script>");
          ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
        }
      }
    }

    /// <summary>
    /// Handles the Click event of the Button1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Button1_Click(object sender, EventArgs e)
    {
      /*
      CheckBox attached = null;
      Label letterName = null;
      string temp;
      for (int i = 0; i < grdLetterList.Rows.Count; i++)
      {
        attached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");
        if (attached.Checked)
        {
          letterName = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
          temp = letterName.Text;
        }
      }
      */

      //SendEmail();

    }

    public string GetLetterFile(string fileName)
    {
      Properties.Settings appSettings = new Properties.Settings();

      //fileName = MapPath((string.Format(@"{0}\{1}.pdf", appSettings.LetterFolder, fileName)));
      fileName = (string.Format(@"{0}\{1}", appSettings.LetterFolder, fileName));

      if (!File.Exists(fileName))
      {
        fileName = string.Empty;
      }

      return fileName;
    }

    private void ViewLetter(string letterName)
    {
      string fileName = string.Empty;

      Response.ContentType = "Application/pdf";


      try
      {
        //Write the file directly to the HTTP content output stream.
        try
        {

          fileName = GetLetterFile(letterName);
          if (fileName.Length > 0)
          {
            Response.WriteFile(fileName);
            //Response.Write("<script>window.open('+fileName+','_blank');</script>");
          }
          else
          {
            //Response.Redirect("~/WebEmail.aspx");
          }
        }
        catch
        {
          throw;
        }
      }
      finally
      {
        //end the buffer
        Response.End();
      }

    }

    protected void btnViewLetter_Click(object sender, EventArgs e)
    {
      string fileName = string.Empty;

      Response.ContentType = "Application/pdf";
      try
      {
        //Write the file directly to the HTTP content output stream.
        try
        {

          fileName = GetLetterFile("CheckByPhoneForm");
          //if a contract is found, open the contract form; otherwise, redirect to
          //the generate contract form
          if (fileName.Length > 0)
          {
            Response.WriteFile(fileName);
            //Response.Write("<script>window.open('+fileName+','_blank');</script>");
          }
          else
          {
            //Response.Redirect("~/WebEmail.aspx");
          }
        }
        catch
        {
          throw;
        }
      }
      finally
      {
        //end the buffer
        Response.End();
      }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      string result = string.Empty;
      string resvNo = string.Empty;
      int itinerayID = 0;
      //string resvNumberList = string.Empty;
      int letterKind;

      ProcessLetterPreArrivalLetter preArrivalLetter;

      try
      {
        resvNo = Session["ReservationNumber"].ToString();
      }
      catch
      {
        resvNo = string.Empty;
      }

      /*
      try
      {
        resvNumberList = Session["ResvNumberList"].ToString();
      }
      catch
      {
        resvNumberList = string.Empty;
      }
      */

      //resvNumberList = ";M62909;M71222;M72201;";

      //resvNo = "M56084";

      if (resvNo.Length > 0)
      {
        //delete all exiting files in the list.
        //printLetter.FileList.Clear();

        letterKind = Convert.ToInt32(rdbtnSelectLetterKind.Value);


        preArrivalLetter = new ProcessLetterPreArrivalLetter(printLetter);
        result = preArrivalLetter.Process(resvNo, Utility.PDF);

        //if testing, use the WebEmail page in the current URL, instead of the production one (Nomad sub site).
        try
        {
          testing = (bool)Session["Testing"];
        }
        catch
        {
          //TODO: Better handle exception
          testing = false;
        }

        if (appSettings.IsTesting || testing)
        {
          Response.Redirect("~/WebEmail.aspx");
        }

        if (result == string.Empty)
        {
          url = string.Format("{0}/{1}", Properties.Settings.Default.LetterSite, "WebEMail.aspx");
          Response.Redirect(url);
          //grdLetterList.DataSource = printLetter.FileList;
          //grdLetterList.DataBind();
        }
        else
        {
          showMessage(result);
        }

      }
    }

    private string GetResvNumber()
    {
      CheckBox chkAttached = null;
      Label lblResvNumber = null;
      string result = ",";
      for (int i = 0; i < grdLetterList.Rows.Count; i++)
      {
        chkAttached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");
        if (chkAttached != null)
        {
          if (chkAttached.Checked)
          {
            lblResvNumber = (Label)grdLetterList.Rows[i].Cells[3].FindControl("lblResvNumber");
            if (lblResvNumber != null)
            {
              result += string.Format("{0},", lblResvNumber.Text);
            }
          }
        }
      }

      return result;
    }

    private void GetValue(int index)
    {
      Label lblTemp = null;
      string temp;

      lblTemp = (Label)grdLetterList.Rows[index].Cells[4].FindControl("lblResvNumber");
      if (lblTemp != null)
      {
        //if a reservation is already existing (added to the list), skip it.
        // also, skipping the adding total price and balance.
        // this logic to take care of resv with TA.  With TA, there are two letters.
        if (!letterList.ResvNumber.Contains(lblTemp.Text))
        {
          letterList.ResvNumber += string.Format("{0},", lblTemp.Text);
          lblTemp = (Label)grdLetterList.Rows[index].Cells[2].FindControl("lblTotalPrice");
          if (lblTemp != null)
          {
            letterList.TotalPrice += Convert.ToDecimal(lblTemp.Text);
          }

          lblTemp = (Label)grdLetterList.Rows[index].Cells[3].FindControl("lblBalanceAmt");
          if (lblTemp != null)
          {
            temp = lblTemp.Text.Trim(new Char[] { '$', '€', '£', '(', ')' }); //remove those charaters from a value if there is any

            letterList.BalanceAmt += Convert.ToDecimal(temp);
          }

          lblTemp = (Label)grdLetterList.Rows[index].Cells[3].FindControl("lblAmountDue");
          if (lblTemp != null)
          {
            temp = lblTemp.Text.Trim(new Char[] { '$', '€', '£', '(', ')' }); //remove those charaters from a value if there is any

            letterList.AmountDue += Convert.ToDecimal(temp);
          }

          lblTemp = (Label)grdLetterList.Rows[index].Cells[9].FindControl("lblVillaCode");
          if (lblTemp != null)
          {
            letterList.VillaCode += string.Format("{0},", lblTemp.Text);
          }
        }
      }

      lblTemp = (Label)grdLetterList.Rows[index].Cells[1].FindControl("lblResvCurrencySymbol");
      if (lblTemp != null)
      {
        letterList.ResvCurrencySymbol = lblTemp.Text;
      }


      /*
      lblTemp = (Label)grdLetterList.Rows[index].Cells[4].FindControl("lblResvNumber");
      if (lblTemp != null)
      {
        letterList.ResvNumber += string.Format("{0},", lblTemp.Text);
      }
      */

      lblTemp = (Label)grdLetterList.Rows[index].Cells[5].FindControl("lblResvAgent");
      if (lblTemp != null)
      {
        letterList.ResvAgent = lblTemp.Text;
      }

      lblTemp = (Label)grdLetterList.Rows[index].Cells[6].FindControl("lblDateFrom");
      if (lblTemp != null)
      {
        letterList.DateFrom = Convert.ToDateTime(lblTemp.Text);
      }
      lblTemp = (Label)grdLetterList.Rows[index].Cells[7].FindControl("lblDateTo");
      if (lblTemp != null)
      {
        letterList.DateTo = Convert.ToDateTime(lblTemp.Text);
      }

      lblTemp = (Label)grdLetterList.Rows[index].Cells[10].FindControl("lblFirstName");
      if (lblTemp != null)
      {
        letterList.FirstName = lblTemp.Text;
      }
      lblTemp = (Label)grdLetterList.Rows[index].Cells[11].FindControl("lblLastName");
      if (lblTemp != null)
      {
        letterList.LastName = lblTemp.Text;
      }
      lblTemp = (Label)grdLetterList.Rows[index].Cells[12].FindControl("lblClientName");
      if (lblTemp != null)
      {
        letterList.ClientName = lblTemp.Text;
      }

    }

    private LetterList GetLetterData()
    {
      letterList = new LetterList();
      int index;
      CheckBox chkAttached = null;
      Label lblTemp = null;
      bool letterIncluded = false;

      letterList.ResvNumber = string.Empty;
      letterList.VillaCode = string.Empty;

      // loop through grid view and get letter info
      for (int i = 0; i < grdLetterList.Rows.Count; i++)
      {
        chkAttached = (CheckBox)grdLetterList.Rows[i].Cells[0].FindControl("chkAttached");

        // get Itinerary Statement data, regardless checked or not checked
        lblTemp = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
        if (lblTemp != null)
        {
          // if it is the Itinerary Statement letter, get start and end travel dates of an itinerary
          if (lblTemp.Text.Contains(itineraryStatementName))
          {
            lblTemp = (Label)grdLetterList.Rows[i].Cells[6].FindControl("lblDateFrom");
            if (lblTemp != null)
            {
              letterList.DateFrom = Convert.ToDateTime(lblTemp.Text);
            }
            lblTemp = (Label)grdLetterList.Rows[i].Cells[7].FindControl("lblDateTo");
            if (lblTemp != null)
            {
              letterList.DateTo = Convert.ToDateTime(lblTemp.Text);
            }
          }
        }

        // if a letter is attached
        if (chkAttached.Checked)
        {
          lblTemp = (Label)grdLetterList.Rows[i].Cells[1].FindControl("lblLetterName");
          if (lblTemp != null)
          {
            if (!lblTemp.Text.Contains(itineraryStatementName))
            {
              letterIncluded = true;
              GetValue(i);
            }
          }
        }
      }

      // if no letter is attached, use info from the Itinerary Statement if there is one.
      if (!letterIncluded)
      {
        if (grdLetterList.Rows.Count > 0)
        {
          // Itinerary statement is always at end of the grid.
          index = grdLetterList.Rows.Count - 1;
          lblTemp = (Label)grdLetterList.Rows[index].Cells[1].FindControl("lblLetterName");
          if (lblTemp != null)
          {
            // if it is the Itinerary Statement letter, get start and end travel dates of an itinerary
            if (lblTemp.Text.Contains(itineraryStatementName))
            {
              GetValue(index);
            }
          }

        }
      }

      if (letterList.ResvNumber.Length > 0)
      {
        // remove the last comma
        letterList.ResvNumber = letterList.ResvNumber.Substring(0, letterList.ResvNumber.Length - 1);
      }
      if (letterList.VillaCode.Length > 0)
      {
        // remove the last comma
        letterList.VillaCode = letterList.VillaCode.Substring(0, letterList.VillaCode.Length - 1);
      }

      return letterList;
    }

    protected void rdbtnSelectLetterKind_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void chkPhoneByCheck0_CheckedChanged(object sender, EventArgs e)
    {

    }
  }
}

