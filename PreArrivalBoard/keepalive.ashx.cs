﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Web.SessionState;

namespace PreArrivalBoard
{
  /// <summary>
  /// keep alive.
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  public class keepalive : IHttpHandler, IRequiresSessionState
  {

    public void ProcessRequest(HttpContext context)
    {
      context.Session["KeepSessionAlive"] = DateTime.Now;
      string sID = context.Session.SessionID;
      string xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><appXml><keepalive>123</keepalive></appXml>";
      context.Response.Write(xmlStr);
      context.Response.Flush();
      context.Response.End();

    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }
  }
}
