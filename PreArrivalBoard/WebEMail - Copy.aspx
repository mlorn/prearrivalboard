﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebEMail.aspx.cs" Inherits="PreArrivalBoard.WebEMail" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
  Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
  Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
  Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Send Letter</title>

  <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>

  <script type="text/javascript" language="javascript">
    function OnCustomCommand(s, e) {
      switch (e.commandName) {
        case 'SendEmail':
          s.PerformDataCallback(e.commandName, function () { });
          break;
      }
    }

  </script>

  <style type="text/css">
    .style2 {
      width: 10px;
    }

    .style3 {
      width: 234px;
    }

    .style4 {
      height: 414px;
    }

    .style5 {
      width: 200px;
    }

    .style6 {
      width: 287px;
    }

    .style7 {
      height: 414px;
      width: 500px;
    }

    .style8 {
      height: 25px;
    }

    .style9 {
      width: 10px;
      height: 25px;
    }

    .style10 {
      width: 287px;
      height: 25px;
    }

    .auto-style2 {
      width: 174px;
    }

    .auto-style3 {
      height: 27px;
    }

    .auto-style4 {
      width: 174px;
      height: 27px;
    }

    .auto-style5 {
      width: 200px;
      height: 27px;
    }

    .auto-style6 {
      margin-right: 1px;
    }
  </style>
</head>
<body>
  <form id="theForm" runat="server">
    <div>
      <asp:ScriptManager ID="ScriptManager1" runat="server">
      </asp:ScriptManager>
      <table style="width: 1182px">
        <tr>
          <td>
            <asp:Label ID="lblFrom" runat="server" Text="From:"></asp:Label>
          </td>
          <td>
            <dx:ASPxTextBox ID="txtFromEmail" runat="server" Width="245px">
              <ValidationSettings SetFocusOnError="True">
                <RequiredField ErrorText="The From Email address is required." />
              </ValidationSettings>
            </dx:ASPxTextBox>
          </td>
          <td class="style2">
            <asp:Label ID="lblCC" runat="server" Text="CC:"></asp:Label>
          </td>
          <td>
            <dx:ASPxTextBox ID="txtCCEmail" runat="server" Width="245px">
            </dx:ASPxTextBox>
          </td>
          <td class="style6">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <asp:Label ID="LblTo" runat="server" Text="To:"></asp:Label>
          </td>
          <td>
            <dx:ASPxTextBox ID="txtToEmail" runat="server" Width="245px" OnTextChanged="ASPxTextBox3_TextChanged">
              <ValidationSettings SetFocusOnError="True">
                <RequiredField ErrorText="The To email address is required." />
              </ValidationSettings>
            </dx:ASPxTextBox>
          </td>
          <td class="style2">
            <asp:Label ID="Label1" runat="server" Text="BC:"></asp:Label>
          </td>
          <td>
            <dx:ASPxTextBox ID="txtBCEmail" runat="server" Width="245px">
            </dx:ASPxTextBox>
          </td>
          <td class="style6">&nbsp;
          </td>
        </tr>
        <tr>
          <td class="style8">
            <asp:Label ID="lblSubject" runat="server" Text="Subject:"></asp:Label>
          </td>
          <td class="style8" colspan="4">
            <dx:ASPxTextBox ID="txtSubject" runat="server" Width="1046px">
            </dx:ASPxTextBox>
          </td>
          <td class="style9">&nbsp;
          </td>
          <td class="style8">&nbsp;
          </td>
          <td class="style10">&nbsp;
          </td>
        </tr>
        <tr>
          <td colspan="5" class="style4">
            <dx:ASPxHtmlEditor ID="htmlEditorWebEMail" runat="server" Width="110%" ClientInstanceName="email"
              OnCustomDataCallback="htmlEditorWebEMail_CustomDataCallback" Height="900px" CssClass="auto-style6">
              <Toolbars>
                <dx:HtmlEditorToolbar Name="StandardToolbar1">
                  <Items>
                    <dx:ToolbarCutButton ToolTip="Cut (Ctrl+X).  This command cannot be executed.  Please use Ctrl + X to Cut to the clipboard.">
                    </dx:ToolbarCutButton>
                    <dx:ToolbarCopyButton ToolTip="Copy (Ctrl+C).  This command cannot be executed.  Please use Ctrl + C to Copy to the clipboard.">
                    </dx:ToolbarCopyButton>
                    <dx:ToolbarPasteButton ToolTip="Paste (Ctrl+V).  This command cannot be executed.  Please use Ctrl + V to Paste from the clipboard.">
                    </dx:ToolbarPasteButton>
                    <dx:ToolbarPasteFromWordButton>
                    </dx:ToolbarPasteFromWordButton>
                    <dx:ToolbarUndoButton BeginGroup="True">
                    </dx:ToolbarUndoButton>
                    <dx:ToolbarRedoButton>
                    </dx:ToolbarRedoButton>
                    <dx:ToolbarRemoveFormatButton BeginGroup="True">
                    </dx:ToolbarRemoveFormatButton>
                    <dx:ToolbarSuperscriptButton BeginGroup="True">
                    </dx:ToolbarSuperscriptButton>
                    <dx:ToolbarSubscriptButton>
                    </dx:ToolbarSubscriptButton>
                    <dx:ToolbarInsertOrderedListButton BeginGroup="True">
                    </dx:ToolbarInsertOrderedListButton>
                    <dx:ToolbarInsertUnorderedListButton>
                    </dx:ToolbarInsertUnorderedListButton>
                    <dx:ToolbarIndentButton BeginGroup="True">
                    </dx:ToolbarIndentButton>
                    <dx:ToolbarOutdentButton>
                    </dx:ToolbarOutdentButton>
                    <dx:ToolbarInsertLinkDialogButton BeginGroup="True">
                    </dx:ToolbarInsertLinkDialogButton>
                    <dx:ToolbarUnlinkButton>
                    </dx:ToolbarUnlinkButton>
                    <dx:ToolbarInsertImageDialogButton>
                    </dx:ToolbarInsertImageDialogButton>
                    <dx:ToolbarTableOperationsDropDownButton BeginGroup="True">
                      <Items>
                        <dx:ToolbarInsertTableDialogButton BeginGroup="True">
                        </dx:ToolbarInsertTableDialogButton>
                        <dx:ToolbarTablePropertiesDialogButton BeginGroup="True">
                        </dx:ToolbarTablePropertiesDialogButton>
                        <dx:ToolbarTableRowPropertiesDialogButton>
                        </dx:ToolbarTableRowPropertiesDialogButton>
                        <dx:ToolbarTableColumnPropertiesDialogButton>
                        </dx:ToolbarTableColumnPropertiesDialogButton>
                        <dx:ToolbarTableCellPropertiesDialogButton>
                        </dx:ToolbarTableCellPropertiesDialogButton>
                        <dx:ToolbarInsertTableRowAboveButton BeginGroup="True">
                        </dx:ToolbarInsertTableRowAboveButton>
                        <dx:ToolbarInsertTableRowBelowButton>
                        </dx:ToolbarInsertTableRowBelowButton>
                        <dx:ToolbarInsertTableColumnToLeftButton>
                        </dx:ToolbarInsertTableColumnToLeftButton>
                        <dx:ToolbarInsertTableColumnToRightButton>
                        </dx:ToolbarInsertTableColumnToRightButton>
                        <dx:ToolbarSplitTableCellHorizontallyButton BeginGroup="True">
                        </dx:ToolbarSplitTableCellHorizontallyButton>
                        <dx:ToolbarSplitTableCellVerticallyButton>
                        </dx:ToolbarSplitTableCellVerticallyButton>
                        <dx:ToolbarMergeTableCellRightButton>
                        </dx:ToolbarMergeTableCellRightButton>
                        <dx:ToolbarMergeTableCellDownButton>
                        </dx:ToolbarMergeTableCellDownButton>
                        <dx:ToolbarDeleteTableButton BeginGroup="True">
                        </dx:ToolbarDeleteTableButton>
                        <dx:ToolbarDeleteTableRowButton>
                        </dx:ToolbarDeleteTableRowButton>
                        <dx:ToolbarDeleteTableColumnButton>
                        </dx:ToolbarDeleteTableColumnButton>
                      </Items>
                    </dx:ToolbarTableOperationsDropDownButton>
                    <dx:ToolbarFullscreenButton BeginGroup="True">
                    </dx:ToolbarFullscreenButton>
                    <dx:CustomToolbarButton CommandName="SendEmail" ViewStyle="Image" ToolTip="Send Email"
                      Visible="False">
                      <Image Url="Images/SendEmail.png" Width="32px" Height="32px">
                      </Image>
                    </dx:CustomToolbarButton>
                  </Items>
                </dx:HtmlEditorToolbar>
                <dx:HtmlEditorToolbar Name="StandardToolbar2">
                  <Items>
                    <dx:ToolbarParagraphFormattingEdit Width="120px">
                      <Items>
                        <dx:ToolbarListEditItem Text="Normal" Value="p" />
                        <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                        <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                        <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                        <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                        <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                        <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                        <dx:ToolbarListEditItem Text="Address" Value="address" />
                        <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                      </Items>
                    </dx:ToolbarParagraphFormattingEdit>
                    <dx:ToolbarFontNameEdit>
                      <Items>
                        <dx:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman" />
                        <dx:ToolbarListEditItem Text="Tahoma" Value="Tahoma" />
                        <dx:ToolbarListEditItem Text="Verdana" Value="Verdana" />
                        <dx:ToolbarListEditItem Text="Arial" Value="Arial" />
                        <dx:ToolbarListEditItem Text="Segoe UI" Value="Segoe UI" />
                        <dx:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif" />
                        <dx:ToolbarListEditItem Text="Courier" Value="Courier" />
                      </Items>
                    </dx:ToolbarFontNameEdit>
                    <dx:ToolbarFontSizeEdit>
                      <Items>
                        <dx:ToolbarListEditItem Text="1 (8pt)" Value="1" />
                        <dx:ToolbarListEditItem Text="2 (10pt)" Value="2" />
                        <dx:ToolbarListEditItem Text="3 (12pt)" Value="3" />
                        <dx:ToolbarListEditItem Text="4 (14pt)" Value="4" />
                        <dx:ToolbarListEditItem Text="5 (18pt)" Value="5" />
                        <dx:ToolbarListEditItem Text="6 (24pt)" Value="6" />
                        <dx:ToolbarListEditItem Text="7 (36pt)" Value="7" />
                      </Items>
                    </dx:ToolbarFontSizeEdit>
                    <dx:ToolbarBoldButton BeginGroup="True">
                    </dx:ToolbarBoldButton>
                    <dx:ToolbarItalicButton>
                    </dx:ToolbarItalicButton>
                    <dx:ToolbarUnderlineButton>
                    </dx:ToolbarUnderlineButton>
                    <dx:ToolbarStrikethroughButton>
                    </dx:ToolbarStrikethroughButton>
                    <dx:ToolbarJustifyLeftButton BeginGroup="True">
                    </dx:ToolbarJustifyLeftButton>
                    <dx:ToolbarJustifyCenterButton>
                    </dx:ToolbarJustifyCenterButton>
                    <dx:ToolbarJustifyRightButton>
                    </dx:ToolbarJustifyRightButton>
                    <dx:ToolbarJustifyFullButton>
                    </dx:ToolbarJustifyFullButton>
                    <dx:ToolbarBackColorButton BeginGroup="True">
                    </dx:ToolbarBackColorButton>
                    <dx:ToolbarFontColorButton>
                    </dx:ToolbarFontColorButton>
                  </Items>
                </dx:HtmlEditorToolbar>
              </Toolbars>
              <settingsimageselector>
                <commonsettings allowedfileextensions=".jpe, .jpeg, .jpg, .gif, .png"></commonsettings>
              </settingsimageselector>
              <settingsimageupload>
                <validationsettings allowedfileextensions=".jpe, .jpeg, .jpg, .gif, .png">
                </validationsettings>
              </settingsimageupload>
              <ClientSideEvents CustomCommand="OnCustomCommand" />

              <SettingsHtmlEditing EnablePasteOptions="True">
                <PasteFiltering Attributes="class"></PasteFiltering>
              </SettingsHtmlEditing>
              <SettingsResize AllowResize="True" />
            </dx:ASPxHtmlEditor>
          </td>
          <td></td>
        </tr>
      </table>
    </div>
    <div>
      <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="881px">
        <PanelCollection>
          <dx:PanelContent runat="server">
            <table class="dxhe-dialogControlsWrapper">
              <tr>
                <td valign="top">
                  <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Select a Different Letter" Visible="False"></asp:Label>
                  <br />
                  <dx:ASPxRadioButtonList ID="rdbtnSelectLetterKind" runat="server" SelectedIndex="0"
                    ValueType="System.Int32" Width="176px" Height="138px" Visible="False">
                    <Items>
                      <dx:ListEditItem Selected="True" Text="Revision" Value="1" />
                      <dx:ListEditItem Text="Final Payment" Value="2" />
                      <dx:ListEditItem Text="Non-Deposit" Value="3" />
                      <dx:ListEditItem Text="Itinerary Letters" Value="4" />
                      <dx:ListEditItem Text="Itinerary Statement" Value="5" />
                      <dx:ListEditItem Text="Pre-Arrival Letters" Value="6" />
                    </Items>
                  </dx:ASPxRadioButtonList>
                  <dx:ASPxButton ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit"
                    ClientInstanceName="btnSubmit" Width="100px" Visible="False">
                    <ClientSideEvents Click="function(s,e){
                      s.SendPostBack('Click');
                      s.SetEnabled(false);     
                  }"></ClientSideEvents>
                  </dx:ASPxButton>
                  <br />
                  <dx:ASPxButton ID="btnSend" runat="server" ClientInstanceName="btnSend" OnClick="btnSendEmail_Click"
                    Text="Send" Width="100px">
                    <ClientSideEvents Click="function(s,e){
                      s.SendPostBack('Click');
                      s.SetEnabled(false);  
                  }" />
                  </dx:ASPxButton>
                  <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label>
                  <br />
                </td>
                <td class="style3" valign="top">
                  <asp:GridView ID="grdLetterList" runat="server" AutoGenerateColumns="False" Width="640px"
                    OnRowCommand="grdLetterList_RowCommand" Caption="Select Letter">
                    <Columns>
                      <asp:TemplateField HeaderText="Attachment?">
                        <ItemTemplate>
                          <asp:CheckBox ID="chkAttached" Checked='<%# Eval("Attached") %>' runat="server" />
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="FilePath" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="430px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Letter Name">
                        <ItemTemplate>
                          <asp:Label ID="lblLetterName" runat="server" Text='<%# Eval("LetterName") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="430px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Total Price" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblTotalPrice" runat="server" Text='<%# string.Format("{0:C}", Eval("TotalPrice")).Remove(0,1) %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="120px" HorizontalAlign="Right" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Balance Amt" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblBalanceAmt" runat="server" Text='<%# string.Format("{0:C}", Eval("BalanceAmt")).Remove(0,1) %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="120px" HorizontalAlign="Right" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Amt Due" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblAmountDue" runat="server" Text='<%# string.Format("{0:C}", Eval("AmountDue")).Remove(0,1) %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" HorizontalAlign="Right" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Resv No." Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblResvNumber" runat="server" Text='<%# Eval("ResvNumber") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Resv Agent" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblResvAgent" runat="server" Text='<%# Eval("ResvAgent") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="DateFrom" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblDateFrom" runat="server" Text='<%# Eval("DateFrom") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="DateTo" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblDateTo" runat="server" Text='<%# Eval("DateTo") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="ResvCurrencySymbol" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblResvCurrencySymbol" runat="server" Text='<%# Eval("ResvCurrencySymbol") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="VillaCode" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblVillaCode" runat="server" Text='<%# Eval("VillaCode") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="FirstName" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="LastName" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="ClientName" Visible="false">
                        <ItemTemplate>
                          <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("ClientName") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                      </asp:TemplateField>
                      <asp:CommandField ButtonType="Button" SelectText="Preview" ShowSelectButton="True" />
                    </Columns>
                  </asp:GridView>
                </td>
                <td>
                  <table>
                    <tr>
                      <td class="auto-style3">
                        <dx:ASPxCheckBox ID="chkUSDTransfer" runat="server" TextAlign="Left" Visible="False">
                        </dx:ASPxCheckBox>
                      </td>
                      <td class="auto-style4">
                        <dx:ASPxLabel ID="lblUSDTransfer" runat="server" Text="USD Wire Transfer Instructions"
                          Width="180px" Visible="False">
                        </dx:ASPxLabel>
                      </td>
                      <td class="auto-style5"></td>
                      <td class="auto-style3">
                        <dx:ASPxButton ID="btnPreviewUSD" runat="server" Text="Preview" OnClick="btnPreviewUSD_Click" Visible="False">
                        </dx:ASPxButton>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <dx:ASPxCheckBox ID="chkEuroTransfer" runat="server" TextAlign="Left" Visible="False">
                        </dx:ASPxCheckBox>
                      </td>
                      <td class="auto-style2">
                        <dx:ASPxLabel ID="lblEuroTransfer" runat="server" Text="Euro Wire Transfer Instructions" Visible="False">
                        </dx:ASPxLabel>
                      </td>
                      <td class="style5">&nbsp;</td>
                      <td>
                        <dx:ASPxButton ID="btnPreviewEuro" runat="server" Text="Preview" OnClick="btnPreviewEuro_Click" Visible="False">
                        </dx:ASPxButton>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <dx:ASPxCheckBox ID="chkPoundTransfer" runat="server" TextAlign="Left" Visible="False">
                        </dx:ASPxCheckBox>
                      </td>
                      <td class="auto-style2">
                        <dx:ASPxLabel ID="lblPoundTransfer" runat="server" Text="Pound Wire Transfer Instructions" Visible="False">
                        </dx:ASPxLabel>
                      </td>
                      <td class="style5">&nbsp;</td>
                      <td>
                        <dx:ASPxButton ID="btnPreviewPound" runat="server" Text="Preview" OnClick="btnPreviewPound_Click" Visible="False">
                        </dx:ASPxButton>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <dx:ASPxCheckBox ID="chkPhoneByCheck" runat="server" TextAlign="Left" Visible="False">
                        </dx:ASPxCheckBox>
                      </td>
                      <td class="auto-style2">
                        <dx:ASPxLabel ID="lblCheckByPhoneForm" runat="server" Text="Check by Phone Form" Visible="False">
                        </dx:ASPxLabel>
                      </td>
                      <td class="style5">
                        <dx:ASPxCheckBox ID="chkPhoneUseBalance" runat="server" CheckState="Unchecked" OnCheckedChanged="chkPhoneByCheck0_CheckedChanged" Text="Request Balance Amt" Width="150px" Visible="False">
                        </dx:ASPxCheckBox>
                      </td>
                      <td>
                        <dx:ASPxButton ID="btnPreviewCheckByPhone" runat="server" Text="Preview" OnClick="btnPreviewCheckByPhone_Click" Visible="False">
                        </dx:ASPxButton>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <dx:ASPxCheckBox ID="chkCcAuthorization" runat="server" TextAlign="Left" OnCheckedChanged="chkCcAuthorization_CheckedChanged" Visible="False">
                        </dx:ASPxCheckBox>
                      </td>
                      <td class="auto-style2">
                        <dx:ASPxLabel ID="lblCcAuthorizationForm" runat="server" Text="Credit Card Authorization Form" Visible="False">
                        </dx:ASPxLabel>
                      </td>
                      <td class="style5">
                        <dx:ASPxCheckBox ID="chkCredidCardBalance" runat="server" CheckState="Unchecked" OnCheckedChanged="chkPhoneByCheck0_CheckedChanged" Text="Request Balance Amt" Visible="False">
                        </dx:ASPxCheckBox>
                      </td>
                      <td>
                        <dx:ASPxButton ID="btnPreviewCreditCard" runat="server" Text="Preview" OnClick="btnPreviewCreditCard_Click" Visible="False">
                        </dx:ASPxButton>
                      </td>
                    </tr>
                  </table>
                  <table>
                    <tr>
                      <td></td>
                      <td class="style5">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Additional Attachment" Font-Bold="True">
                        </dx:ASPxLabel>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td class="style5">
                        <dx:ASPxUploadControl ID="fuAdditionalAttachment" runat="server" FileInputCount="4"
                          ShowAddRemoveButtons="True" ClientInstanceName="attachment">
                        </dx:ASPxUploadControl>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td class="style5">&nbsp;
                      </td>
                      <td></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </dx:PanelContent>
        </PanelCollection>
      </dx:ASPxPanel>
    </div>
    <p>
      &nbsp;
    </p>
  </form>

</body>
</html>
