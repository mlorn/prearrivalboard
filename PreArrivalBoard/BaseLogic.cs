﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace PreArrivalBoard
{
  public class BaseLogic
  {
    public struct UserData
    {
      public string userName;
      public string adminUserName;
      public int userID;
      public int groupID;
      public string password;
      public string fullName;
      public int salesAssistID;
      public string emailAddress;
      public bool isLoggedIn;
    }

    public static string SerializeAnObject(object AnObject)
    {
      XmlSerializer Xml_Serializer = new XmlSerializer(AnObject.GetType());
      StringWriter Writer = new StringWriter();
      Xml_Serializer.Serialize(Writer, AnObject);
      return Writer.ToString();
    }

    public static string EncodeTo64(string toEncode)
    {
      byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);
      string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
      return returnValue;
    }

    public static string DecodeFrom64(string encodedData)
    {
      byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
      string returnValue = System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);
      return returnValue;
    }
  }
}