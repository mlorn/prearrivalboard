﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PreArrivalBoard.Default" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>PreArrivalBoard</title>
  <script>
    function pageLoad() {
      setInterval("KeepSessionAlive()", 60000);
    }

    function KeepSessionAlive() {
      url = "/keepalive.ashx?";
      var xmlHttp = new XMLHttpRequest();
      xmlHttp.open("GET", url, true);
      xmlHttp.send();
    }
    function ShowEmailWindow() {
      /*      pcEmail.RefreshContentUrl();*/
      pcEmail.Show();
    }
    function HidePopupAndShowInfo(closedBy, returnValue) {
      pcEmail.Hide();
      __doPostBack("<%# btnRefresh.ClientID %>", "");
      /*alert('Closed By: ' + closedBy + '\nReturn Value: ' + returnValue);*/
    }
  </script>
</head>
<body>
  <form id="form1" runat="server">
    <div>
      <asp:SqlDataSource ID="sqlDsReservation" runat="server" ConnectionString="<%$ ConnectionStrings:MagnumConnectionString %>" SelectCommand="spPreArrivalCommResv" SelectCommandType="StoredProcedure">
        <SelectParameters>
          <asp:SessionParameter Name="PrmItineraryID" SessionField="ItineraryID" Type="Int32" />
        </SelectParameters>
      </asp:SqlDataSource>
      <asp:SqlDataSource ID="sdsFinalResv" runat="server" ConnectionString="<%$ ConnectionStrings:MagnumConnectionString %>" SelectCommand="spPreArrivalFinalResv" SelectCommandType="StoredProcedure">
        <SelectParameters>
          <asp:SessionParameter Name="PrmItineraryID" SessionField="ItineraryID" Type="Int32" />
        </SelectParameters>
      </asp:SqlDataSource>
      <asp:ScriptManager ID="ScriptManager1" runat="server">
      </asp:ScriptManager>
      <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
          <dx:ASPxGridView ID="grdItinerary" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdItinerary" Theme="Glass" KeyFieldName="ItineraryID" Width="1658px" Caption="PRE-ARRIVAL Comm">
            <SettingsDetail ShowDetailRow="True" AllowOnlyOneMasterRowExpanded="True" />
            <Templates>
              <DetailRow>
                <dx:ASPxGridView ID="grdReservation" runat="server" AutoGenerateColumns="False" OnBeforePerformDataSelect="grdReservation_BeforePerformDataSelect" KeyFieldName="ReservationNumber" Theme="Glass" DataSourceID="sqlDsReservation">
                  <SettingsPopup>
                    <HeaderFilter MinHeight="140px">
                    </HeaderFilter>
                  </SettingsPopup>
                  <Columns>
                    <dx:GridViewDataTextColumn FieldName="ReservationNumber" VisibleIndex="0">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn FieldName="DateFrom" VisibleIndex="1">
                      <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                      </PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn FieldName="DateTo" VisibleIndex="2">
                      <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                      </PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="VillaCode" VisibleIndex="4">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastName" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="IsDepositPaid" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PhoneNumber" VisibleIndex="8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="NumAdults" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="NumChildren" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="NumRooms" VisibleIndex="11">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TotalCharges" VisibleIndex="12">
                      <PropertiesTextEdit DisplayFormatString="f" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TotalReceived" VisibleIndex="13">
                      <PropertiesTextEdit DisplayFormatString="f" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TotalBalance" VisibleIndex="14">
                      <PropertiesTextEdit DisplayFormatString="f" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ResvAgent" VisibleIndex="15">
                    </dx:GridViewDataTextColumn>
                  </Columns>
                </dx:ASPxGridView>
              </DetailRow>
            </Templates>
            <SettingsPager Mode="ShowAllRecords">
            </SettingsPager>
            <SettingsPopup>
              <HeaderFilter MinHeight="140px"></HeaderFilter>
            </SettingsPopup>
            <Columns>
              <dx:GridViewDataTextColumn FieldName="ItineraryID" VisibleIndex="0" Visible="false">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="Remove" VisibleIndex="1">
                <DataItemTemplate>
                  <asp:LinkButton ID="lnkbtnRemove" runat="server" CommandArgument='<%# Eval("ItineraryID") %>'
                    OnClick="lnkBtnRemove_Click" Text='Remove' ToolTip="Remove this record from displaying">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataDateColumn FieldName="DateSent" VisibleIndex="2">
                <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                </PropertiesDateEdit>
              </dx:GridViewDataDateColumn>
              <dx:GridViewDataTextColumn Caption="First Name" VisibleIndex="3">
                <DataItemTemplate>
                  <asp:LinkButton ID="lnkbtnFirstName" runat="server" CommandArgument='<%# Eval("ClientID") %>'
                    OnClick="lnkBtnClientName_Click" Text='<%# Eval("FirstName") %>' ToolTip="Go to Nomad">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="Last Name" VisibleIndex="4">
                <DataItemTemplate>
                  <asp:LinkButton ID="lnkbtnLastName" runat="server" CommandArgument='<%# Eval("ClientID") %>'
                    OnClick="lnkBtnClientName_Click" Text='<%# Eval("LastName") %>' ToolTip="Go to Nomad">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="ClientEmail" VisibleIndex="5">
                <DataItemTemplate>
                  <asp:LinkButton ID="lnkbtnClientEmail" runat="server" CommandArgument='<%# $"{Eval("ItineraryID")}:{Eval("ClientEmail")}" %>'
                    OnClick="lnkbtnClientEmail_Click" Text='<%# Eval("ClientEmail") %>' ToolTip="Opens email page">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataDateColumn FieldName="DateArrival" VisibleIndex="6">
                <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                </PropertiesDateEdit>
              </dx:GridViewDataDateColumn>
              <dx:GridViewDataDateColumn FieldName="DateDeparture" VisibleIndex="7">
                <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                </PropertiesDateEdit>
              </dx:GridViewDataDateColumn>
              <dx:GridViewDataTextColumn FieldName="TotalCharges" VisibleIndex="8">
                <PropertiesTextEdit DisplayFormatString="f" />
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="TotalReceived" VisibleIndex="9">
                <PropertiesTextEdit DisplayFormatString="f" />
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="Air" VisibleIndex="11">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="Car" VisibleIndex="12">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="AddPax" VisibleIndex="13">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="TA" VisibleIndex="14">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="TotalBalance" VisibleIndex="10">
              </dx:GridViewDataTextColumn>
            </Columns>
          </dx:ASPxGridView>
        </ContentTemplate>
        <Triggers>
          <asp:AsyncPostBackTrigger ControlID="btnRefresh" EventName="Click" />
        </Triggers>
      </asp:UpdatePanel>
    </div>
    <div>
      <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none;" />
    </div>
    <dx:ASPxPopupControl ID="pcEmail" runat="server" ContentUrl="~/WebEMail.aspx" Height="1221px" Modal="True" Width="1279px" ClientInstanceName="pcEmail" CloseAction="CloseButton">
      <ClientSideEvents CloseUp="function(s, e) { s.RefreshContentUrl();  
      }" /> 
    </dx:ASPxPopupControl>

    <div>
      <asp:UpdatePanel ID="upFinalPayment" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
          <dx:ASPxGridView ID="grdFinalItinerary" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdFinalItinerary" Theme="Glass" KeyFieldName="ItineraryID" Width="1658px" Caption="FINAL PAYMENT Collection">
            <SettingsDetail ShowDetailRow="True" AllowOnlyOneMasterRowExpanded="True" />
            <Templates>
              <DetailRow>
                <dx:ASPxGridView ID="grdFinalResv" runat="server" AutoGenerateColumns="False" OnBeforePerformDataSelect="grdFinalResv_BeforePerformDataSelect" KeyFieldName="ItineraryID,ReservationNumber" Theme="Glass" DataSourceID="sdsFinalResv">
                  <SettingsPopup>
                    <HeaderFilter MinHeight="140px">
                    </HeaderFilter>
                  </SettingsPopup>
                  <Columns>
                    <dx:GridViewDataDateColumn FieldName="LetterSentDate" Caption="Date Sent" VisibleIndex="0">
                      <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                      </PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="LetterType" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ReservationNumber" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn FieldName="DateFrom" VisibleIndex="3">
                      <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                      </PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn FieldName="DateTo" VisibleIndex="4">
                      <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                      </PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="VillaCode" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastName" VisibleIndex="8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TotalCharges" VisibleIndex="9">
                      <PropertiesTextEdit DisplayFormatString="f" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TotalReceived" VisibleIndex="10">
                      <PropertiesTextEdit DisplayFormatString="f" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TotalBalance" VisibleIndex="11">
                      <PropertiesTextEdit DisplayFormatString="f" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ResvAgent" VisibleIndex="12">
                    </dx:GridViewDataTextColumn>
                  </Columns>
                </dx:ASPxGridView>
              </DetailRow>
            </Templates>
            <SettingsPager Mode="ShowAllRecords">
            </SettingsPager>
            <SettingsPopup>
              <HeaderFilter MinHeight="140px"></HeaderFilter>
            </SettingsPopup>
            <Columns>
              <dx:GridViewDataTextColumn FieldName="ItineraryID" VisibleIndex="0" Visible="false">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn VisibleIndex="2">
                <DataItemTemplate>
                  <asp:LinkButton ID="btnPrintLetterFinal" runat="server" CausesValidation="false" CommandName="" OnClick="btnPrintLetterFinal_Click" Text="Email"
                    CommandArgument='<%# $"{Eval("ItineraryID")}" %>' EnableViewState="True">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="First Name" VisibleIndex="3">
                <DataItemTemplate>
                  <asp:LinkButton ID="lnkbtnFirstName" runat="server" CommandArgument='<%# Eval("ClientID") %>'
                    OnClick="lnkBtnClientName_Click" Text='<%# Eval("FirstName") %>' ToolTip="Go to Nomad">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="Last Name" VisibleIndex="4">
                <DataItemTemplate>
                  <asp:LinkButton ID="lnkbtnLastName" runat="server" CommandArgument='<%# Eval("ClientID") %>'
                    OnClick="lnkBtnClientName_Click" Text='<%# Eval("LastName") %>' ToolTip="Go to Nomad">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="ClientEmail" VisibleIndex="5">
                <DataItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalClientEmail" runat="server" CommandArgument='<%# $"{Eval("ClientID")}" %>'
                    OnClick="Email_Click" Text='<%# Eval("ClientEmail") %>' ToolTip="Opens email page">
                  </asp:LinkButton>
                </DataItemTemplate>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataDateColumn FieldName="DateArrival" VisibleIndex="6">
                <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                </PropertiesDateEdit>
              </dx:GridViewDataDateColumn>
              <dx:GridViewDataDateColumn FieldName="DateDeparture" VisibleIndex="7">
                <PropertiesDateEdit DisplayFormatString="{0:MMM dd, yyyy}">
                </PropertiesDateEdit>
              </dx:GridViewDataDateColumn>
              <dx:GridViewDataTextColumn FieldName="VillaCode" VisibleIndex="8">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="TotalCharges" VisibleIndex="9">
                <PropertiesTextEdit DisplayFormatString="f" />
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="TotalReceived" VisibleIndex="10">
                <PropertiesTextEdit DisplayFormatString="f" />
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="TotalBalance" VisibleIndex="11">
                <PropertiesTextEdit DisplayFormatString="f" />
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="AgencyName" VisibleIndex="12">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="ResvAgent" VisibleIndex="13">
              </dx:GridViewDataTextColumn>
            </Columns>
          </dx:ASPxGridView>
        </ContentTemplate>
      </asp:UpdatePanel>
    </div>
  </form>
</body>
</html>
