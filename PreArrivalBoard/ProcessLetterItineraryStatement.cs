using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Data;
//using System.Windows.Forms;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;
//using QuickReport;

namespace PreArrivalBoard
{
  public class ProcessLetterItineraryStatement
  {
    private DataSet dsResv;
    private string resvNo;
    private int printTo;
    private PrintLetter printLetter;

    public ProcessLetterItineraryStatement(PrintLetter printLetter)
    {
      this.printLetter = printLetter;
    }
    /// <summary>
    /// Itineraries the statement.
    /// </summary>
    private void ItineraryStatement()
    {
      if (dsResv.Tables[0].DefaultView.Count > 0)
      {
        printLetter.PrintTo((int)LetterType.ItineraryStatement, this.dsResv, this.printTo);
      }
    }

    /// <summary>
    /// Processes the specified resv no.
    /// </summary>
    /// <param name="resvNo">The resv no.</param>
    /// <returns></returns>
    public string Process(int itineraryID, string resvNo, int printTo)
    {
      string result = string.Empty;
      //this.resvNo = resvNo;
      this.printTo = printTo;

      // set this to true so that it will ask to select a printer
      printLetter.FirstTime = true;

      LetterData letterData = new LetterData();
      this.dsResv = letterData.GetItineraryStatementLetterData(itineraryID, resvNo);
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        result = "There is no letter.";
        return result;
      }

      try
      {
        #region Print travel itinerary letter
        //travel itinerary letter
        ItineraryStatement();
        #endregion
      }
      catch (Exception ex)
      {
        result = string.Format("Error has been occurred while printing letters. With error message: {0}", ex.Message);
      }


      return result;
    }
  }
}
