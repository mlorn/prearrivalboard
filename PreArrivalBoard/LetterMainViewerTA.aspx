﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LetterMainViewerTA.aspx.cs" Inherits="PreArrivalBoard.LetterMainViewerTA" %>

<%@ Register Assembly="DevExpress.XtraReports.v19.2.Web.WebForms, Version=19.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Letter Viewer</title>
  <style type="text/css">
    .style1 {
      width: 500px;
    }
  </style>
</head>
<body>
  <form id="form1" runat="server">
    <div>

      <table style="width: 100%;">
        <tr>
          <td class="style1">

            <dx:ReportToolbar ID="TopReportToolbar" runat="server"
              ShowDefaultButtons="False" ReportViewer="<%# ReportViewer1 %>" Width="550px">
              <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                  <Elements>
                    <dx:ListElement Value="pdf" />
                    <dx:ListElement Value="xls" />
                    <dx:ListElement Value="xlsx" />
                    <dx:ListElement Value="rtf" />
                    <dx:ListElement Value="mht" />
                    <dx:ListElement Value="html" />
                    <dx:ListElement Value="txt" />
                    <dx:ListElement Value="csv" />
                    <dx:ListElement Value="png" />
                  </Elements>
                </dx:ReportToolbarComboBox>
              </Items>
              <Styles>
                <LabelStyle>
                  <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
              </Styles>
            </dx:ReportToolbar>
          </td>
          <td width="50PX">
            <dx:ASPxButton ID="btnEmailTop" runat="server" Height="30px"
              HorizontalAlign="Center" Text="EMail" Width="40px" Style="margin-top: 0px"
              OnClick="btnEmail_Click">
            </dx:ASPxButton>
          </td>
          <td>
            <dx:ASPxButton ID="btnSelectDifferentLetterTop" runat="server" Height="30px"
              HorizontalAlign="Center" Text="Select Different Letter" Width="150px" Style="margin-top: 0px"
              OnClick="btnSelectDifferentLetter_Click">
            </dx:ASPxButton>
          </td>
          <td align="left" class="style1">&nbsp;
          </td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <dx:ReportViewer ID="ReportViewer1" runat="server"
        OnUnload="ReportViewer1_Unload">
      </dx:ReportViewer>

      <table style="width: 100%;">
        <tr>
          <td class="style1">
            <dx:ReportToolbar ID="BottomReportToolbar" runat="server"
              ShowDefaultButtons="False" ReportViewer="<%# ReportViewer1 %>" Width="550px">
              <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                  <Elements>
                    <dx:ListElement Value="pdf" />
                    <dx:ListElement Value="xls" />
                    <dx:ListElement Value="xlsx" />
                    <dx:ListElement Value="rtf" />
                    <dx:ListElement Value="mht" />
                    <dx:ListElement Value="html" />
                    <dx:ListElement Value="txt" />
                    <dx:ListElement Value="csv" />
                    <dx:ListElement Value="png" />
                  </Elements>
                </dx:ReportToolbarComboBox>
              </Items>
              <Styles>
                <LabelStyle>
                  <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
              </Styles>
            </dx:ReportToolbar>

          </td>
          <td width="50PX">
            <dx:ASPxButton ID="btnEmailBottom" runat="server" Height="30px"
              HorizontalAlign="Center" Text="EMail" Width="40px" Style="margin-top: 0px"
              OnClick="btnEmail_Click">
            </dx:ASPxButton>
          </td>
          <td>
            <dx:ASPxButton ID="btnSelectDifferentLetterBottom" runat="server" Height="30px"
              HorizontalAlign="Center" Text="Select Different Letter" Width="150px" Style="margin-top: 0px"
              OnClick="btnSelectDifferentLetter_Click">
            </dx:ASPxButton>
          </td>
          <td></td>
          <td>&nbsp;
          </td>
        </tr>
      </table>

    </div>
  </form>
</body>
</html>
